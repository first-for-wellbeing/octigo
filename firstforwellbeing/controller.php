<?php
namespace Concrete\Package\firstForWellbeing; // unique namespace

defined('C5_EXECUTE') or die('Access Denied.');

use \Concrete\Core\Package\Package;
use \Concrete\Core\Page\Single as SinglePage;
use \Concrete\Core\Asset\AssetList as AssetList;
use Concrete\Core\User\Group\Group as Group;
use \Concrete\Core\Page\Theme\Theme;
use \Concrete\Core\Attribute\Key\UserKey;
use Route;

class Controller extends Package
{
    protected $pkgHandle = 'firstforwellbeing'; // package folder name must match exactly
    protected $appVersionRequired = '5.8.1';
    protected $pkgVersion = '0.1.7'; // increment this to install updates in C5 dashboard
    protected $pkgAutoloaderRegistries = array('src/FfwAssessmentLibs' => '\FfwAssessmentLibs');
    public $package_name = 'First For Wellbeing';
    public $package_handle = 'firstforwellbeing';

    public function getPackageDescription()
    {
        return t('First For Wellbeing');
    }

    public function getPackageName()
    {
        return t('First For Wellbeing');
    }

    public function install()
    {
        $pkg = parent::install();

        //////////////////// Register Themes
        Theme::add('ffw_admin', $pkg);
        Theme::add('ffw_assessment', $pkg);

        /////////// Admin
        $sp = SinglePage::add("/ffw", $pkg); $sp_ffw = $sp; // remember for later
        $sp->update(array('cName'=>t('First For Wellbeing Admin'), 'cDescription'=>t('Manage your First For Wellbeing installation.')));
        $sp->setTheme(\PageTheme::getByHandle('ffw_admin'));

        $sp = SinglePage::add("/ffw/questions", $pkg);
        $sp->update(array('cName'=>t('Manage Questions'), 'cDescription'=>t('Manage Questions in First For Wellbeing.')));
        $sp->setTheme(\PageTheme::getByHandle('ffw_admin'));

        $sp = SinglePage::add("/ffw/themes", $pkg);
        $sp->update(array('cName'=>t('Manage Steps'), 'cDescription'=>t('Manage Steps in First For Wellbeing.')));
        $sp->setTheme(\PageTheme::getByHandle('ffw_admin'));

        $sp = SinglePage::add("/ffw/assessments", $pkg);
        $sp->update(array('cName'=>t('Manage Assessments'), 'cDescription'=>t('Manage Assessments in First For Wellbeing.')));
        $sp->setTheme(\PageTheme::getByHandle('ffw_admin'));

        $sp = SinglePage::add("/ffw/theme_rules", $pkg);
        $sp->update(array('cName'=>t('Theme Rules'), 'cDescription'=>t('Manage Rules for Assessment Steps.')));
        $sp->setTheme(\PageTheme::getByHandle('ffw_admin'));
        $sp->setAttribute('exclude_nav', 1);

        $sp = SinglePage::add("/ffw/question_rules", $pkg);
        $sp->update(array('cName'=>t('Question Rules'), 'cDescription'=>t('Manage Rules for Assessment Question.')));
        $sp->setTheme(\PageTheme::getByHandle('ffw_admin'));
        $sp->setAttribute('exclude_nav', 1);

        $sp = SinglePage::add("/ffw/ajax", $pkg);
        $sp->update(array('cName'=>t('FFW Ajax processor'), 'cDescription'=>t('FFW Ajax processor')));
        $sp->setTheme(\PageTheme::getByHandle('ffw_admin'));
        $sp->setAttribute('exclude_nav', 1);

        $sp = SinglePage::add("/ffw/pathways", $pkg);
        $sp->update(array('cName'=>t('Manage Pathways'), 'cDescription'=>t('Manage Pathways')));
        $sp->setTheme(\PageTheme::getByHandle('ffw_admin'));

        $sp = SinglePage::add("/ffw/pathway_rules", $pkg);
        $sp->update(array('cName'=>t('Pathway Rules'), 'cDescription'=>t('Pathway Rules')));
        $sp->setTheme(\PageTheme::getByHandle('ffw_admin'));
        $sp->setAttribute('exclude_nav', 1);

        /////////// Assessment
        $sp = SinglePage::add("/assessment", $pkg); $sp_assessment = $sp;
        $sp->update(array('cName'=>t('First For Wellbeing Assessment'), 'cDescription'=>t('Complete your First For Wellbeing Assessment.')));
        $sp = SinglePage::add("/myassessment", $pkg); $sp_myassessment = $sp;
        $sp->update(array('cName'=>t('My First For Wellbeing Assessment'), 'cDescription'=>t('Your First For Wellbeing Assessment.')));

        /// Ajax processor
		$sp = SinglePage::add("/ajax", $pkg);
		$sp->update(array('cName'=>t('Ajax processor'), 'cDescription'=>t('Ajax processor')));
        $sp->setAttribute('exclude_nav', 1);
        // Login router
		$sp = SinglePage::add("/loginrouter", $pkg);
		$sp->update(array('cName'=>t('Login router'), 'cDescription'=>t('Route users onward dependent upon their group')));
        $sp->setAttribute('exclude_nav', 1);
        // Assessors
		$sp = SinglePage::add("/assessors", $pkg); $sp_ffwassessors = $sp; // save for later
		$sp->update(array('cName'=>t('Assessor Home'), 'cDescription'=>t('Login destination for Assessors')));
        $sp->setTheme(\PageTheme::getByHandle('ffw_admin'));

        // Providers
		$sp = SinglePage::add("/providers", $pkg); $sp_providers = $sp; // save for later
		$sp->update(array('cName'=>t('Provider Home'), 'cDescription'=>t('Login destination for Providers')));
        $sp->setTheme(\PageTheme::getByHandle('ffw_admin'));

        /////////////////// Create usergroups
        // syntax; add( string $gName, string $gDescription, $parentGroup = false, $pkg = null, $gID = null )
        $group_ffwassessors = Group::add( 'FFW Advisors', 'Users allowed to fill in assessments', false, $pkg, null );
        $group_cscassessors = Group::add( 'CSC Advisors', 'Users allowed to fill in assessments', false, $pkg, null );
        $group_providers = Group::add( 'Providers', 'Users who provide services to assessees', false, $pkg, null );
        $group_assadmins = Group::add( 'Assessor Admins', 'Assessors with extra privileges', false, $pkg, null );
        $group_sysadmins = Group::add( 'System Admins', 'Users who can do all but full CMS admin tasks', false, $pkg, null );

        //////// set page permissions
		// syntax: assignPermissions( $userOrGroup, $permissions = array(), $accessType = Concrete\Core\Page\PagePermissionKey::ACCESS_TYPE_INCLUDE )
		// syntax: removePermissions( $userOrGroup, $permissions = array() )
        $group_guest = Group::getByName('Guest');
        $group_users = Group::getByName('Registered Users');
        $sp_myassessment->setPermissionsToManualOverride( );
        $sp_myassessment->assignPermissions( $group_users, [ 'view_page' ]);
        $sp_myassessment->removePermissions( $group_guest, [ 'view_page' ] );

        $sp_ffw->setPermissionsToManualOverride( );
        $sp_ffw->assignPermissions( $group_sysadmins, [ 'view_page' ]);
        $sp_ffw->removePermissions( $group_guest, [ 'view_page' ] );

        $sp_ffwassessors->setPermissionsToManualOverride( );
        $sp_ffwassessors->assignPermissions( $group_ffwassessors, [ 'view_page' ]);
        $sp_ffwassessors->assignPermissions( $group_cscassessors, [ 'view_page' ]);
        $sp_ffwassessors->assignPermissions( $group_assadmins, [ 'view_page' ]);
        $sp_ffwassessors->assignPermissions( $group_sysadmins, [ 'view_page' ]);
        $sp_ffwassessors->removePermissions( $group_guest, [ 'view_page' ] );

		$sp_providers->setPermissionsToManualOverride( );
        $sp_providers->assignPermissions( $group_providers, [ 'view_page' ]);
        $sp_providers->removePermissions( $group_guest, [ 'view_page' ] );

        //// add attributes
        // provider type
        $attr = \Concrete\Core\Attribute\Key\UserKey::getByHandle('userprovidertype');
        if( $attr === null ) {
            $attr = \Concrete\Core\Attribute\Key\UserKey::add('select', array(
                'akHandle' => 'userprovidertype',
                'akName' => t('Provider type'),
                'akIsSearchable' => true
            ), $pkg);
            $option = \Concrete\Attribute\Select\Option::add( $attr, 'Alcohol', 0);
            $option = \Concrete\Attribute\Select\Option::add( $attr, 'Smoking', 0);
            $option = \Concrete\Attribute\Select\Option::add( $attr, 'Housing', 0);
            $option = \Concrete\Attribute\Select\Option::add( $attr, 'Finance', 0);
            $option = \Concrete\Attribute\Select\Option::add( $attr, 'Weight', 0);
            $option = \Concrete\Attribute\Select\Option::add( $attr, 'Social', 0);
            $option = \Concrete\Attribute\Select\Option::add( $attr, 'Emotional', 0);
            $option = \Concrete\Attribute\Select\Option::add( $attr, 'Cold Homes', 0);
            $option = \Concrete\Attribute\Select\Option::add( $attr, 'Employment', 0);
        }

        // assign themes
        $sp_ffw->setTheme(\PageTheme::getByHandle('ffw_admin'));
        $sp_assessment->setTheme(\PageTheme::getByHandle('ffw_assessment'));
        $sp_myassessment->setTheme(\PageTheme::getByHandle('ffw_assessment'));
        $sp_ffwassessors->setTheme(\PageTheme::getByHandle('ffw_assessment'));
        $sp_providers->setTheme(\PageTheme::getByHandle('ffw_assessment'));
        $sp_login = \Concrete\Core\Page\Page::getByPath('/login');
        $sp_login->setTheme(\PageTheme::getByHandle('ffw_assessment'));
        $sp_login = \Concrete\Core\Page\Page::getByPath('/');
        $sp_login->setTheme(\PageTheme::getByHandle('ffw_assessment'));
    }

	public function on_start() {
        $al = AssetList::getInstance();
        //$al->register('javascript', 'firstforwellbeing_sortable', 'js/sortable.js', array(), 'firstforwellbeing');
        $al->register(
            'css', 'glyphicons', 'css/glyphicons.css', [], $this->package_handle);

        // Login re-route
        //\Route::register('/login', function() { \View::element('login', null, $this->package_handle); } );

        // define some system constants that are platform-aware
        // Apache config needs e.g.: SetEnv FFW_PLATFORM LOCAL so we can determine where we're running
        define('FFW_CONFIG', [
            'LOCAL' => [
                'wpurl' => 'http://staging.firstforwellbeing.co.uk',
                'email_override' => 'test@surefyre.com',
                'thisdomain' => 'http://c5.localhost'
            ],
            'DEV' => [
                'wpurl' => 'https://firstforwellbeing.co.uk',
                'email_override' => 'test@surefyre.com',
                'thisdomain' => 'http://dev.assessment.firstforwellbeing.co.uk'
            ],
            'STAGING' => [
                'wpurl' => 'https://firstforwellbeing.co.uk',
                'email_override' => 'test@surefyre.com',
                'thisdomain' => 'http://staging.assessment.firstforwellbeing.co.uk'
            ],
            'PRODUCTION' => [
                'wpurl' => 'https://firstforwellbeing.co.uk',
                'thisdomain' => 'https://assessment.firstforwellbeing.co.uk'
            ]
        ]);
	}



}
