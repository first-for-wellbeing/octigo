<?php
namespace Concrete\Package\firstForWellbeing\Controller\SinglePage; // derived from top-level controller namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Concrete\Core\User\Group\Group as Group;
use Exception;
use stdClass;

class Ajax extends PageController {

	private $data = null;
	private $uID = null;

	public function on_start() {
		$this->data = \Database::connection('ffwdata');
        $this->encdb = \Database::connection('ffwdata_e');
        $this->encryptor = \Core::make("helper/encryption");
		$u = new User();
		$this->uID=$u->getUserID();
        $this->usergroups = [];
        foreach( $u->getUserGroups() as $v ) {
            $this->usergroups[] = Group::getById($v)->gName; // user's assigned groups
        }
		// inheritance
		parent::on_start();
	}

	public function view() {
		die(); // there is no default
	}

	public function checkusername() {
		header('Content-type: application/json');
		if(!isset($_POST['p'])) {
			echo json_encode( [ 'status' => 'error' ] );
			die();
		}
		$list = new \Concrete\Core\User\UserList();
		$list->filterByUsername($_POST['p']);
		$result = $list->getResults();
		if(count($result) != 0) {
			// user exists
			$result[ 'exists' ] = true;
		} else {
			$result[ 'exists' ] = false;
		}
		$result['status'] = 'ok';
		echo json_encode($result);
		die(); // die needed to prevent C5 outputting UI
	}

	public function checkpostcode() {
		header('Content-type: application/json');
		if(!isset($_POST['p'])) {
			echo json_encode( [ 'status' => 'error' ] );
			die();
		}
		$p = str_replace(',', '', strtoupper($_POST['p']));
		$h = $this->data->executeQuery('select count(*) as cnt from postcodes_northants where postcode = ?', [$p], [ \PDO::PARAM_STR ]);
		$h = $h->fetch();
		$result['status'] = 'ok';
		$result['valid'] = ($h['cnt'] > 0);
		echo json_encode($result);
		die(); // die needed to prevent C5 outputting UI
	}

	public function savenote() {

        header('Content-type: application/json');
        if(!isset($_POST['field'])) {
            echo json_encode( [ 'status' => 'error' ] );
            die();
        }
        if(!in_array('FFW Advisors', $this->usergroups)) {
            die();
        }
        // erase any old key and replace
        $h = $this->data->executeQuery('delete from response_results where result_key = ?', [$_POST['field']], [\PDO::PARAM_STR]);
        $content = $this->encryptor->encrypt($_POST['content']);
        $h = $this->encdb->executeQuery('insert into assets (asset) values (?)', [$content], [\PDO::PARAM_STR]);
        $id = $this->encdb->lastInsertId();
        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, ?, ?, now())', [$_POST['rcid'], $_POST['field'], $id]);
        echo json_encode(['status' => 'ok']);
        die();
    }

    function get_tweet() {
	    die();
        //require_once(dirname(__FILE__) . '/../../elements/twitter/tmhOAuth-master/tmhOAuth.php');
        //require_once(dirname(__FILE__) . '/../../elements/twitter/tmhOAuth-master/tmhUtilities.php');
        $TwiKey = 'SLCjz9tnJliF1DOTW1Sbnw'; // Twitter CONSUMER_KEY
        $TwiSec = '4lVUamO1Xorkd4y6BoaP0ln4bYi1uLJOYI2GJE6fuo'; // Twitter CONSUMER_SECRET
        $UsrTok = '361823062-Z0IB44U2uIqrLAwiWJmplrvbfekgtjGr7FspVMnP'; // Twitter Access Token
        $UsrSec = 'tIakMTL6nPT0GlUSpjSSthLM4Ilby4LZPVYzLYjKc'; // Twitter Secret Key

        $output = array();
        $tmhOAuth = new \FfwAssessmentLibs\tmhOAuth(
            array(
                'consumer_key' => $TwiKey,
                'consumer_secret' => $TwiSec,
                'user_token' => $UsrTok,
                'user_secret' => $UsrSec
            )
        );

        $count = (isset($_POST['count'])) ? $_POST['count'] : 1;
        $page = (isset($_POST['page'])) ? $_POST['page'] : 1;
        $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/statuses/user_timeline'), array(
                'screen_name' => (isset($_POST['twinam'])) ? $_POST['twinam'] : 'Firstwellbeing',
                'count' => $count,
                'page' => $page
            )
        );
        $output = $tmhOAuth->response['response'];
        echo json_encode($output);
        die();
    }

    public function postcode_lookup() {
	    if($_POST['p'] == 's') { return json_encode([status => 'error']); }
        $postcode = urlencode($_POST['p']);
        $ch = curl_init("http://maps.northamptonshire.gov.uk/mappingwebservices/address.asmx/GetAddressBaseAddressesForPostcode?postcode=$postcode");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        $output = preg_replace('/xmlns.*\>/', '>', $output);

        $dom = new \DOMDocument();
        $dom->loadXML(utf8_encode($output));
        $xpath = new \DOMXpath($dom);

        $results = $xpath->query('//AddressBaseAddress');
        $addresses = array();
        foreach($results as $address) {
            array_push($addresses, array(
                'Uprn' => $xpath->query('./Uprn', $address)->item(0)->nodeValue,
                'AddressToid' => $xpath->query('./AddressToid', $address)->item(0)->nodeValue,
                'POBox' => $xpath->query('./POBox', $address)->item(0)->nodeValue,
                'HouseNumber' => $xpath->query('./HouseNumber', $address)->item(0)->nodeValue,
                'SubBuildingName' => $xpath->query('./SubBuildingName', $address)->item(0)->nodeValue,
                'Postcode' => $xpath->query('./Postcode', $address)->item(0)->nodeValue,
                'Easting' => $xpath->query('./Easting', $address)->item(0)->nodeValue,
                'Northing' => $xpath->query('./Northing', $address)->item(0)->nodeValue,
                'RepresentativePointCode' => $xpath->query('./RepresentativePointCode', $address)->item(0)->nodeValue,
                'DoubleDependentLocality' => $xpath->query('./DoubleDependentLocality', $address)->item(0)->nodeValue,
                'ChangeType' => $xpath->query('./ChangeType', $address)->item(0)->nodeValue,
                'PostcodeType' => $xpath->query('./PostcodeType', $address)->item(0)->nodeValue,
                'Organisation' => $xpath->query('./Organisation', $address)->item(0)->nodeValue,
                'Department' => $xpath->query('./Department', $address)->item(0)->nodeValue,
                'BuildingName' => $xpath->query('./BuildingName', $address)->item(0)->nodeValue,
                'Street' => $xpath->query('./Street', $address)->item(0)->nodeValue,
                'DependentThoroughfare' => $xpath->query('./DependentThoroughfare', $address)->item(0)->nodeValue,
                'DependentLocality' => $xpath->query('./DependentLocality', $address)->item(0)->nodeValue,
                'PostTown' => $xpath->query('./PostTown', $address)->item(0)->nodeValue
            ));
        }
        header('Content-type: application/json');
        echo json_encode($addresses);
        die();
    }

    public function profileupdate() {
        if(!in_array('FFW Advisors', $this->usergroups) and !in_array('Registered Users', $this->usergroups)) {
            die();
        }
        $custid = $_POST['custid'];
        if(!in_array('FFW Advisors', $this->usergroups) and $this->uID != $custid) {
            die(); // not an assessor, not the current user's id
        }
            $data = stripslashes($_POST['data']);
        // validate custid;
        $custid += 0;
        if($custid < 1) {
            echo json_encode( array( 'status' => 'error' ) );
            die();
        }
        header('Content-type: application/json');
        // validate data
        $data = json_decode($data);
        foreach($data as $k=>$v) {
            $sql = "
			SELECT *
			FROM information_schema.COLUMNS 
			WHERE 
				TABLE_SCHEMA = 'ffw_data' 
			AND TABLE_NAME = 'user_profiles' 
			AND COLUMN_NAME = ?
		";
            $h = $this->data->executeQuery($sql, [$k], [\PDO::PARAM_STR]);
            if(!($h = $h->fetch())) {
                echo json_encode( array( 'status' => 'error' ) );
                die();
            }
        }

        foreach($data as $k=>$v) {
            // get enc assest id from user_profiles
            $h = $this->data->executeQuery("select `$k` from user_profiles where user_id = ?", [$custid], [\PDO::PARAM_INT]);
            if($h = $h->fetch()) {
                $value = $this->encryptor->encrypt($v);
                if(empty($h[$k])) {
                    $h = $this->encdb->executeQuery('insert into assets (asset) values (?)', [$value], [\PDO::PARAM_STR]);
                    $id = $this->encdb->lastInsertId();
                    $h = $this->data->executeQuery("update user_profiles set `$k` = ? where user_id = ?", [$id, $custid], [\PDO::PARAM_INT, \PDO::PARAM_INT]);
                } else {
                    $h = $this->encdb->executeQuery('UPDATE assets SET asset = ? WHERE id = ?', [$value, $h[$k]], [\PDO::PARAM_STR, \PDO::PARAM_INT]);
                }
            } else {
                echo json_encode( array( 'status' => 'error' ) );
                die();
            }
        }
        echo json_encode( array( 'status' => 'ok' ) );
        die();
    }
}
