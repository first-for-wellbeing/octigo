<?php
namespace Concrete\Package\firstForWellbeing\Controller\SinglePage; // derived from top-level controller namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Concrete\Core\User\Group\Group as Group;
use Concrete\Core\Encryption as Encryption;
//use \FfwAssessmentLibs;
use Exception;
use stdClass;

class Assessment extends PageController {

	public $uID = null;
	public $usergroups = null;
    public $username = null;

	public function on_start() {
		// Get DB connection
		$this->data = \Database::connection('ffwdata');
		$this->encdb = \Database::connection('ffwdata_e');
		$this->encryptor = \Core::make("helper/encryption");
		$this->assessment = new \FfwAssessmentLibs\FfwAssessment();
		// Get User info
		$u = new User();
        $this->uID = $u->getUserID(); // userid
        $this->username = $u->getUserName(); // userid
		$this->usergroups = [];
		foreach( $u->getUserGroups() as $v ) {
			$this->usergroups[] = Group::getById($v)->gName; // user's assigned groups
		}
		$this->is_retake = false;
		// Look for existing assessment
        // search by userid & session id
        $sql = "select *, create_datetime + interval 12 week as twelve_weeks, now() >= create_datetime + interval 12 week as can_retake from response_collections where deleted = 0 and (user_id = ? or session_key = ?) order by user_id desc, id desc, create_datetime desc limit 1"; // most recent
        if(isset($_SESSION['for_user'])) {
            $params = [$_SESSION['for_user'], session_id()];
        } else {
            $params = [$this->uID, session_id()];
        }
        $d = [\PDO::PARAM_INT, \PDO::PARAM_STR];
		$h = $this->data->executeQuery($sql, $params, $d);
		if($this->response_collection = $h->fetch()) {
            $this->response_collection['username'] = $this->username;
		    if($this->response_collection['user_id'] == null and $this->uID > 0) {
		        // assign userid to this session-based record
                $h = $this->data->executeQuery('update response_collections set user_id = ? where id = ?', [$this->uID, $this->response_collection['id']], [ \PDO::PARAM_INT, \PDO::PARAM_INT]);
                $this->response_collection['user_id'] = $this->uID;
            }
            // check if marked completed
            if($this->response_collection['completed']) {
		        if(!$this->response_collection['can_retake']) {
		            $this->redirect('/myassessment');
                } else {
		            $this->is_retake = true;
                }
            }
            if($this->response_collection['assessment_number'] > 1) {
		        $this->is_retake = true;
            }
            $this->set('is_retake', $this->is_retake);

            if($this->is_retake and $this->response_collection['assessment_number'] < 2) {
                $this->response_collection = [];
            } else {
                $h = $this->data->executeQuery('SELECT * FROM responses WHERE response_collection_id = ?', [$this->response_collection['id']], [\PDO::PARAM_INT]);
                while ($row = $h->fetch()) {
                    if ($row['response_enc_asset_id'] > 0) {
                        // fetch and decrypt
                        $h2 = $this->encdb->executeQuery('SELECT * FROM assets WHERE id = ?', [$row['response_enc_asset_id']], [\PDO::PARAM_INT]);
                        $h2 = $h2->fetch();
                        $row['value'] = $this->encryptor->decrypt($h2['asset']);
                    }
                    if ($row['response_id'] == null) {
                        // get tickbox responses
                        $row['multiple_responses'] = [];
                        $h2 = $this->data->executeQuery('SELECT * FROM multiple_responses WHERE responses_id = ?', [$row['id']], [\PDO::PARAM_INT]);
                        while ($row2 = $h2->fetch()) {
                            $row['multiple_responses'][$row2['question_choice_id']] = 1;
                        }
                    }
                    $this->response_collection['responses'][$row['question_id']] = $row;
                }
            }
		} else {
			$this->response_collection = [];
		}

        // prefetch profile fields
        if($this->uID > 0 and !in_array('FFW Advisors', $this->usergroups) or isset($_SESSION['for_user'])) {
            $h = $this->data->executeQuery('SELECT * FROM user_profiles WHERE user_id = ?', [$this->response_collection['user_id']], [\PDO::PARAM_INT]);
            $h = $h->fetch();
            $hminus = array_flip($h);
            $ids = '"' . implode('","', array_keys($hminus)) . '"';
            $h1 = $this->encdb->executeQuery('SELECT * FROM assets WHERE id IN (' . $ids . ')');
            while ($row = $h1->fetch()) {
                $h[$hminus[$row['id']]] = $this->encryptor->decrypt($row['asset']);
            }
            $this->response_collection['user_profile'] = $h;
        }
        $this->response_collection['username'] = $this->username;
        $this->response_collection['usergroups'] = $this->usergroups;
		// inheritance
		parent::on_start();
	}

	public function view($step = null, $redir = false) {
        $this->set('uID', $this->uID);
        $this->set('username', $this->username);
		$this->set('usergroups', $this->usergroups);
		$this->set('response_collection', $this->response_collection);
		$this->set('assessment', $this->assessment);

		// get postcode value if set
        $h = $this->data->executeQuery('select id from questions where deleted = 0 and data_handle = "postcode"');
        $h = $h->fetch();
        $this->set('user_postcode', $this->response_collection['responses'][$h['id']]['value']);

        // get # of steps
        $maxstep = count($this->assessment->themes);
        $this->set('maxstep', $maxstep);
        // check for a last step
        if(!isset($this->response_collection['last_step'])) {
            $step = null; // go to the beginning if not logged in, ignore session
        } else if($$redir and $this->response_collection['last_step'] > 0) {
            $step = min($step, $this->response_collection['last_step'] +1);
        }
		// Get correct step by rules, this step may not be visible
        $real_step = $this->get_next_step($step);
		if($real_step != $step) {
		    $this->set('step', $real_step);
		    $this->redirect("/assessment/$real_step");
        } else {
		    $this->set('step', $step);
        }
	}
	
	public function nextstep() {
		if(!isset($this->response_collection['id'])) {
			// create response collection if needed
			if($this->uID > 0) {
			    // check for previous entries
                if($this->is_retake) {
                    $an = 2;
                } else {
                    $an = 1;
                }
				$h = $this->data->executeQuery('insert into response_collections (user_id, create_datetime, assessment_number) values (?, now(), ?)', [$this->uID, $an], [\PDO::PARAM_INT, \PDO::PARAM_INT]);
			} else {
				$h = $this->data->executeQuery('insert into response_collections (session_key, create_datetime) values (?, now())', [session_id()], [\PDO::PARAM_STR]);
			}
			// retrieve new response collection
			$rcid = $this->data->lastInsertId();
			$h = $this->data->executeQuery('select * from response_collections where id = ?', [$rcid], [\PDO::PARAM_INT]);
			$this->response_collection = $h->fetch();
		}
		// start processing the form data
		$rcid = $this->response_collection['id'];
		foreach($_POST as $key => $val) {
			if(is_numeric($key)) { // should be a question response
				// check question type, affects encryption behaviour
				$sql = 'select question_type from questions where id = ?';
				$h = $this->data->executeQuery($sql, [ $key ], [ \PDO::PARAM_INT ]);
				$h = $h->fetch();
				$qtid = $h['question_type'];
				$encid = 0;
				if(in_array($qtid, [ 2, 3, 11, 12 ])) { // text short, text long, email, password (should password be stored?)
					$val = $this->encryptor->encrypt($val);
					$sql = 'insert into assets (asset) values (?)';
					$h = $this->encdb->executeQuery($sql, [ $val ], [ \PDO::PARAM_STR ]);
					$encid = $this->encdb->lastInsertId();
				}
				// save value to DB
				if($encid == 0) {
					if(in_array($qtid, [ 1, 4, 5, 9, 15 ])) { // date, int, decimal, yes/no, calc
						// insert question & response value
						$sql = 'insert into responses (question_id, response_collection_id, response_id, value, creator) values (?, ?, null, ?, ?)
							ON DUPLICATE KEY UPDATE response_id = null, value = ?';
						$params = [ $key, $rcid, $val, $this->uID, $val ];
						$h = $this->data->executeQuery($sql, $params, [\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT]);
					} else if(in_array($qtid, [ 6, 8, 10, 13 ])) { // dropdown, radio, radio icon, slider
						// insert question & response ids
						$sql = 'insert into responses (question_id, response_collection_id, response_id, creator) values (?, ?, ?, ?)
							ON DUPLICATE KEY UPDATE response_id = ?';
						$params = [ $key, $rcid, $val, $this->uID, $val ];
						$h = $this->data->executeQuery($sql, $params, [\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT]);
					} else { // tickboxes
						// store multiple responses
						$sql = 'insert into responses (question_id, response_collection_id, response_id, creator) values (?, ?, null, ?)
							ON DUPLICATE KEY UPDATE response_id = null';
						$params = [ $key, $rcid, $this->uID ];
						$h = $this->data->executeQuery($sql, $params, [\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT]);
						$iid = $this->data->lastInsertId();
						if($iid == 0) { // If the insert just updated then there won't be an insert id so get this row's id
							$h = $this->data->executeQuery('select id from responses where question_id = ? and response_collection_id = ?', [ $key, $rcid ], [ \PDO::PARAM_INT, \PDO::PARAM_INT ] );
							$iid = $h->fetch();
							$iid = $iid['id'];
						}
						// delete old response(s)
						$h = $this->data->executeQuery('delete from multiple_responses where responses_id = ?', [$iid], [\PDO::PARAM_INT]);
						foreach($val as $mr_val) {
							$sql = 'insert into multiple_responses (responses_id, question_choice_id) values (?, ?)';
							$h = $this->data->executeQuery($sql, [ $iid, $mr_val ], [ \PDO::PARAM_INT, \PDO::PARAM_INT ]);
						}
					}
				} else {
					// insert reference to encrypted asset
					$sql = 'insert into responses (question_id, response_collection_id, response_id, response_enc_asset_id, creator) values (?, ?, null, ?, ?)
							ON DUPLICATE KEY UPDATE response_id = null, response_enc_asset_id = ?';
					$params = [ $key, $rcid, $encid, $this->uID, $encid ];
					$h = $this->data->executeQuery($sql, $params, [\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT]);
				}
			}
		}
		// Create user
        if($_POST['step'] == 2) {
		    // find username field id N.B. there should only be one in the whole assessment
            $h = $this->data->executeQuery('select id from questions where special_validation = 3 and deleted = 0');
            $h = $h->fetch();
            $username = $_POST[$h['id']];
            $userRegistrationService = \Core::make('\Concrete\Core\User\RegistrationServiceInterface');
            // double check username not taken
            $list = new \Concrete\Core\User\UserList();
            $list->filterByUsername($username);
            $result = $list->getResults();
            if(count($result) != 0) {
                // reject this user as it exists, unless an assessor
                if(in_array('FFW Advisors', $this->usergroups)) {
                    $ui = $result[0]->getUserID();
                    // Ensure assessment user_id is the end user's
                    $h = $this->data->executeQuery('update response_collections set user_id = ?, assigned_to = ? where id = ?', [$ui, $this->uID, $rcid], [\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT]);
                    $_SESSION['for_user'] = $ui;
                } else {
                    // not an assessor
                    $this->redirect('/login'); // user exists
                }
            } else {
                // not a dupe, attempt creation
                $pass = $_POST['password'];
                $userinfo = [ 'uName' => $username, 'uEmail' => 'dummy@erewhon.fake', 'uPassword' => $pass ]; // email skipped, stored elsewhere
                $reg = $userRegistrationService->create($userinfo);
                if(!$reg) {
                    // failed, but why??
                    $this->redirect('/todo/failed-userreg'); // TODO userreg failed
                } else {
                    // success
                    if(!$this->uID) { // assessor will have an id, don't want to log them out during assessment
                        // if not logged in then log in as this new user
                        $u = new User();
                        $u->getByUserID($reg->getUserID(), true); // log 'em in
                    }
                    // create empty user profile so one always exists
                    $h = $this->data->executeQuery('insert into user_profiles (user_id) values (?)', [$reg->getUserID()], [\PDO::PARAM_INT]);
                    // Ensure assessment user_id is the end user's
                    $h = $this->data->executeQuery('update response_collections set user_id = ? where id = ?', [$reg->getUserID(), $rcid], [\PDO::PARAM_INT, \PDO::PARAM_INT]);
                    if(in_array('FFW Advisors', $this->usergroups)) {
                        $_SESSION['for_user'] = $reg->getUserID();
                    }
                }
            }
            // calc assessment number
            if(isset($_SESSION['for_user'])) {
                $u = $_SESSION['for_user'];
            } else {
                $u = $this->uID;
            }
            $h = $this->data->executeQuery('select max(assessment_number) as mx from response_collections where deleted = 0 and completed = 1 and user_id = ?', [$u], [\PDO::PARAM_INT]);
            $h = $h->fetch();
            $h = (int) $h['mx'] + 1;
            $h = $this->data->executeQuery('update response_collections set assessment_number = ? where id = ?', [$h, $rcid], [\PDO::PARAM_INT, \PDO::PARAM_INT]);
        }
		// remember last completed step
		$h = $this->data->executeQuery('update response_collections set last_step = ? where id = ?', [ $_POST['step'], $rcid ]);
		// check if this was the last step
        if($_POST['step'] == count($this->assessment->themes)) {
            // finalise this assessment
            $h = $this->data->executeQuery('update response_collections set completed = 1, create_datetime = now() where id = ?', [ $rcid ]);
            // if user has no profile info then add assessment field data to into it
            $h = $this->data->executeQuery('select id, profile_mapping from questions where deleted = 0 and profile_mapping <> ""'); // TODO select from theme_questions perhaps?
            $profile = [];
            while($row = $h->fetch()) {
                if($this->response_collection['responses'][$row['id']]['response_enc_asset_id'] > 0) { // is an encrypted value
                    $profile[$row['profile_mapping']]['encid'] = $this->response_collection['responses'][$row['id']]['response_enc_asset_id'];
                } elseif(!empty($this->response_collection['responses'][$row['id']]['value'])) {
                    $profile[$row['profile_mapping']]['val'] = $this->response_collection['responses'][$row['id']]['value'];
                } elseif($this->response_collection['responses'][$row['id']]['response_id'] > 0) {
                    $h1 = $this->data->executeQuery('select choice from question_choices where id = ?', [$this->response_collection['responses'][$row['id']]['response_id']], [\PDO::PARAM_INT]);
                    $h1 = $h1->fetch();
                    $profile[$row['profile_mapping']]['val'] = $h1['choice'];
                }
            }
            if(count($profile) > 0) {
                // insert data into user profile
                foreach($profile as $f => $v) {
                    if (isset($v['val'])) {
                        $h = $this->encdb->executeQuery('INSERT INTO assets (asset) VALUES (?)', [$this->encryptor->encrypt($v['val'])], [\PDO::PARAM_STR]);
                        $id = $this->encdb->lastInsertId();
                        $h = $this->data->executeQuery("update user_profiles set `$f` = ? where user_id = ?", [$id, $this->response_collection['user_id']], [\PDO::PARAM_INT, \PDO::PARAM_INT]);
                    } else {
                        if(isset($v['encid'])) {
                            $h = $this->data->executeQuery("update user_profiles set `$f` = ? where user_id = ?", [$v['encid'], $this->response_collection['user_id']], [\PDO::PARAM_INT, \PDO::PARAM_INT]);
                        }
                    }
                }
            }
            $this->finalise_assessment($rcid);
            // die(); // debug, let the page be reloaded
            $this->redirect('/myassessment/' . $rcid);
        }
        // Get correct step by rules, this step may not be visible
        $real_step = $this->get_next_step($_POST['step'] + 1);
        if($real_step != $_POST['step']) {
            $this->redirect("/assessment/$real_step");
        } else {
            $this->redirect('/assessment/' . ($_POST['step'] + 1));
        }
	}

	public function prevstep() {
		if($_POST['step'] < 2) { $this->redirect('/assessment/1'); }
            $real_step = $this->get_next_step($_POST['step'] - 1, 1);
            if($real_step != $_POST['step'] - 1) {
                $this->redirect("/assessment/$real_step/1");
            } else {
                $this->redirect('/assessment/' . ($_POST['step'] - 1));
            }
	}
	
	private function get_next_step($step, $direction = null) { // direction = 1 then next step is the previous one
		// calculate theme visibility based on rules
        if(empty($this->assessment->themes[$step - 1]['rules'])) {
            return $step; // short cut, if the requested step has no rules then let's just be 'avin it
        }
		$visible_themes = [];
        // aggregate questions
        $choices = [];
        $h = $this->data->executeQuery('select * from question_choices');
        while($row = $h->fetch()) {
            $choices[$row['id']] = $row;
        }
		foreach($this->assessment->themes as $k => $theme) {
			if(empty($theme['rules'])) {
				$visible_themes[] = [ $theme['id'], 1 ];
			} else {
			    // todo roll this into one private method?
				// Need to parse rules
                $ruleset_outcome = false;
				foreach($theme['rules'] as $kr1 => $rule) {
                    $rule_outcome = true;
					foreach($rule['conditions'] as $kr2 => $condition) {
						// Get terms
						// LHS
						if($condition['left_valuetype'] == 4) {
							// question value
							if(isset($this->response_collection['responses'][$condition['left_value']])) {
							    if(empty($this->response_collection['responses'][$condition['left_value']]['value'])) {
							        // Check for response id
                                    $left_val = $this->response_collection['responses'][$condition['left_value']]['response_id'];
                                    $left_val = $choices[$left_val]['value'];
                                } else {
							        $left_val = $this->response_collection['responses'][$condition['left_value']]['value'];
                                }
							} else {
								$left_val = null;
							}
						} else {
							$left_val = $condition['left_value'];
						}
						// RHS
						if($condition['right_valuetype'] == 4) {
							// question value
							if(isset($this->response_collection['responses'][$condition['right_value']])) {
                                if(empty($this->response_collection['responses'][$condition['right_value']]['value'])) {
                                    // Check for response id
                                    $right_val = $this->response_collection['responses'][$condition['right_value']]['response_id'];
                                    $right_val = $choices[$right_val]['value'];
                                } else {
                                    $right_val = $this->response_collection['responses'][$condition['right_value']]['value'];
                                }
							} else {
								$right_val = null;
							}
						} else {
							$right_val = $condition['right_value'];
						}
						switch($condition['ruletype_id']) {
							case 1: // ==
								$rule_outcome &= ( $left_val == $right_val );
								break;
							case 2: // like
								$rule_outcome &= ( stripos($left_val, $right_val) !== false );
								break;
							case 3: // <
								$rule_outcome &= ( $left_val > $right_val);
								break;
							case 4: // >
								$rule_outcome &= ( $left_val < $right_val);
								break;
							case 5: // !=
								$rule_outcome &= ( $left_val != $right_val);
								break;
							default:
								break; // something wrong if here
						}
					} // end conditions
                    $ruleset_outcome |= $rule_outcome;
				} // end rules
                $visible_themes[] = [ $theme['id'], $ruleset_outcome ];
			}
		} // end themes
        // Get steps filtered by rules
        foreach($visible_themes as $k => $v) {
            if($step == ($k + 1)) {
                if($v[1]) {
                    break; // this step is visible
                } else {
                    $direction ? $k-- : $k++;
                    while(!$visible_themes[$k][1]) { $direction ? $k-- : $k++; } // look for next visible step
                    $step = $k + 1; // convert index# to step number
                    break;
                }
            }
        }
        return $step;
	}

	private function finalise_assessment($rcid) {
	    // calc assessment_number
        if(isset($_SESSION['for_user'])) {
            $id = $_SESSION['for_user'];
        } else {
            $id = $uID;
        }
        if(in_array('FFW Advisors', $this->usergroups)) {
            $assid = $this->uID;
        } else {
            $assid = 0;
        }
        $h = $this->data->executeQuery('update response_collections set completed_by_user_id = ?, assigned_to = ? where id= ?', [$this->uID, $assid, $rcid], [\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT]);
	    // run through all pathways
        $h = $this->data->executeQuery('select p.*, pr.rule_id from pathways p join pathway_rulesets pr on p.id = pr.pathway_id where p.deleted = 0');
        $pathways = $ruleset = $rules_matched = [];
        while($row = $h->fetch()) {
            $ruleset['conditions'] = $this->assessment->getRules($row['rule_id']);
            $row['rule_outcome'] = $this->evaluate_rule( [$ruleset] );
            $pathways[$row['id']] = $row;
            if($row['rule_outcome']) {
                $rules_matched[] = $row['rule_id']; // useful for debug
            }
        }
        // Collate P1, P2, P3 outcomes
        $p1_count = $p2_count = $p3_count = 0;
        // Arrays of outcome ids
        $p1_outcomes = [ 1, 2, 5, 11, 14, 15, 18, 19, 20, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 42, 43, 51, 52, 56, 58, 59, 60, 61, 63 ];
        $p2_outcomes = [ 3, 6, 12, 16, 21, 22, 23, 40, 44, 45, 46, 47, 48, 49, 50, 53, 54, 55, 57, 62 ];
        $p3_outcomes = [ 4, 7, 10, 13, 17, 41 ];
        $provider_priorities = [];
        foreach($pathways as $k => $v) {
            if($v['rule_outcome'] == 0) { continue; } // not interested in rules that failed
            // count outcomes
            $p1_count += in_array($v['outcome_id'], $p1_outcomes);
            $p2_count += in_array($v['outcome_id'], $p2_outcomes);
            $p3_count += in_array($v['outcome_id'], $p3_outcomes);
            // calc provider priority
            $this_pri = in_array($v['outcome_id'], $p1_outcomes) * 1 + in_array($v['outcome_id'], $p2_outcomes) * 2 + in_array($v['outcome_id'], $p3_outcomes) * 3; // should always be 1, 2, or 3
            $provider_priorities[$v['provider_type']] = min($this_pri, (isset($provider_priorities[$v['provider_type']]) ? $provider_priorities[$v['provider_type']] : 3));
        }
        // store priorities in DB
        $p1_count = 0;
        foreach($provider_priorities as $k => $v) {
            $sql = 'insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, ?, ?, now())';
            $params = [ $rcid, $k . '_priority', $v ];
            $h = $this->data->executeQuery($sql, $params, [\PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR]);
            if($v == 1) { $p1_count++; }
        }
        // todo one alcohol answer downgrades from p1 to p2
        // Determine on hold status
        $onhold = ($p1_count > 4 or $provider_priorities['Emotional'] == 1 or $provider_priorities['Social'] == 1) * 1; // doing *1 yields 1 or 0
        $sql = 'insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "on_hold", ?, now())';
        $h = $this->data->executeQuery($sql, [ $rcid, $onhold ], [ \PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_INT]);

        // determine if focus selection needed
        $focus_needed = ($p1_count > 2) * 1;
        $sql = 'insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "focus_needed", ?, now())';
        $h = $this->data->executeQuery($sql, [ $rcid, $focus_needed ], [ \PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_INT]);

        // set focus items if <3 P1
        if($p1_count < 3) {
            $c = 1;
            foreach($provider_priorities as $k => $v) {
                if($v == 1) {
                    $key = "focus_item_". $c++;
                    $sql = 'insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, ?, ?, now())';
                    $h = $this->data->executeQuery($sql, [ $rcid, $key, strtolower($k) ], [ \PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_INT]);
                }
            }
        }

        // Deal with all automatic pathway actions
        $this->execute_automatic_pathways($pathways);

        // clear for user marker in case set for assessors
        unset($_SESSION['for_user']);
    }

    private function execute_automatic_pathways($pathways) {
        // pathway outcomes not requiring user interaction
        // alc fix
        if($this->response_collection['responses'][392]['value'] + $this->response_collection['responses'][380]['value'] > 19) {
            $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "alcohol_gt19", 1, now())', [$this->response_collection['id']], [\PDO::PARAM_INT]);
        }
        foreach($pathways as $k => $v) {
            if($v['rule_outcome']) {
                switch($v['outcome_id']) {
                    case 18: // bmi < 18.6
                        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "low_bmi", 1, now())', [$this->response_collection['id']], [\PDO::PARAM_INT]);
                        break;
                    case 26:
                        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "homeless_nnpc", 1, now())', [$this->response_collection['id']], [\PDO::PARAM_INT]);
                        break;
                    case 27:
                        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "homeless_ld", 1, now())', [$this->response_collection['id']], [\PDO::PARAM_INT]);
                    case 28:
                        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "homeless_other", 1, now())', [$this->response_collection['id']], [\PDO::PARAM_INT]);
                        break;
                    case 29:
                        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "homeless_risk_nnru", 1, now())', [$this->response_collection['id']], [\PDO::PARAM_INT]);
                        break;
                    case 30:
                        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "homeless_risk_weda", 1, now())', [$this->response_collection['id']], [\PDO::PARAM_INT]);
                        break;
                    case 31:
                        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "homeless_risk_other", 1, now())', [$this->response_collection['id']], [\PDO::PARAM_INT]);
                        break;
                    case 32:
                        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "housing_pc", 1, now())', [$this->response_collection['id']], [\PDO::PARAM_INT]);
                        break;
                    case 34:
                        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "housing_pc55d", 1, now())', [$this->response_collection['id']], [\PDO::PARAM_INT]);
                        break;
                    case 39:
                        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "alcohol_gt19", 1, now())', [$this->response_collection['id']], [\PDO::PARAM_INT]);
                        break;
                }
            }
        }

    }

    private function evaluate_rule($ruleset) {
	    // todo make into one reusable method
        $choices = [];
        $h = $this->data->executeQuery('select * from question_choices');
        while($row = $h->fetch()) {
            $choices[$row['id']] = $row;
        }
        $ruleset_outcome = false;
        foreach($ruleset as $kr1 => $rule) {
            $rule_outcome = true;
            foreach($rule['conditions'] as $kr2 => $condition) {
                // Get terms
                // LHS
                if($condition['left_valuetype'] == 4) {
                    // question value
                    if(isset($this->response_collection['responses'][$condition['left_value']])) {
                        if(empty($this->response_collection['responses'][$condition['left_value']]['value'])) {
                            // Check for response id
                            $left_val = $this->response_collection['responses'][$condition['left_value']]['response_id'];
                            $left_val = $choices[$left_val]['value'];
                        } else {
                            $left_val = $this->response_collection['responses'][$condition['left_value']]['value'];
                        }
                    } else {
                        $left_val = null;
                    }
                } else {
                    $left_val = $condition['left_value'];
                }
                // RHS
                if($condition['right_valuetype'] == 4) {
                    // question value
                    if(isset($this->response_collection['responses'][$condition['right_value']])) {
                        if(empty($this->response_collection['responses'][$condition['right_value']]['value'])) {
                            // Check for response id
                            $right_val = $this->response_collection['responses'][$condition['right_value']]['response_id'];
                            $right_val = $choices[$right_val]['value'];
                        } else {
                            $right_val = $this->response_collection['responses'][$condition['right_value']]['value'];
                        }
                    } else {
                        $right_val = null;
                    }
                } else {
                    $right_val = $condition['right_value'];
                }
                switch($condition['ruletype_id']) {
                    case 1: // ==
                        $rule_outcome &= ( $left_val == $right_val );
                        break;
                    case 2: // like
                        $rule_outcome &= ( stripos($left_val, $right_val) !== false );
                        break;
                    case 3: // <
                        $rule_outcome &= ( $left_val > $right_val);
                        break;
                    case 4: // >
                        $rule_outcome &= ( $left_val < $right_val);
                        break;
                    case 5: // !=
                        $rule_outcome &= ( $left_val != $right_val);
                        break;
                    default:
                        break; // something wrong if here
                }
            } // end conditions
            $ruleset_outcome |= $rule_outcome;
        } // end rules
        return $ruleset_outcome;
    }
}
