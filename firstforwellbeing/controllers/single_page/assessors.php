<?php
namespace Concrete\Package\firstForWellbeing\Controller\SinglePage; // derived from top-level controller namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Concrete\Core\User\Group\Group as Group;
use Concrete\Core\User\UserList;
use Concrete\Core\User\UserInfo;
use Exception;
use stdClass;

class Assessors extends PageController {

	public $uID = null;
	public $usergroups = null;
	public $username = null;

	public function on_start() {
        $this->data = \Database::connection('ffwdata');
        $this->encdb = \Database::connection('ffwdata_e');
        $this->encryptor = \Core::make("helper/encryption");
        $u = new User();
        $this->uID = $u->getUserID(); // userid
        $this->username = $u->getUserName(); // userid
        $this->usergroups = [];
        foreach( $u->getUserGroups() as $v ) {
            $this->usergroups[] = Group::getById($v)->gName; // user's assigned groups
        }
        $this->set('uID', $this->uID);
        $this->set('username', $this->username);
        $this->set('usergroups', $this->usergroups);
        $displayname = UserInfo::getByID($this->uID);
        $this->set('displayname', $displayname);
		// inheritance
		parent::on_start();
	}

	public function view() {

		// get count of completed assessments
        $h = $this->data->executeQuery('select count(*) as cnt from response_collections where completed = 1 and deleted = 0');
        $h = $h->fetch();
        $total = $h['cnt'];
        $this->set('total', $total);
        $this->set('perpage', max(20, $_POST['number_per_page']));

        // Pagination
        if( isset($_POST['current_page']) and is_numeric($_POST['current_page']) ) {
            $currentPage = $_POST['current_page'];
        } else {
            $currentPage = 1;
        }
        if( isset($_POST['number_per_page']) and is_numeric($_POST['number_per_page']) and $_POST['number_per_page'] > 0 ) {
            $perpage = $_POST['number_per_page'];
        } else {
            $perpage = 20;
        }
        $start = $perpage * ($currentPage - 1);

        $filter = isset($_POST['view']) ? $_POST['view'] : 'assessor';
        if(in_array('Assessor Admins', $this->usergroups) && !isset($_POST['view'])){
            $filter = 'all';
        }

        // Filters to pass to DB
        if( !empty($_POST['custname']) ) {
            $entry_filter[] = array( 'field' => 'custname', 'operator' => 'like', 'value' => $_POST['custname'] ); // encrypted field, searched by php not sql
        }
        if( !empty($_POST['email']) ) {
            $entry_filter[] = array( 'field' => 'email', 'operator' => 'like', 'value' => $_POST['email'] ); // encrypted field, searched by php not sql
        }

        // get assessments
        $sql = 'select id, user_id, assigned_to, create_datetime, create_datetime <= (now() - interval 12 week) as over12weeks from response_collections where deleted = 0 and completed = 1';
        if( !empty($_POST['assessment_id']) ) {
            $sql .= ' and id = ' . (int)$_POST['assessment_id'];
        } else {
            // if not by assid then other filters make a difference
            if( !empty($_POST['customer_id']) ) {
                $sql .= ' and user_id = ' . (int)$_POST['customer_id'];
            }
            if( !empty($_POST['fromdate']) and preg_match('\d\d/\d\d/\d\d\d\d', $_POST['fromdate']) ) {
                $fromdate = preg_replace('#(\d+)/(\d+)/(\d+)#', "$3-$2-$1", $_POST['fromdate']);
                $sql .= ' and create_datetime >= "' . $fromdate . '"';
            }
            if( !empty($_POST['todate']) and preg_match('\d\d/\d\d/\d\d\d\d', $_POST['todate']) ) {
                $todate = preg_replace('#(\d+)/(\d+)/(\d+)#', "$3-$2-$1", $_POST['todate']);
                $sql .= ' and create_datetime <= "' . $todate . '"';
            }
            if( !empty($_POST['assessor_user_id']) ) {
                $sql .= ' and assigned_to = ' + (int)$_POST['assessor_user_id'];
            }
        }
        $h = $this->data->executeQuery($sql);
        $responses = [];
        while($row = $h->fetch()) {
            $responses[$row['id']] = $row;
        } // got list of filtered collections
        $ids = array_keys($responses);
        $ids_list = implode(', ', $ids);

        // get responses
        if(empty($_POST['custname']) and empty($_POST['email'])) {

        } else {
            // todo gotta search encrypted fields. Slow.
            // get encrypted asset ids
            $sql = 'select r.response_collection_id, r.response_enc_asset_id, q.data_handle from responses r join questions q on r.question_id = q.id where q.data_handle in ("firstname", "lastname", "email") and q.deleted = 0 and r.response_collection_id in (' . $ids_list . ')';
            $h = $this->data->executeQuery($sql);
            $enclist = [];
            $rclist = [];
            while($row = $h->fetch()) {
                $encids[$row['response_enc_asset_id']] = $row;
                $rclist[$row['response_collection_id']][$row['data_handle']] = $row['response_enc_asset_id'];
            }
            $encids = implode(', ', array_keys($encids));
            $h = $this->encdb->executeQuery('select * from assets where id in (' . $encids . ')');
            $assets = [];
            while($row = $h->fetch()) {
                $assets[$row['id']] = $row['asset'];
            }
            // filter
            $failures = [];
            foreach($rclist as $k => $v) {
                if(!empty($_POST['email'])) {
                    $asset = $this->encryptor->decrypt($assets[$v['email']]);
                    if( stripos($asset, $_POST['email']) !== false ) {
                        $failures[] = $k; // store rcid for later removal
                    }
                }
                if(!empty($_POST['custname'])) {
                    $asset = $this->encryptor->decrypt($assets[$v['firstname']]) . ' ' . $this->encryptor->decrypt($assets[$v['lastname']]);
                    if( stripos($asset, $_POST['custname']) === false ) {
                        $failures[] = $k; // store rcid for later removal
                    }
                }
            }
            // remove failed matches from ids array
            $ids = array_diff($ids, $failures); // remove failure ids from list
            $ids_list = implode(', ', $ids);
        }

        //retrieve by ids
        $sql = 'SELECT r.question_id, r.response_id, r.response_enc_asset_id, r.value, r.response_collection_id, q.data_handle, qc.value as qcvalue, qc.choice as qcchoice FROM responses r join questions q on r.question_id = q.id left join question_choices qc on r.response_id = qc.id WHERE r.response_collection_id IN (' . $ids_list . ')';
        $h = $this->data->executeQuery($sql);
        while ($row = $h->fetch()) {
            if($row['response_enc_asset_id'] > 0) {
                $h2 = $this->encdb->executeQuery('select asset from assets where id = ?', [$row['response_enc_asset_id']], [\PDO::PARAM_INT]);
                if($row2 = $h2->fetch()) {
                    $row['value'] = $this->encryptor->decrypt($row2['asset']);
                }
            }
            if(!empty($row['qcvalue'])) {
                $row['value'] = $row['qcvalue'];
            }
            $responses[$row['response_collection_id']][$row['data_handle']] = $row;
        }
        $responses = array_intersect_key($responses, array_flip($ids));
        $responses = array_reverse($responses, true); // we want descending order, array is numerically ordered until now
        $this->set('assessments', $responses);
    }

    public function show($id) {
	    if($id < 1) {
	        $this->redirect('/assessors');
        }
	    // get assessment data
        $h = $this->data->executeQuery('select * from response_collections where deleted = 0 and id = ?', [$id], [\PDO::PARAM_INT]);
	    if($response_collection = $h->fetch()) {
	        // get responses
            $h = $this->data->executeQuery('
              select r.*, q.data_handle, qc.value as qcvalue, qc.choice as qcchoice 
              FROM responses r 
                join questions q on r.question_id = q.id 
                left join question_choices qc on r.response_id = qc.id 
                join response_results rr on rr.response_collection_id = r.response_collection_id
              where r.response_collection_id = ?
              ', [$id], [\PDO::PARAM_INT]);
            while($row = $h->fetch()) {
                if($row['response_enc_asset_id'] > 0) {
                    // decrypt
                    $h2 = $this->encdb->executeQuery('select asset from assets where id = ?', [$row['response_enc_asset_id']], [\PDO::PARAM_INT]);
                    $h2 = $h2->fetch();
                    $row['value'] = $this->encryptor->decrypt($h2['asset']);
                } else if(!empty($row['response_id'])) {
                    // get get multiple choice instead
                    $row['value'] = $row['qcchoice'];
                }
                $response_collection['responses'][$row['data_handle']] = $row['value'];
            }
            // get results
            $h = $this->data->executeQuery('select * from response_results where response_collection_id = ?', [$id], [\PDO::PARAM_INT]);
            while($row = $h->fetch()) {
                $response_collection['results'][$row['result_key']] = $row['result_value'];
            }
            $this->set('assessment', $response_collection);

            // get assessors list
            $userlist = new UserList();
            $userlist->filterByGroup('FFW Advisors');
            $list = $userlist->getResults();
            $assessors = [];
            foreach( $list as $user) {
                $assessors[$user->getUserId()] = $user->getUserDisplayName();
            }
            asort($assessors);

            // get any assessor notes
            $sql = '
                select an.*, date_format(an.created_datetime, "%d-%m-%Y") as datepart, date_format(an.created_datetime, "%H:%i:%s") as timepart, fa.followup_action, toc.type_of_contact
                from assessor_notes an
                inner join response_collections rc on rc.id = an.response_collection_id
                inner join followup_actions fa on an.followup_action = fa.id
                inner join types_of_contact toc on an.type_of_contact = toc.id
                where rc.id = ?
                order  by an.created_datetime desc
            ';
            $h = $this->data->executeQuery($sql, [$id], [\PDO::PARAM_INT]);
            $assessor_notes = [];
            while($row = $h->fetch()) {
                // get username
                $u = UserInfo::getByID($row['created_by']);
                $row['display_name'] = $u->getUserDisplayName();
                $assessor_notes[] = $row;
            }
            $this->set('assessor_notes', $assessor_notes);

            // get types of contact & follouwp actions list
            $h = $this->data->executeQuery('select * from types_of_contact order by display');
            $types_of_contact = [];
            while($row = $h->fetch()) {
                $types_of_contact[] = $row;
            }
            $h = $this->data->executeQuery('select * from followup_actions order by followup_action');
            $followup_actions = [];
            while($row = $h->fetch()) {
                $followup_actions[] = $row;
            }
            $this->set('types_of_contact', $types_of_contact);
            $this->set('followup_actions', $followup_actions);

            // get list of FFW Assessors
            $userList = new UserList();
            $userList->filterByGroup('FFW Advisors');
            $userlist->sortByUserName();
            $list = $userlist->getResults();
            $assessors = [];
            foreach($list as $user) {
                $assessors[$user->getUserDisplayName()] = $user->getUserID();
            }
            $this->set('assessors', $assessors);

            // output the 'show' view
            $this->render('assessors_show');
        } else {
	        // no assessment found
            // todo
        }
    }

    public function saveassessornote() {
        if(!isset($_POST['aid'], $_POST['user_note'])) {
            $this->redirect('/assessors');
        }
        // encrypt user_note
        $note = $this->encryptor->encrypt($_POST['user_note']);
        $h = $this->encdb->executeQuery('insert into assets (asset) values (?)', [$note], [\PDO::PARAM_STR]);
        $id = $this->encdb->lastInsertId();
        $sql = 'insert into assessor_notes (response_collection_id, created_datetime, note_enc_asset_id, type_of_contact, followup_action, created_by, visible_to) values (?, now(), ?, ?, ?, ?, ?)';
        $params = [ $_POST['aid'], $id, $_POST['type_of_contact'], $_POST['followup_action'], $this->uID, implode(',', $_POST['provider']) ];
        $t = [ \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_STR ];
        $h = $this->data->executeQuery($sql, $params, $t);
        $this->redirect('/assessors/show/' . $_POST['aid']);
    }

    public function userprofile($custid) {
	    $this->set('custid', $custid);
	    $h = $this->data->executeQuery('select * from user_profiles where user_id = ?', [$custid], [\PDO::PARAM_INT]);
	    if($h = $h->fetch()) {
	        $hminus = array_flip($h);
            $ids = '"' . implode('","', array_keys($hminus)) . '"';
	        $h1 = $this->encdb->executeQuery('select * from assets where id in (' . $ids . ')');
	        while($row = $h1->fetch()) {
	            $h[$hminus[$row['id']]] = $this->encryptor->decrypt($row['asset']);
            }
        } else {
	        $this->redirect('/assessors');
        }
        $this->set('user_data', $h);
        $this->render('user_profile');
    }

    public function changeassessor($assid, $rcid) {
	    if($assid > 0 and $rcid > 0) {
	        $h = $this->data->executeQuery('update response_collections set assigned_to = ? where id = ?', [$assid, $rcid], [\PDO::PARAM_INT, \PDO::PARAM_INT]);
        }
        $this->redirect('/assessors/show/' . $rcid);
    }

}
