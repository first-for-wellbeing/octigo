<?php
namespace Concrete\Package\firstForWellbeing\Controller\SinglePage\Ffw; // derived from top-level controller namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Exception;
use stdClass;

class Ajax extends PageController {

	private $data = null;
	private $uID = null;

	public function on_start() {
		$this->data = \Database::connection('ffwdata');
		$u = new User();
		$this->uID=$u->getUserID();
		// inheritance
		parent::on_start();
	}

	public function view() {
		die(); // there is no default
	}

	public function getchoices() {
		header('Content-type: application/json');
		if(!isset($_POST['id'])) {
			echo json_encode( [ 'status' => 'error' ] );
			die();
		}
		$qid = (int)$_POST['id'];
		if($qid < 1) {
			return json_encode( ['status' => 'error'] );
		}
		// 6, 7, 8, 9, 10, 13 = dropdown, tickbox, radio, yes/no, radio icons, slider
		$h = $this->data->executeQuery('select * from questions q left join question_choices qc on q.id = qc.question_id where q.id = ? and q.question_type in (6, 7, 8, 9, 10, 13) order by qc.display', [$qid], [ \PDO::PARAM_STR ]);
		$result = [ 'data' => [] ];
		while( $row = $h->fetch() ) {
			if($row['question_type'] == 9) {
				// Yes/No special case
				$result['data'] = [ [ 'choice' => 'Yes', 'value' => '1' ], [ 'choice' => 'No', 'value' => '2' ] ];
				break; // all done
			}
			// get choices
			$result['data'][] = $row;	
		}
		$result['status'] = 'ok';
		echo json_encode($result);
		die(); // die needed to prevent C5 outputting UI
	}

}
