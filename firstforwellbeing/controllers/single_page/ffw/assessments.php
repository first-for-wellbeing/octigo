<?php
namespace Concrete\Package\firstForWellbeing\Controller\SinglePage\Ffw; // derived from top-level controller namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Exception;
use stdClass;

class Assessments extends PageController {

	private $data = null;
	private $uID = null;

	public function on_start() {
		$this->data = \Database::connection('ffwdata');
		$u = new User();
		$this->uID=$u->getUserID();
		// required includes
        //$this->requireAsset('javascript', 'firstforwellbeing_sortable');
		// inheritance
		parent::on_start();
	}

	public function view($pagenum = 1, $perpage = 20, $editid = null) {
		// Get list of assessments
		$params = [ ($pagenum - 1) * $perpage, (int)$perpage ];
		$h = $this->data->executeQuery('select a.id, a.name, a.description, count(at.id) as cnt from assessments a left join assessment_themes at on a.id = at.assessment_id where a.deleted = 0 group by a.id order by a.name limit ?, ?', $params, [\PDO::PARAM_INT, \PDO::PARAM_INT]); // NB 'limit' need int type specifying to avoid vals being quoted
		$this->set('assessments_list', $h);
		// Get list of questions
		$this->set('themes_list', $this->data->executeQuery('select id, name from themes where deleted = 0 order by name'));
		// get total theme count
		$result = $this->data->executeQuery('select count(*) as cnt from assessments where deleted = 0');
		$tot = $result->fetch();
		$tot = $tot['cnt'];
		// set some vars for the view
		$this->set('perpage', $perpage);
		$this->set('pagenum', $pagenum);
		$this->set('totalrecs', $tot);
		// is editing required?
		$editrecord = null;
		if($editid !== null and is_numeric($editid)) {
			// get assessment
			$h = $this->data->executeQuery('select * from assessments where id = ?', [ $editid ], [ \PDO::PARAM_INT ]);
			$editrecord = $h->fetch();
			// get assessment themes
			$h = $this->data->executeQuery('select * from assessment_themes where assessment_id = ? order by display', [ $editid ], [ \PDO::PARAM_INT ]);
			while($row = $h->fetch()) {
				$editrecord['themes'][] = $row;
			}

		}
		$this->set('editrecord', $editrecord);
	}
	
	public function save() {
		if( !isset($_POST['atitle'], $_POST['adesc']) ) {
			// redirect to default
			$this->redirect('/ffw/assessments');
		}
		// save assessment
		$editid = $_POST['editid'] + 0;
		if($editid) {
			// mark old version deleted, continue creating replacement
			$sql = "update assessments set deleted = 1 where id = ?";
			$h = $this->data->executeQuery($sql, [ $editid ], [ \PDO::PARAM_INT ]);
		}
		$sql = 'insert into assessments (name, description, owner, creator) values (?, ?, ?, ?)';
		$params = [ $_POST['atitle'], $_POST['adesc'], $this->uID, $this->uID ];
		$this->data->executeQuery($sql, $params, [\PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_INT]);
		$qid = $this->data->lastInsertId();
		// save any choices
		if( count($_POST['qid']) > 0 ) { // themes provided
			$sql = 'insert into assessment_themes (assessment_id, theme_id, display) values ';
			$sql .= str_repeat( '(?, ?, ?), ', count($_POST['qid']) ); // add params
			$sql = substr($sql, 0, strlen($sql) - 2); // remove trailing comma
			$params = [];
			for($a = 0; $a < count($_POST['qid']); $a++) {
				$params = array_merge($params, [ $qid, $_POST['qid'][$a], $a ]);
			}
			$this->data->executeQuery($sql, $params, array_fill(0, count($_POST['qid']) * 3, \PDO::PARAM_INT) );
			$h = $this->data->executeQuery('delete from assessment_themes where assessment_id = ?', [$editid], [\PDO::PARAM_INT] );
		}
		$this->redirect($this->action(''));
	}

	public function delete($id = null) {
		$sql = 'update assessments set deleted = 1 where id = ?';
		$this->data->executeQuery( $sql, [$id], [\PDO::PARAM_INT] );
		$this->redirect( $this->action('') );
	}

}
