<?php
namespace Concrete\Package\firstForWellbeing\Controller\SinglePage\Ffw; // derived from top-level controller namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Exception;
use stdClass;

class PathwayRules extends PageController {

	private $data = null;
	private $uID = null;

	public function on_start() {
		$this->data = \Database::connection('ffwdata');
		$u = new User();
		$this->uID=$u->getUserID();
		// required includes
		$this->requireAsset('javascript', 'ffw_admin_sortable');
		// inheritance
		parent::on_start();
	}

	public function view($pid = null) {
		if(!is_numeric($pid)) {
			return;
		}
		// Get theme info
		$h = $this->data->executeQuery('select * from pathways where id = ?', [ $pid ], [\PDO::PARAM_INT]);
		if($row = $h->fetch()) {
			$this->set('rulefor', $row['name']);
			$this->set('athemeid', $pid); // used in rules form
			// Check for existing rules
			$ruleset = $this->data->executeQuery('select rule_id from pathway_rulesets where pathway_id = ?', [ $pid ], [\PDO::PARAM_INT]);
			$list = $rules = [];
			while( $row = $ruleset->fetch() ) {
				$list[ $row['rule_id'] ] = 1;
			}
			// store top-level ruleset rules
			$this->set('ruleset', $list);
			if(count($list) > 0) { // there is at least one rule
				// fetch rule tree
				$count = 1;
				while($count > 0) {
					$count = 0;
					$list = implode(',', array_keys($list)); // using keys minimises dupe entries
					$r = $this->data->executeQuery('
                        select r.*, rt.name, rvl.name as left_name, rvr.name as right_name, ql.title as left_title, qr.title as right_title, qcl.choice as left_choice, qcr.choice as right_choice
                        from rules r 
                            join ruletypes rt on r.ruletype_id = rt.id
                            join rule_valuetypes rvl on rvl.id = r.left_valuetype
                            join rule_valuetypes rvr on rvr.id = r.right_valuetype
                            left join questions ql on ql.id = r.left_value
                            left join questions qr on qr.id = r.right_value
                            left join question_choices qcl on r.left_value = qcl.question_id and r.right_value = qcl.value
                            left join question_choices qcr on r.right_value = qcr.question_id and r.left_value = qcr.value
						where
							r.id in (' . $list . ')'
					);
					$list = [];
					while($row = $r->fetch() ) {
						$list[$row['true_path']] = 1;
						//$list[$row['false_path']] = 1; // only have a true path for now. 'Else' path might be overkill for users
						$rules[ $row['id'] ] = $row;
						$count++; // because PDO can't be relied on to return select mySQL rowcounts with rowCount()
					}
				}
				// Should have list of all rules in the tree now, in $rules
			}
			$this->set('rules', $rules);
			// Retrieve ruletypes
			$h = $this->data->executeQuery('select * from ruletypes order by name');
			$this->set('ruletypes_list', $h);
			// Get assessment theme id for this assessment
            // TODO not nultiple assessment friendly
            $h = $this->data->executeQuery('select id from assessment_themes where assessment_id in (select id from assessments where deleted = 0)');
            $h = $h->fetch();
            $athemeid = $h['id'];
			// Retrieve all assessment questions for this assessment
			$h = $this->data->executeQuery('
				select distinct q.id, q.title, qt.type_name
				from assessment_themes at
					join theme_questions tq on tq.theme_id = at.theme_id
					join questions q on q.id = tq.question_id
					join question_types qt on q.question_type = qt.id
				where
					q.deleted = 0
					and at.assessment_id = (select assessment_id from assessment_themes at1 join assessments a1 on a1.id = at1.assessment_id where a1.deleted = 0 and at1.id = ?)
				order by q.title
				;', [$athemeid], [\PDO::PARAM_INT] );
			$this->set('questions_list', $h);
			// Retrieve question operators
			$h = $this->data->executeQuery('select * from ruletypes order by name');
			$this->set('ruletypes_list', $h);
			// Retrieve rule valuetypes
			$h = $this->data->executeQuery('select -id as id, name from rule_valuetypes where id not in (4) order by name'); // note negative id
			$this->set('rulevaluetypes_list', $h);
		}
	}

	public function add() {
		if( !isset($_POST['var_left'], $_POST['var_right'], $_POST['operator'], $_POST['athemeid']) ) {
			// redirect to default
			$this->redirect('/ffw/pathway_rules.' . $_POST['athemeid']);
		}
		// add condition
		if( $_POST['var_left'] == 0 or $_POST['var_right'] == 0 or $_POST['operator'] == 0 ) {
			// redirect to default
			$this->redirect('/ffw/pathway_rules/' . $_POST['athemeid']);
		}
		// determine left & right vars
		// left
		if($_POST['var_left'] > 0) {
			$type_left = 4; // question id
		} else {
			$type_left = -$_POST['var_left']; // restore +ve sign
			$_POST['var_left'] = $_POST['manual_left'];
		}
		// right
		if($_POST['var_right'] > 0) {
			$type_right = 4; // question id
		} else {
			$type_right = -$_POST['var_right']; // restore +ve sign
			$_POST['var_right'] = $_POST['manual_right'];
		}
		// add condition
		$sql = 'insert into rules (ruletype_id, left_value, right_value, left_valuetype, right_valuetype) values (?, ?, ?, ?, ?)';
		$params = [ $_POST['operator'], $_POST['var_left'], $_POST['var_right'], $type_left, $type_right ];
		$h = $this->data->executeQuery( $sql, $params, [ \PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_INT ]);
		$id = $this->data->lastInsertId();
		// connect condition with any 'parent' condition
		if( $_POST['parent_true'] > 0 ) {
			$h = $this->data->executeQuery('update rules set true_path = ? where id = ?', [ $id, $_POST['parent_true'] ], [ \PDO::PARAM_INT, \PDO::PARAM_INT ] );
		}
		//if( $_POST['parent_false'] > 0 ) { /// 'Else' path not used for now
			//$h = $this->data->executeQuery('update rules set false_path = ? where id = ?', [ $id, $_POST['parent_false'] ], [ \PDO::PARAM_INT, \PDO::PARAM_INT ] );
		//}
		// deal with rulesets
		if($_POST['parent_true'] == 0 and $_POST['parent_false'] == 0) { // brand new condition not attached to any other so needs a ruleset making
			// create new ruleset and put this condition in it
			$sql = 'insert into pathway_rulesets (rule_id, pathway_id) values (?, ?)';
			$params = [ $id, $_POST['athemeid'] ];
			$h = $this->data->executeQuery( $sql, $params, [ \PDO::PARAM_INT, \PDO::PARAM_INT ] );
		}

		// redirect to kill reload-repost
		$this->redirect($this->action($_POST['athemeid']));
	}

	public function delete($id = null, $athemeid = null, $truepath = null) {
		if($id === null or !is_numeric($id) or $athemeid == null or !is_numeric($athemeid) or $truepath == null or !is_numeric($truepath)) {
			$this->redirect('/ffw/pathway_rules/' . $athemeid);
		}
		// update this rule's parent's true_path to the true_path of this rule to bypass it, then delete it
		$sql = 'update rules set true_path = ? where true_path = ?';
		$h = $this->data->executeQuery($sql, [$truepath, $id], [ \PDO::PARAM_INT, \PDO::PARAM_INT ]);
		if($h->rowCount() == 0) {
			if($truepath == 0) { // was first rule and has no subsequent rules
				// remove refs to this rule if it's the last rule in the ruleset
				$h = $this->data->executeQuery('delete from pathway_rulesets where rule_id = ?', [$id], [ \PDO::PARAM_INT ]);
			} else {
				// must be the first row in a sequence of conditions, slightly different action needed
				$sql = 'update pathway_rulesets set rule_id = ? where rule_id = ?';
				$params = [ $truepath, $id ];
				$h = $this->data->executeQuery($sql, $params, [ \PDO::PARAM_INT, \PDO::PARAM_INT ]);
			}
		}
		$h = $this->data->executeQuery('delete from rules where id = ?', [$id], [ \PDO::PARAM_INT ]);
		$this->redirect('/ffw/pathway_rules/' . $athemeid);
	}

}
