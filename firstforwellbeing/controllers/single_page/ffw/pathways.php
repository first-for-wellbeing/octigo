<?php
namespace Concrete\Package\firstForWellbeing\Controller\SinglePage\Ffw; // derived from top-level controller namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Concrete\Attribute\Select\Controller as SelectAttributeTypeController;
use Concrete\Core\Attribute\Type as AttributeType;
use Exception;
use stdClass;

class Pathways extends PageController {

    private $data = null;
    private $uID = null;

    public function on_start() {
        $this->data = \Database::connection('ffwdata');
        $u = new User();
        $this->uID=$u->getUserID();
        // required includes
        $this->requireAsset('javascript', 'firstforwellbeing_sortable');
        // inheritance
        parent::on_start();
    }

    public function view($pagenum = 1, $perpage = 20, $editid = null) {
        // Get list providers from SELECT attribute created at install
        $attr = \Concrete\Core\Attribute\Key\UserKey::getByHandle('userprovidertype');
        $atc = $attr->getController();
        $options = $atc->getOptions();
        $opts = [];
        foreach($options as $opt) {
            $opts[] = $opt->getSelectAttributeOptionValue();
        }
        sort($opts);
        $this->set('providers', $opts);

        // Outcomes list
        $h = $this->data->executeQuery('select * from outcomes order by name');
        $this->set('outcomes', $h);

        // Pathways list
        $params = [ ($pagenum - 1) * $perpage, (int)$perpage ];
        $h = $this->data->executeQuery('select p.*, count(pr.id) as cnt from pathways p left join pathway_rulesets pr on p.id = pr.pathway_id where p.deleted = 0 group by p.id order by name limit ?, ?', $params, [\PDO::PARAM_INT, \PDO::PARAM_INT]); // NB 'limit' need int type specifying to avoid vals being quoted
        $this->set('pathways_list', $h);
        // get total pathway count
        $result = $this->data->executeQuery('select count(*) as cnt from pathways where deleted = 0');
        $tot = $result->fetch();
        $tot = $tot['cnt'];
        // set some vars for the view
        $this->set('perpage', $perpage);
        $this->set('pagenum', $pagenum);
        $this->set('totalrecs', $tot);
        // is editing required?
        $editrecord = null;
        if($editid !== null and is_numeric($editid)) {
            // get pathway
            $h = $this->data->executeQuery('select * from pathways where id = ?', [ $editid ], [ \PDO::PARAM_INT ]);
            $editrecord = $h->fetch();
        }
        $this->set('editrecord', $editrecord);
    }

    public function save() {
        if( !isset($_POST['atitle'], $_POST['adesc'], $_POST['qprovider']) ) {
            // redirect to default
            $this->redirect('/ffw/pathways');
        }
        // save assessment
        $editid = $_POST['editid'] + 0;
        if($editid) {
            // mark old version deleted, continue creating replacement
            $sql = "update pathways set deleted = 1 where id = ?";
            $h = $this->data->executeQuery($sql, [ $editid ], [ \PDO::PARAM_INT ]);
            // TODO update rules
        }
        $sql = 'insert into pathways (name, description, provider_type, outcome_id) values (?, ?, ?, ?)';
        $params = [ $_POST['atitle'], $_POST['adesc'], $_POST['qprovider'], $_POST['qoutcome'] ];
        $this->data->executeQuery($sql, $params, [\PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT]);
        $qid = $this->data->lastInsertId();
        // update rulesets
        $h = $this->data->executeQuery('update pathway_rulesets set pathway_id = ? where pathway_id = ?', [$qid, $editid], [\PDO::PARAM_INT, \PDO::PARAM_INT]);
        $this->redirect($this->action(''));
    }

    public function delete($id = null) {
        $sql = 'update pathways set deleted = 1 where id = ?';
        $this->data->executeQuery( $sql, [$id], [\PDO::PARAM_INT] );
        $this->redirect( $this->action('') );
    }

}
