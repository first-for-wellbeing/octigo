<?php
namespace Concrete\Package\firstForWellbeing\Controller\SinglePage\Ffw; // derived from top-level controller namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Exception;
use stdClass;

class Questions extends PageController {

	private $data = null;
	private $uID = null;

	public function on_start() {
		$this->data = \Database::connection('ffwdata');
		$u = new User();
		$this->uID=$u->getUserID();
		// required includes
		$this->requireAsset('javascript', 'ffw_admin_sortable');
		$this->requireAsset('css', 'ffw_admin_css');
		// inheritance
		parent::on_start();
	}

	public function view($pagenum = 1, $perpage = 20, $editid = null) {
		// Make file list object to list icons if needed
		$set = \Concrete\Core\File\Set\Set::getByName('sprites');
		$icons = new \Concrete\Core\File\FileList();
		$icons->filterBySet($set);
		$icons->sortByFilenameAscending();
		$this->set('icons', $icons->getResults());
		// Get list of questions
		$params = [ ($pagenum - 1) * $perpage, (int)$perpage ];
		$sql = '
          select q.*, qt.type_name, count(qc.id) AS cnt 
          from questions q join question_types qt on qt.id = q.question_type
            left join question_choices qc on qc.question_id = q.id
          where 
            q.deleted = 0 
          group by q.id 
          order by q.title 
          limit ?, ?
        ';
		$h = $this->data->executeQuery($sql, $params, [\PDO::PARAM_INT, \PDO::PARAM_INT]); // NB 'limit' need int type specifying to avoid vals being quoted
		$this->set('questions_list', $h);
		// Get all questions for calc field UI
        $h = $this->data->executeQuery('select * from questions where deleted = 0 order by title');
        $this->set('questions', $h);
		// Get list of question types
		$this->set('questions_types_list', $this->data->executeQuery('select * from view_questions_types_list'));
		// get total question count
		$result = $this->data->executeQuery('select count(*) as cnt from questions where deleted = 0');
		$tot = $result->fetch();
		$tot = $tot['cnt'];
		// set some vars for the view
		$this->set('perpage', $perpage);
		$this->set('pagenum', $pagenum);
		$this->set('totalrecs', $tot);
		// is editing required?
		$editrecord = null;
		if($editid !== null and is_numeric($editid)) {
			// get question
			$h = $this->data->executeQuery('select q.*, qt.type_name from questions q join question_types qt on q.question_type = qt.id where q.id = ?', [ $editid ], [ \PDO::PARAM_INT ]);
			$editrecord = $h->fetch();
			// get question choices
			$h = $this->data->executeQuery('select * from question_choices where question_id = ?', [ $editid ], [ \PDO::PARAM_INT ]);
			while($row = $h->fetch()) {
				$editrecord['choices'][] = $row;
			}
		}
		$this->set('editrecord', $editrecord);
		// Get special validation types
		$h = $this->data->executeQuery('select * from special_validation order by name');
		$this->set('special_validation', $h);
	}
	
	public function save() {
		if( !isset($_POST['qtitle'], $_POST['qtext'], $_POST['qstyle'], $_POST['qmandatory'], $_POST['qmandatorya']) ) {
			// redirect to default
			$this->redirect('/ffw/questions');
		}
		if( in_array($_POST['qstyle'], [6, 7, 8, 13]) ) { // dropdown, radio or tickbox or slider
			// check we have options provided
			if(!isset($_POST['qchoice'], $_POST['qvalue']) or count($_POST['qchoice']) < 1 or count($_POST['qchoice']) != count($_POST['qvalue']) ) {
				// bad choice data, redirect
				$this->redirect('/ffw/questions');
			}
		}
		// Should be ok to save to DB by this point
		$editid = $_POST['editid'] + 0;
		// save question
		if($_POST['qspecial'] == '') {
			$_POST['qspecial'] = null;
		}
		$sql = 'insert into questions (title, question_text, question_type, creator, mandatory, assessor_mandatory, derived_from, default_value, special_validation, placeholder, show_label, invisible, css, validation_message, profile_mapping, data_handle, label_pos, read_only) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
		$params = [ $_POST['qtitle'], $_POST['qtext'], $_POST['qstyle'], $this->uID, $_POST['qmandatory'], $_POST['qmandatorya'], $editid, $_POST['qdefault'], $_POST['qspecial'], $_POST['qplaceholder'], $_POST['qshowlabel'], $_POST['qinvisible'], $_POST['qcss'], $_POST['qvalmsg'], $_POST['qprofile'], $_POST['qhandle'], $_POST['qlabelpos'], $_POST['qreadonly'] ];
		$this->data->executeQuery($sql, $params, [ \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_INT ]);
		$qid = $this->data->lastInsertId();
		if($editid) {
			// if an edit then 'renumber' old associations to the 'deleted' entry to the new one
			$sql = "update theme_questions set question_id = ? where question_id = ?";
			$h = $this->data->executeQuery($sql, [ $qid, $editid ], [ \PDO::PARAM_INT, \PDO::PARAM_INT ]);
			// mark old version deleted, continue creating replacement
			$sql = "update questions set deleted = 1 where id = ?";
			$h = $this->data->executeQuery($sql, [ $editid ], [ \PDO::PARAM_INT ]);
			// update calc fields using old id
            $sql = 'update questions set default_value = replace(default_value, ?, ?) where deleted = 0 and question_type = 15 and default_value like ?';
            $params = [ '{' . $editid . '}', '{' . $qid . '}', '%{' . $editid . '}%' ];
            $h = $this->data->executeQuery($sql, $params, [ \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR ]);
		}
		// save any choices
		if( in_array($_POST['qstyle'], [6, 7, 8, 10, 13]) ) { // dropdown, radio, tickbox, radio icons, slider
			$sql = 'insert into question_choices (question_id, choice, value, display, icon) values ';
			$sql .= str_repeat( '(?, ?, ?, ?, ?), ', count($_POST['qchoice']) ); // add params
			$sql = substr($sql, 0, strlen($sql) - 2);
			$params = [];
			for($a = 0; $a < count($_POST['qchoice']); $a++) {
				$params = array_merge($params, [ $qid, $_POST['qchoice'][$a], $_POST['qvalue'][$a], $a, $_POST['qicon'][$a] ]);
			}
			$this->data->executeQuery($sql, $params, [ \PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_INT ]);
		}
		// update rules referring to this question
		$h = $this->data->executeQuery('select id from theme_questions where question_id = ?', [ $qid ], [ \PDO::PARAM_INT ] );
		while( $tqid = $h->fetch()) {
			$h2 = $this->data->executeQuery('update rules set left_value = ? where left_value = ? and left_valuetype = 4', [ $qid, $editid ] , [ \PDO::PARAM_INT, \PDO::PARAM_INT ]);
			$h2 = $this->data->executeQuery('update rules set right_value = ? where right_value = ? and right_valuetype = 4', [ $qid, $editid ] , [ \PDO::PARAM_INT, \PDO::PARAM_INT ]);
		}
		$this->redirect($this->action(''));
	}

	public function delete($id = null) {
		$sql = 'update questions set deleted = 1 where id = ?';
		$this->data->executeQuery( $sql, [$id], [\PDO::PARAM_INT] );
		$this->redirect( $this->action('') );
	}
}
