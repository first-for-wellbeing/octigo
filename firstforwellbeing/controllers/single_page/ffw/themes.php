<?php
namespace Concrete\Package\firstForWellbeing\Controller\SinglePage\Ffw; // derived from top-level controller namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Exception;
use stdClass;

class Themes extends PageController {

	private $data = null;
	private $uID = null;

	public function on_start() {
		$this->data = \Database::connection('ffwdata');
		$u = new User();
		$this->uID = $u->getUserID();
		// required includes
		$this->requireAsset('javascript', 'firstforwellbeing_sortable');
		// inheritance
		parent::on_start();
	}

	public function view($pagenum = 1, $perpage = 20, $editid = null) {
		// Get list of themes
		$params = [ ($pagenum - 1) * $perpage, (int)$perpage ];
		$h = $this->data->executeQuery('
			select t.id, t.name, t.description, count(tq.id) as cnt
			from themes t
				left join theme_questions tq on t.id = tq.theme_id
			where
				t.deleted = 0
			group by t.id
			order by name limit ?, ?
			', $params, [\PDO::PARAM_INT, \PDO::PARAM_INT]); // NB 'limit' need int type specifying to avoid vals being quoted
		$this->set('themes_list', $h);
		// Get list of questions
		$this->set('questions_list', $this->data->executeQuery('select id, title, question_text, data_handle from questions where deleted = 0 order by title'));
		// get total theme count
		$result = $this->data->executeQuery('select count(*) as cnt from themes where deleted = 0');
		$tot = $result->fetch();
		$tot = $tot['cnt'];
		// set some vars for the view
		$this->set('perpage', $perpage);
		$this->set('pagenum', $pagenum);
		$this->set('totalrecs', $tot);
		// is editing required?
		$editrecord = null;
		if($editid !== null and is_numeric($editid)) {
			// get theme
			$h = $this->data->executeQuery('select * from themes where id = ?', [ $editid ], [ \PDO::PARAM_INT ]);
			$editrecord = $h->fetch();
			// get theme questions
			$h = $this->data->executeQuery('select * from theme_questions where theme_id = ? order by display', [ $editid ], [ \PDO::PARAM_INT ]);
			while($row = $h->fetch()) {
				$editrecord['questions'][] = $row;
			}
		}
		$this->set('editrecord', $editrecord);
	}
	
	public function save() {
		if( !isset($_POST['ttitle'], $_POST['tdesc']) ) {
			// redirect to default
			$this->redirect('/dashboard/ffw/themes');
		}
		// save theme
		$editid = (int)$_POST['editid'] + 0;
		if($editid) {
			// mark old version deleted, continue creating replacement
			$sql = "update themes set deleted = 1 where id = ?";
			$h = $this->data->executeQuery($sql, [ $editid ], [ \PDO::PARAM_INT ]);
		}
		$sql = 'insert into themes (name, description, owner, creator, derived_from) values (?, ?, ?, ?, ?)';
		$params = [ $_POST['ttitle'], $_POST['tdesc'], $this->uID, $this->uID, $editid ];
		$this->data->executeQuery($sql, $params, [\PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT]);
		$qid = $this->data->lastInsertId();
		// if an edit then 'renumber' old associations to the 'deleted' entry to the new one
		$oldtq = [];
		if($editid) {
			$sql = "update assessment_themes set theme_id = ? where theme_id = ?";
			$h = $this->data->executeQuery($sql, [ $qid, $editid ], [ \PDO::PARAM_INT, \PDO::PARAM_INT ]);
			// get any existing rule associations
			$h = $this->data->executeQuery('select * from theme_questions where theme_id = ?', [$editid], [\PDO::PARAM_INT] );
			while($row = $h->fetch()) {
				$oldtq[$row['question_id']] = $row;
			}
		}
		// save any choices
		if( count($_POST['qid']) > 0 ) { // questions provided
			$sql = 'insert into theme_questions (theme_id, question_id, display) values (?, ?, ?)';
			for($a = 0; $a < count($_POST['qid']); $a++) {
				$params =  [ $qid, $_POST['qid'][$a], $a ];
				$this->data->executeQuery($sql, $params, [ \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT ] );
				$tqid = $this->data->lastInsertId();
				// do we need to remap rules?
				if(count($oldtq) > 0) { // there are some old rules to check
					if(isset( $oldtq[ $_POST['qid'][$a] ] )) { // recreate corresponding rule
						// get previous tq id
						$row = $oldtq[$_POST['qid'][$a]];
						// update qr entry
						$sql2 = 'update question_rulesets set theme_question_id = ? where theme_question_id = ?';
						$h2 = $this->data->executeQuery($sql2, [ $tqid, $row['id'] ], [ \PDO::PARAM_INT, \PDO::PARAM_INT ] );
					}
				}
			}
			foreach($oldtq as $row) {
				// remove old references
				$h = $this->data->executeQuery('delete from question_rulesets where theme_question_id = ?', [$row['id']], [ \PDO::PARAM_INT] );
				$h = $this->data->executeQuery('delete from theme_questions where id = ?', [$row['id']], [ \PDO::PARAM_INT] );
			}
		}
		$this->redirect($this->action(''));
	}

	public function delete($id = null) {
		$sql = 'update themes set deleted = 1 where id = ?';
		$this->data->executeQuery( $sql, [$id], [\PDO::PARAM_INT] );
		$this->redirect( $this->action('') );
	}
}
