<?php
namespace Concrete\Package\firstForWellbeing\Controller\SinglePage; // derived from top-level controller namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Concrete\Core\User\Group\Group as Group;
use Exception;
use stdClass;

class Loginrouter extends PageController {

	private $data = null;
	private $uID = null;

	public function on_start() {
		$this->data = \Database::connection('ffwdata');
		$u = new User();
		$this->uID=$u->getUserID();
		$usergroups = [];
		foreach( $u->getUserGroups() as $v ) {
			$usergroups[] = Group::getById($v)->gName; // user's assigned groups
		}
		$usergroups = array_flip($usergroups);
		if(isset($usergroups['System Admins'])) {
			$this->redirect('/dashboard');
		}
		if(isset($usergroups['Assessor Admins'])) {
			$this->redirect('/assessors');
		}
		if(isset($usergroups['FFW Advisors'])) {
			$this->redirect('/assessors');
		}
		if(isset($usergroups['Providers'])) {
			$this->redirect('/providers');
		}
		$this->redirect('/myassessment');

		// inheritance
		parent::on_start();
	}

	public function view() {
		die(); // there is no default
	}

}
