<?php
namespace Concrete\Package\firstForWellbeing\Controller\SinglePage; // derived from top-level controller namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\User\UserInfo;
use Concrete\Core\Page\Controller\PageController;
use Concrete\Core\User\Group\Group as Group;
use Exception;
use stdClass;

class Myassessment extends PageController {

    public $uID = null;
    public $usergroups = null;
    public $username = null;

    public function on_start() {
        // Get DB connection
        $this->data = \Database::connection('ffwdata');
        $this->encdb = \Database::connection('ffwdata_e');
        $this->encryptor = \Core::make("helper/encryption");
        //$this->assessment = new \FfwAssessmentLibs\FfwAssessment();

        // Get User info
        $u = new User();
        $this->uID = $u->getUserID(); // userid
        $this->username = $u->getUserName(); // userid
        $this->usergroups = [];
        foreach( $u->getUserGroups() as $v ) {
            $this->usergroups[] = Group::getById($v)->gName; // user's assigned groups
        }
        $this->encrypted_result_fields = [
            'user_notes_smoking',
            'user_notes_finance',
            'user_notes_alcohol',
            'user_notes_weight',
            'user_notes_emotional',
            'user_notes_social',
            'user_notes_employment',
            'user_notes_housing',
            'user_notes_coldhomes'
        ];

        // inheritance
        parent::on_start();
    }

    public function view($assessment_id = null) {
        // get response results
        $this->set('uID', $this->uID);
        $this->set('username', $this->username);
        $this->set('usergroups', $this->usergroups);
        // postcodes
        // Look for completed assessment
        // search by userid or assessment_id if user is assessor
        if(in_array('FFW Advisors', $this->usergroups)) {
            if($assessment_id > 0) {
                $sql = "
                    SELECT 
                        rc.*, date_format(cast(create_datetime + INTERVAL 12 WEEK as date), '%d/%m/%Y') AS twelve_weeks, 
                        now() >= create_datetime + INTERVAL 12 WEEK AS can_retake,
                        up.forename, up.surname 
                    FROM response_collections rc
                        join user_profiles up on up.user_id = rc.user_id
                    WHERE rc.id = ? AND rc.completed = 1";
                $params = [$assessment_id];
            } else {
                $this->redirect('/assessors');
            }
        } else {
            $sql = "
                    SELECT 
                        rc.*, date_format(cast(create_datetime + INTERVAL 12 WEEK as date), '%d/%m/%Y') AS twelve_weeks, 
                        now() >= create_datetime + INTERVAL 12 WEEK AS can_retake,
                        up.forename, up.surname 
                    FROM response_collections rc
                        join user_profiles up on up.user_id = rc.user_id
                    WHERE rc.user_id = ? AND rc.completed = 1
                    ORDER BY create_datetime DESC LIMIT 1"; // most recent
            $params = [$this->uID];
        }
        $d = [\PDO::PARAM_INT];
        $h = $this->data->executeQuery($sql, $params, $d);
        if($this->response_collection = $h->fetch()) {
            // get response results
            $h = $this->data->executeQuery('select * from response_results where response_collection_id = ?', [$this->response_collection['id']], [\PDO::PARAM_INT]);
            while($row = $h->fetch() ) {
                if(in_array($row['result_key'], $this->encrypted_result_fields)) {
                    $h2 = $this->encdb->executeQuery('select asset from assets where id = ?', [$row['result_value']], [\PDO::PARAM_INT]);
                    $h2 = $h2->fetch();
                    $row['result_value'] = $this->encryptor->decrypt($h2['asset']);
                }
                $this->response_collection['response_results'][$row['result_key']] = $row['result_value'];
            }
            $sql = 'select (select asset from assets where id = ?) as forename, (select asset from assets where id = ?) as surname';
            $params = [ $this->response_collection['forename'], $this->response_collection['surname']];
            $h3 = $this->encdb->executeQuery($sql, $params, [\PDO::PARAM_INT, \PDO::PARAM_INT]);
            $h3 = $h3->fetch();
            $this->response_collection['forename'] = $this->encryptor->decrypt($h3['forename']);
            $this->response_collection['surname'] = $this->encryptor->decrypt($h3['surname']);
            $this->response_collection['cab_postcodes'] =[]; // todo postcodes not yet provided
                $this->set('response_collection', $this->response_collection);
        } else {
            $this->redirect('/assessment'); // no completed assessments, fill one out
        }
    }

    public function unhold($assessment_id = null) {
        if(!in_array('FFW Advisors', $this->usergroups) or $assessment_id < 0) {
            $this->redirect('/myassessment');
        }
        $h = $this->data->executeQuery('update response_results set result_value = 0 where response_collection_id = ? and result_key = "on_hold"', [$assessment_id], [\PDO::PARAM_INT]);
        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, "unheld", 1, now())', [$assessment_id], [\PDO::PARAM_INT]);
        $this->redirect('/myassessment/' . $assessment_id);
    }

    public function refocus($assessment_id = null) {
        if($assessment_id < 1) {
            $this->redirect('/myassessment');
        }
        // remove any existing focus
        $h = $this->data->executeQuery('delete from response_results where response_collection_id = ? and result_key like "focus_item_%"', [$assessment_id], [\PDO::PARAM_INT]);
        // remove focus flag
        $h = $this->data->executeQuery('delete from response_results where response_collection_id = ? and result_key = "focus_needed"', [$assessment_id], [\PDO::PARAM_INT]);
        $c = 1;
        foreach($_POST['focus_item'] as $k => $v) {
            // insert replacement focus
            $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, ?, ?, now())', [$assessment_id, "focus_item_" . $c++, $k], [\PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR]);
        }
        $this->redirect("/myassessment/$assessment_id");
    }

    public function unholdp1($rcid, $type) {
        if(!in_array('FFW Advisors', $this->usergroups) or $rcid < 1) {
            $this->redirect('/myassessment');
        }
        // give focus to $type
        $h = $this->data->executeQuery('select * from response_results where response_collection_id = ? and result_key like "focus_item_%" order by result_key', [$rcid], [\PDO::PARAM_INT]);
        $focus_items = [];
        while($row = $h->fetch()) {
            $focus_items[] = $row; // should always be less than 2 results
        }
        if(isset($focus_items[0]['result_key']) and $focus_items[0]['result_key'] == 'focus_item_1') {
            $key = 'focus_item_2';
        } else {
            $key = 'focus_item_1';
        }
        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, ?, ?, now())', [ $rcid, $key, $type ], [\PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR ]);
        $this->redirect("/myassessment/$rcid");
    }

    public function holdp1($rcid, $type) {
        if(!in_array('FFW Advisors', $this->usergroups) or $rcid < 1) {
            $this->redirect("/myassessment/$rcid");
        }
        // take focus from $type
        // get which provider they are, 1 or 2
        $h = $this->data->executeQuery('select * from response_results where response_collection_id = ? and result_value = ?', [$rcid, $type], [\PDO::PARAM_INT, \PDO::PARAM_STR]);
        $h = $h->fetch();
        $which = substr($h['result_key'], -1);
        $k1 = $h['result_key'];
        $k2 = "focus_provider_$which";
        $h = $this->data->executeQuery('delete from response_results where response_collection_id = ? and (result_key like ? or result_key like ?)', [$rcid, $k1, $k2], [\PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR]);
        $this->redirect("/myassessment/$rcid");
    }

    public function confirmprovider($rcid, $provname) {
        $h = $this->data->executeQuery('select * from response_results where response_collection_id = ? and result_key like "focus_provider_%"', [$rcid], [\PDO::PARAM_INT]);
        $f = [];
        while($row = $h->fetch()) {
            $f[] = $row;
        }
        if($f[0]['result_key'] == 'focus_provider_1') {
            $key = 'focus_provider_2';
        } else {
            $key = 'focus_provider_1';
        }
        $h = $this->data->executeQuery('insert into response_results (response_collection_id, result_key, result_value, create_datetime) values (?, ?, ?, now())', [$rcid, $key, $provname], [\PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR]);

        // email provider
        $mail = new \FfwAssessmentLibs\FfwEmail();
        $express = new \FfwAssessmentLibs\FfwExpress();
        $provider = UserInfo::getByUserName(preg_replace('[- ]', '', $provname));
        $tokens = [ '@provider@' => $provname, '@custid@' => $rcid];
        $email = $express->get_email_tokenised('new-first-wellbeing-customer', $tokens);
        $mail->send($provider->getUserEmail(), $email['subject'], $email['body']);

        // create provider data initial record so customer appears in their list
        $h = $this->data->executeQuery('insert into provider_data (response_collection_id, provider_user_id, create_datetime) values (?, ?, now())', [$rcid, $provider->getUserID()], [\PDO::PARAM_INT, \PDO::PARAM_INT]);
        $id = $this->data->lastInsertId();
        $h = $this->data->executeQuery('insert into provider_datasets (provider_data_id, keyname, keyvalue) values (?, "initial", "Customer selected provider")', [$id], [\PDO::PARAM_INT]);

        if(!in_array('FFW Advisors', $this->usergroups)) {
            $this->redirect("/myassessment"); // user destination
        } else {
            $this->redirect("/myassessment/$rcid"); // advisor destination
        }
    }

}
