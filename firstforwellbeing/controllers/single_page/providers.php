<?php
namespace Concrete\Package\firstForWellbeing\Controller\SinglePage; // derived from top-level controller namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Concrete\Core\User\Group\Group as Group;
use Concrete\Core\User\UserList;
use Concrete\Core\User\UserInfo;
use Exception;
use stdClass;

class Providers extends PageController {

    public $uID = null;
    public $usergroups = null;
    public $username = null;

	public function on_start() {
        $this->data = \Database::connection('ffwdata');
        $this->encdb = \Database::connection('ffwdata_e');
        $this->encryptor = \Core::make("helper/encryption");
        $u = new User();
        $this->uID = $u->getUserID(); // userid
        $this->username = $u->getUserName(); // userid
        $ui = UserInfo::getByID($this->uID);
        $this->provider_type = $ui->getAttribute('userprovidertype')->getSelectedOptions()->current()->getSelectAttributeOptionValue();
        $this->usergroups = [];
        foreach( $u->getUserGroups() as $v ) {
            $this->usergroups[] = Group::getById($v)->gName; // user's assigned groups
        }
        $this->set('uID', $this->uID);
        $this->set('username', $this->username);
        $this->set('usergroups', $this->usergroups);
        $displayname = UserInfo::getByID($this->uID)->getUserDisplayName();
        $this->set('displayname', $displayname);
        $this->set('controller', $this);
        // inheritance
        parent::on_start();
	}

	public function view() {
		// Get
        $sql = 'SELECT distinct rc.user_id, a.asset as forename, b.asset as surname, (select count(*) from provider_data where response_collection_id = rc.id) as notes_count
            from response_collections rc
              join user_profiles up on rc.user_id = up.user_id
              join ffw_data_e.assets a on a.id = up.forename
              join ffw_data_e.assets b on b.id = up.surname
              join provider_data pd on pd.response_collection_id = rc.id
            where
              pd.provider_user_id = ?
              and rc.completed = 1
            order by rc.id desc
        ';
        $h = $this->data->executeQuery($sql, [ $this->uID ], [ \PDO::PARAM_INT ]);
        $customers = [];
        while($row = $h->fetch()) {
            $row['forename'] = $this->encryptor->decrypt($row['forename']);
            $row['surname'] = $this->encryptor->decrypt($row['surname']);
            $customers[] = $row;
        }
        $this->set('customers', $customers); // customer list
	}

	public function entry($user_id) {
	    // retrieve user data for provider
        $sql = 'select * from response_collections where user_id = ? and completed = 1 order by id desc limit 1'; // most recent by userid
        $h = $this->data->executeQuery($sql, [$user_id], [\PDO::PARAM_INT]);
        if(!$row = $h->fetch()) {
            $this->redirect('/providers');
        }

        // get assessment count
        $sql = 'select count(*) as cnt from response_collections where user_id = ? and completed = 1';
        $h = $this->data->executeQuery($sql, [$user_id], [\PDO::PARAM_INT]);
        $h = $h->fetch();
        $this->set('assessment_count', $h['cnt']);

        // get responses
        $sql = '
          select r.*, q.data_handle, qc.choice as qc_choice, qc.value as qc_value
          from responses r
           join questions q on q.id = r.question_id
           left join question_choices qc on qc.id = r.response_id 
          where
           response_collection_id = ?
        ';
        $h = $this->data->executeQuery($sql, [$row['id']], [\PDO::PARAM_INT]);
        while($row2 = $h->fetch()) {
            $row['responses'][$row2['data_handle']] = $row2;
        }
        $this->set('assessment', $row);

        // get provider data
        $h = $this->data->executeQuery('select pd.*, pds.keyname, pds.keyvalue, pds.keyvalue_enc_asset_id from provider_data pd join provider_datasets pds on pd.id = pds.provider_data_id order by pd.id desc');
        $data = [];
        while($row = $h->fetch()) {
            if($row['keyvalue_enc_asset_id'] > 0) {
                // get and decrypt asset
                $h2 = $this->encdb->executeQuery('select asset from assets where id = ?', [$row['keyvalue_enc_asset_id']], [\PDO::PARAM_INT]);
                if($h2 = $h2->fetch()) {
                    $row['keyvalue'] = $this->encryptor->decrypt($h['asset']);
                }
            }
            $data[$row['id']][] = $row;
        }
        $this->set('provider_data', $data);

        // get notes
        $h = $this->data->executeQuery('select * from assessor_notes where response_collection_id = ? order by id desc', [$this->response_collection['id']]
        , [\PDO::PARAM_INT]);
        $notes = [];
        while($row = $h->fetch()) {
            $visibility = explode(',', $h['visible_to']);
            if(in_array(strtolower($this->provider_type), $visibilty)) {
                $notes[] = $row;
            }
        }
        $this->set('customer_notes', $notes);
        $this->set('provider_type', strtolower($this->provider_type));
        $this->set('customer_id', $user_id);

        $this->render('provider_entry');
    }

    public function get_provider_form_fields($cid) {
        $standard_fields = ['User ID' => ['customer_id', 'hidden', null, $cid], "Date of Referral" => ['referral_date', 'date', 'first'],  "Date of contact by specialist team" => ['specialist_contact_date', 'hidden', null, date('d/m/Y')]];
        /*
         * Boolean
           Integer
           Text
           Date
           Float
         */
        switch($this->provider_type){
            case 'Smoking':
                $formFields = [
                    "Start Cigarettes per day" => ['per_day', 'text', 'first'],
                    "Start other tobacco use" => ['other_tobacco', 'boolean', 'first'],
                    "Start CO2 reading" => ['co2_reading', 'text', 'first'],
                    "Quit Date" => ['quit_date', 'date', 'first'],
                    "12 Week Follow up date" => ['12_week_date', 'date', 'first'],
                    "26 Week Follow up date" => ['26_week_date', 'date', 'first'],
                    "52 Week Follow up date" => ['52_week_date', 'date', 'first'],
                    "CO Recorded" => ['co2_reading', 'integer', 'second'],
                    "Cigarettes per day" => ['cigarettes', 'integer', 'second'],
                    "Other tobacco use" => ['other_tobacco', 'boolean', 'second'],
                    "NRT Issued" => ['nrt_issued', 'boolean', 'second'],
                    "Support Received" => ['support_received', 'boolean', 'second'],
                    "Date of recorded quit" => ['activity_date', 'date', 'second'],
                ];
                break;
            case 'Weight':
                $formFields = [
                    'Height (cm)' => ['height', 'float', 'first'],
                    'Weight Recorded (kg)' => ['weight_recorded', 'integer'],
                    'Target Weight (kg)' => ['taget_weight', 'integer', 'first'],
                    'Date of weight check' => ['activity_date', 'date', 'second']
                ];
                // weightwatchers special case
                if($this->username == 'weightwatchers') {
                    $formFields['Customer code'] = ['wwatch_custcode', 'text', null];
                }
                // slimmingworld special case
                if($this->username == 'Slimming World') {
                    $formFields['Customer ref'] = ['sworld_custref', 'text', null];
                }
                break;
            case 'Alcohol':
                $formFields = [
                    'Audit C Score' => ['audit_c_score', 'integer', 'first'],
                    'Daily alcohol intake (Units)' => ['daily_alcohol_intake', 'integer', 'first'],
                    'Date of alcohol review' => ['activity_date', 'date', 'second'],
                ];
                break;
            case 'Finance':
                $formFields = [
                    'Benefits Received' => ['benefits_received', 'boolean', 'first'],
                    'List of benefits received' => ['benefits_received', 'text', 'first'],
                    'Benefits Claimed' => ['benefits_claimed', 'float', 'first'],
                    'Benefits Claimed' => ['benefits_claimed', 'float', 'second'],
                    'Debt' => ['amount_debt', 'float', 'first'],
                    'In receeipt of warm homes discount' => ['warm_home_discount', 'boolean', 'first'],
                    'Support/advice given' => ['support_given', 'boolean', 'second'],
                    'Benefits Received' => ['benefits_received_at_review', 'boolean', 'second'],
                ];
                break;
            case 'Housing':
                $formFields = [
                    'Housing status on referral' => ['housing_status', 'text', 'first'],
                    'Length of time in this housing status' => ['housing_status_time', 'text', 'first'],
                    'Customer identified as priority need group' => ['housing_priority', 'boolean', 'first'],
                    'Date of housing review' => ['housing_review_date', 'date', 'second'],
                    'Housing Status' => ['housing_status', 'text', 'second'],
                    'Type of home support received' => ['housing_support', 'text', 'second'],
                    'Date of home support received' => ['housing_support_date', 'date'],
                    'Type of home adaption received' => ['housing_adaption', 'text', 'second'],
                    'Date of home adaptions installed' => ['housing_adaption_date', 'date', 'second'],
                    'Type of major home works received' => ['housing_major_work', 'text', 'second'],
                    'Date of home works received' => ['housing_major_work_date', 'date', 'second']
                ];
                break;
            case 'Emotional':
                $formFields = [];
                // learn2b special case
                if($this->username == 'Learn2B') {
                    $formFields['Customer notes'] = ['learn2b_custnotes', 'textarea', null];
                    $formFields['Contact made'] = ['learn2b_contact', 'select', null, null, ['Face to Face', 'Phone call', 'Email', 'Post', 'Peer Support Group', 'Ambassador', 'Initial Assessment - IAG'] ];
                    $formFields['Actions'] = ['learn2b_action', 'select', null, null, ['Enrolment', 'Brochure - Mailing List', 'Ambassador support', 'IAG referral (external)', 'Functional Skills referral', 'Dyslexia referral', '1:1 sessions'] ];
                    $formFields['Date of consultation'] = ['learn2b_condate', 'date', null];
                    $formFields['Who met the customer'] = ['learn2b_whomet', 'text', null];
                }
                $formFields["WEMWBS score"] = ['wemwbs_score', 'integer', 'first'];
                $formFields["Date of Attendance"] = ['date_of_attendance', 'date', 'second'];
                $formFields["Activity Type"] = ['activity_type', 'text', 'second'];
                $formFields["Duration of activity"] = ['duration_of_activity', 'float', 'second'];
                break;
            case 'Social':
                $formFields = [
                    "Provision of transport" => ['provision_of_transport', 'text', 'second'],
                    "Activity Type" => ['activity_type', 'text', 'second'],
                    "Duration of activity" => ['duration_of_activity', 'float', 'second'],
                ];
                break;
            case 'Employment':
                $formFields = [
                    "Employment status" => ['employment_status', 'boolean', 'first'],
                    "Most recent Employment (end date)" => ['most_recent_employment', 'date', 'first'],
                    "Date of employment review" => ['employment_review', 'date', 'second'],
                    "Employment status" => ['employment_status', 'boolean', 'second'],
                    "Employment area" => ['employment_area', 'text', 'second'],
                    "Employment gained on" => ['employment_gained', 'date', 'second'],
                    "Date commenced" => ['date_commenced', 'date', 'second'],
                    "Type of voluntary activity" => ['type_of_voluntary_activity', 'text', 'second'],
                    "Location of voluntary activity" => ['location_of_voluntary_activity', 'text', 'second'],
                    "Duration (Hours)" => ['duration_of_activity', 'float', 'second'],
                    "Name of course attended" => ['course_attended', 'text', 'second'],
                    "Course delivery by" => ['course_delivered_by', 'text', 'second'],
                    "Date of course" => ['course_date', 'date', 'second'],
                    "Duration of course" => ['course_duration', 'text', 'second']
                ];
                break;

        }

        $end_fields = [
            "Location" => ['location', 'text'],
            "Recorded By" => ['recording_user_id', 'integer'],
        ];

        return $standard_fields + $formFields + $end_fields;
    }

    public function get_provider_data() {
	    $h = $this->data->executeQuery('select pd.*, pds.keyname, pds.keyvalue, pds.keyvalue_enc_asset_id from provider_data pd join provider_datasets pds on pd.id = pds.provider_data_id where pd.response_collection_id = ? order by id desc', [$this->response_collection['id']], [\PDO::PARAM_INT]);
	    $data = [];
	    while($row = $h->fetch()) {
	        $data[$row['id']][$row['keyname']] = $row;
        }
        return $data;
    }

    public function add_data($rcid = null, $cid = null) {
	    $h = $this->data->executeQuery('insert into provider_data (response_collection_id, provider_user_id, create_datetime) values (?, ?, now())', [ $rcid, $this->uID], [\PDO::PARAM_INT, \PDO::PARAM_INT]);
	    $id = $this->data->lastInsertId();
	    $fields = $this->get_provider_form_fields(0); // so we can determine and encrypt textareas
        $fields2 = [];
        foreach($fields as $field) {
            $fields2[$field[0]] = $field;
        }
	    foreach($_POST as $k => $v) {
            if($k == 'action') { continue; }
            if($fields2[$k][1] == 'textarea') {
                $v_enc = $this->encryptor->encrypt($v);
                $h = $this->encdb->executeQuery('insert into assets (asset) values (?)', [$v_enc], [\PDO::PARAM_STR]);
                $e = $this->encdb->lastInsertId();
                $v = ''; // don't store the unencrypted version
            } else {
                $e = 0;
            }

	        $h = $this->data->executeQuery('insert into provider_datasets (provider_data_id, keyname, keyvalue, keyvalue_enc_asset_id) values (?, ?, ?, ?)', [$id, $k, $v, $e], [\PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT]);
        }
        $this->redirect('/providers/entry/' . $cid);
    }

    public function export() {
        $fields = $this->get_provider_form_fields(0);
        $header_names = [];
        foreach($fields as $k => $v) {
            $header_names[$v[0]] = $k;
        }

        $sql = '
            select pd.response_collection_id as aid, rc.user_id as cid, rc.assessment_number, rc.create_datetime as assessment_created_date, up.*, pds.keyname, pds.keyvalue
            from provider_data pd
              join provider_datasets pds on pds.provider_data_id = pd.id
              join response_collections rc on pd.response_collection_id = rc.id
              join user_profiles up on up.user_id = rc.user_id
            where pd.provider_user_id = ?
            order by rc.user_id desc
        ';
        $h = $this->data->executeQuery($sql, [$this->uID], [\PDO::PARAM_INT]);
        $items = [];
        $lastid = 0;
        $sql = '
              select qc.choice 
              from question_choices qc 
                join questions q on qc.question_id = q.id
                join responses r on r.question_id = q.id
                join response_collections rc on r.response_collection_id = rc.id
              where 
                rc.id = ?
                and qc.id = r.response_id
                and q.data_handle = "preferredmethodofcontact"
            ';
        $prefcon = '';
        while($row = $h->fetch()) {
            if(in_array( $row['keyname'] , ['customer_id', 'assessment_id'] )) {continue;}
            if($row['cid'] != $lastid) {
                // get preferred contact method
                $h2 = $this->data->executeQuery($sql, [$row['aid']], [\PDO::PARAM_INT]);
                $h2 = $h2->fetch();
                $prefcon = $h2['choice'];
                $lastid = $row['cid'];
            }
            $items[$row['aid']]['Assessment ID'] = $row['aid'];
            $items[$row['aid']]['Customer ID'] = $row['cid'];
            $items[$row['aid']]['Submission Number'] = $row['assessment_number'];
            $items[$row['aid']]['Assessment Date'] = $row['assessment_created_date'];
            $items[$row['aid']]['Forename'] = $this->get_encrypted_asset($row['forename']);
            $items[$row['aid']]['Surname'] = $this->get_encrypted_asset($row['surname']);
            $items[$row['aid']]['Address 1'] = $this->get_encrypted_asset($row['address1']);
            $items[$row['aid']]['Address 2'] = $this->get_encrypted_asset($row['address2']);
            $items[$row['aid']]['Town'] = $this->get_encrypted_asset($row['town']);
            $items[$row['aid']]['County'] = $this->get_encrypted_asset($row['county']);
            $items[$row['aid']]['Postcode'] = $this->get_encrypted_asset($row['postcode']);
            $items[$row['aid']]['Telephone number'] = $this->get_encrypted_asset($row['tel1']);
            $items[$row['aid']]['Mobile'] = $this->get_encrypted_asset($row['tel2']);
            $items[$row['aid']]['Preferred contact'] = $h2['choice'];
            $items[$row['aid']][$row['keyname']] = $row['keyvalue'];
            $header_cols[$row['keyname']] = $header_names[$row['keyname']]; // look up human col name
        }
        $header_cols = implode(',', $header_cols);

        // output csv header row
        $fileName = date("d-m-Y") . ".csv";
        header('Content-type: text/csv');
        header("Content-disposition: csv" . $fileName);
        header("Content-disposition: filename=" . $fileName);
        echo "Assessment ID,Customer ID,Submission Number,Assessment Date,Forename,Surname,Address 1,Address 2,Town,County,Postcode,Telephone number,Mobile,Preferred contact,$header_cols\n";

        $con = ['Email', 'Telephone', 'Post'];
        foreach($items as $item) {
            // csv formatting
            foreach($item as $k=>$v) {
                // check for "
                if(strpos($v, '"') !== false) {
                    // double up quotes
                    $v = preg_replace('/"/', '""', $v);
                }
                // quote-enclose by default
                $v = '"' . $v . '"';
                $item[$k] = $v;
            }
            //output csv row
            echo implode(',', $item) . "\n";
        }
        die();
    }

    public function template() {
        $fields = $this->get_provider_form_fields(0);
        $header_names = [];
        foreach($fields as $k => $v) {
            $header_names[$v[0]] = $k;
        }

        $sql = '
            select pd.response_collection_id as aid, rc.user_id as cid, rc.assessment_number, rc.create_datetime as assessment_created_date, up.*, pds.keyname, pds.keyvalue
            from provider_data pd
              join provider_datasets pds on pds.provider_data_id = pd.id
              join response_collections rc on pd.response_collection_id = rc.id
              join user_profiles up on up.user_id = rc.user_id
            where pd.provider_user_id = ?
            order by rc.user_id desc
        ';
        $h = $this->data->executeQuery($sql, [$this->uID], [\PDO::PARAM_INT]);
        $items = [];
        $lastid = 0;
        $sql = '
              select qc.choice 
              from question_choices qc 
                join questions q on qc.question_id = q.id
                join responses r on r.question_id = q.id
                join response_collections rc on r.response_collection_id = rc.id
              where 
                rc.id = ?
                and qc.id = r.response_id
                and q.data_handle = "preferredmethodofcontact"
            ';
        $prefcon = '';
        while($row = $h->fetch()) {
            if(in_array( $row['keyname'] , ['customer_id', 'assessment_id'] )) {continue;}
            $items[$row['aid']]['Assessment ID'] = $row['aid'];
            $items[$row['aid']]['Customer ID'] = $row['cid'];
            $items[$row['aid']]['Address 1'] = $this->get_encrypted_asset($row['address1']);
            $items[$row['aid']]['Postcode'] = $this->get_encrypted_asset($row['postcode']);

            $items[$row['aid']][$row['keyname']] = '';
            $header_cols[$row['keyname']] = $header_names[$row['keyname']]; // look up human col name
        }
        $header_cols = implode(',', $header_cols);

        // output csv header row
        $fileName = date("d-m-Y") . ".csv";
        header('Content-type: text/csv');
        header("Content-disposition: csv" . $fileName);
        header("Content-disposition: filename=" . $fileName);
        echo "Assessment ID,Customer ID,Address 1,Postcode,$header_cols\n";

        $con = ['Email', 'Telephone', 'Post'];
        foreach($items as $item) {
            // csv formatting
            foreach($item as $k=>$v) {
                // check for "
                if(strpos($v, '"') !== false) {
                    // double up quotes
                    $v = preg_replace('/"/', '""', $v);
                }
                // quote-enclose by default
                $v = '"' . $v . '"';
                $item[$k] = $v;
            }
            //output csv row
            echo implode(',', $item) . "\n";
        }
        die();
	}

    public function upload() {
	    $this->render('provider_upload');
    }

    public function doupload() {
        if( isset($_FILES["data"]["tmp_name"]) ) {
            // check csv data
            $fh = fopen( $_FILES["data"]["tmp_name"], 'r' );
            $data = array();
            $count = 0;
            while( $row = fgetcsv($fh) ) {
                if( $count == 0 ) {
                    // column headers
                    $headers = $row;
                    $count++;
                    continue;
                }
                $data[] = $row;
            }
            // get provider data fields
            $user = $this->uID;
            $meta = $this->provider_type;
            $fields = $this->get_provider_form_fields(0);
            // insert records. Note there's no reliable way to check the provider has entered sensible data, currently
            foreach($data as $k=>$row) {
                $rcid = $row[0];
                $cid = $row[1];
                $recid = $user;
                $sql = 'insert into provider_data (response_collection_id, provider_user_id, create_datetime) values (?, ?, now())';
                $h = $this->data->executeQuery($sql, [$rcid, $this->uID], [\PDO::PARAM_INT, \PDO::PARAM_INT]);
                $id = $this->data->lastInsertId();

                $insert = array();
                foreach(array_slice($headers, 4) as $k1=>$h1) { // don't need first four columns
                    $colname = $fields[$h1][0];
                    if(empty($colname)) { continue; }
                    if($fields[$colname][1] == 'textarea') {
                        // encrypt textareas
                        $encval = $this->encryptor->encrypt($row[$k1 + 4]);
                        $h3 = $this->encdb->executeQuery('insert into assets (asset) values (?)', [$encval], [\PDO::PARAM_STR]);
                        $e = $this->encdb->lastInsertId();
                        $row[$k1 + 4] = '';
                    } else {
                        $e = 0;
                    }
                    $params = [ $id, $colname, $row[ $k1 + 4 ], $e ];
                    $sql = 'insert into provider_datasets (provider_data_id, keyname, keyvalue, keyvalue_enc_asset_id) values (?, ?, ?, ?)';
                    $h2 = $this->data->executeQuery($sql, $params, [\PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT]);
                }
            }
            $this->set('upload_done', true);
            $this->redirect('/providers/upload');
        }
    }

    private function get_encrypted_asset($id = null) {
	    if($id === null) return null;
	    $h = $this->encdb->executeQuery('select asset from assets where id = ?', [$id], [\PDO::PARAM_INT]);
	    if($h = $h->fetch()) {
	        return $this->encryptor->decrypt($h['asset']);
        } else {
	        return null;
        }
    }
}
