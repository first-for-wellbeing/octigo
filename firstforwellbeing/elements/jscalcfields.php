<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>
<script type="text/javascript">
    <?php
        // build js ver of response_collection
        echo 'rc = ' . json_encode($rc) . ";\n\n";
    ?>
    $(document).ready( function() {
        $('input, select').change( function(e) {
           $('.calculation').each( function() {
               var calc = $(this).attr('data-val');
               var tokens = calc.match(/\{(\d+)\}/g); // find all occurences of {123}
               if(tokens !== null && tokens.length > 0) {
                   var value = null;
                   $.each(tokens, function(i, v) {
                       v = v.substr(1, v.length - 2);
                       var el = $('[name="' + v + '"]');
                       if($(el).length > 0) {
                           // field in this page
                           if($(el).is('select, [type="radio"]')) {
                               if($(el).is('select')) {
                                   value =  $(el).filter(':selected').attr('data-score');
                               } else {
                                   value =  $(el).filter(':checked').attr('data-score');
                               }
                           } else {
                               value = $(el).val();
                           }
                       } else {
                           // look in rc array
                           if( typeof rc.responses[v] !== 'undefined') {
                               value = rc.responses[v].value;
                           }
                       }
//                       if(isNaN(parseFloat(value))) {
//                           calc = null;
//                           return false; // break
//                       }
                       if(value == '' || value.replace(/\d|\./g, '') != '') {
                           value = "'" + value + "'"; // quote non-numerics, e.g.date
                       }
                       calc = calc.replace('{' + v + '}', value);
                   });
                   var calc_eval = eval(calc);
                   if(!isNaN(parseFloat(calc_eval))) {
                       $(this).text(calc_eval.toFixed(2));
                       $('#' + $(this).attr('data-qid')).val(calc_eval.toFixed(2));
                   } else {
                       if(isNaN(calc_eval)) {
                           $(this).text('N/A');
                           $('#' + $(this).attr('data-qid')).val('');
                       } else {
                           $(this).text(calc_eval);
                           $('#' + $(this).attr('data-qid')).val(calc_eval);
                       }
                   }
               }
           }) ;
        });
    });
</script>