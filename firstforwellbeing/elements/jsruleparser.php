<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>
<script type="text/javascript">

<?php
	// build JS rule array from assessment object
//print_r($questions);
	$jsrules = [];
	$ar_rules = [];
	foreach($questions as $k => $question) {
		// collect choices
		foreach($question['choices'] as $i => $choice) {
			$choice['question_id'] = $question['id'];
			$choices[$choice['id']] = $choice;
		}
		// create rules
		if(empty($question['rules'])) { continue; }
		$ar_rulesets = [];
		foreach($question['rules'] as $i2 => $rule) {
			if(empty($rule['conditions'])) { continue; }
			$ar_conditions = [];
			foreach($rule['conditions'] as $condition) {
				//$jsrule[ $question['id'] ][ $rule['id'] ][ $condition['id'] ] =
				// determine left/right values
				if($condition['left_valuetype'] == 4) {
					$left_val = get_term(get_questiontype_by_id($condition['left_value'], $questions), $condition['left_value']);
				} else {
					$left_val = "'" . $condition['left_value'] . "'";
				}
				if($condition['right_valuetype'] == 4) {
					$right_val = get_term(get_questiontype_by_id($condition['right_value'], $questions), $condition['right_value']);
				} else {
					$right_val = "'" . $condition['right_value'] . "'";
				}
				$ar_conditions[] = 'eval_rule(' . $condition['ruletype_id'] . ', ' . $left_val . ', ' . $right_val . " )\n";
			}
			$ar_rulesets[] = '(' . implode(') && (', $ar_conditions) . ')';
		}
		$ar_rules[] = 'if(' . implode(' || ', $ar_rulesets) . ") {\n $('#row-" . $question['id'] . "').fadeIn(500).removeClass('hidden-by-rule'); \n} else {\n $('#row-" . $question['id'] . "').fadeOut(500).addClass('hidden-by-rule'); \n};\n";
	}
	echo 'var jschoices = ' . json_encode($choices) . ";\n\n";

	///////////////////////////////
	///
	/// FUNCTION
	function get_questiontype_by_id($id, $questions) {
		foreach($questions as $q) {
			if($q['id'] == $id) {
				return $q['question_type'];
			}
		}
	}
	
	function get_term($questiontype, $qid) {
		switch($questiontype) {
			case 1: // date
			case 2: // text short
			case 4: // int
			case 5: // decimal
			case 11: // email
			case 12: // password
            case 14: // plain text
            case 15: // calc
				$val = "( $('#row-$qid').is('.hidden-by-rule') ? null : $('input[name=\"$qid\"]').val() )";
				break;
			case 3: // text long
				$val = "( $('#row-$qid').is('.hidden-by-rule') ? null : $('textarea[name=\"" . $qid . "\"]').val() )";
				break;
			case 6: // dropdown
			$val = "( (!$('#row-$qid').is('.hidden-by-rule') && $('select[name=\"$qid\"]').val() != '') ? jschoices[$('select[name=\"$qid\"]').val()].value : null )";
				break;
			case 7: // tickbox
				$val = "( $('#row-$qid').is('.hidden-by-rule') ? null : $('input[name=\"$qid\[\]\"]').parent().find(':checked').attr('textval') )";
				break;
			case 9: // yes/no
				$val = "( $('#row-$qid').is('.hidden-by-rule') ? null : $('input[name=\"$qid\"]:checked').val() )";
				break;
			case 8: // radio
			case 10: // radio icons
			case 13: // slider
				$val = "( (!$('#row-$qid').is('.hidden-by-rule') && typeof($('input[name=\"$qid\"]:checked').val()) !== 'undefined') ? jschoices[$('input[name=\"$qid\"]:checked').val()].value : null )";
				break;
			default:
				$val = '';
				console.log('Unknown ruletype: ' . $questiontype);
				break;
		}
		return $val;
	}
	
?>

$(document).ready( function() {
	$('.question').on('change', function(e) {
<?php 	echo implode("\n", $ar_rules); ?>
	});
});

function eval_rule(ruletype, leftval, rightval) {
    if(leftval === null || rightval === null) {
        return false; // a null is always false
    }
	larr = Array.isArray(leftval);
	rarr = Array.isArray(rightval);
	if(typeof(leftval) === 'undefined') {
		leftval = 0;
	}
	if(typeof(rightval) === 'undefined') {
		rightval = 0;
	}
	// NB we don't cater for checkbox - checkbox comparisons
	switch(ruletype) {
		case 1: // ==,  checkboxes allowed
			if(larr) {
				return (leftval.indexOf(rightval) != -1);
			} else if(rarr) {
				return (rightval.indexOf(leftval) != -1);
			} else {
				return (leftval == rightval);
			}
			break;
		case 2: // Like, checkboxes not allowed
			if(larr || rarr) {
				return false;
			} else {
				return ( (leftval == 1 ? 'YES' : 'NO').indexOf(rightval.toUpperCase()) != -1);
			}
			break;
		case 3: // GT, checkboxes not allowed
			if(larr || rarr) {
				return false;
			} else {
				return (leftval > rightval);
			}
			break;
		case 4: // LT, checkboxes not allowed
			if(larr || rarr) {
				return false;
			} else {
				return (leftval < rightval);
			}
			break;
		case 5: // !=,  checkboxes allowed
			if(larr) {
				return (leftval.indexOf(rightval) == -1);
			} else if(rarr) {
				return (rightval.indexOf(leftval) == -1);
			} else {
				return (leftval != rightval);
			}
			break;
		default:
			return 'error - unhandled ruletype';
			break;
	}
}

</script>
