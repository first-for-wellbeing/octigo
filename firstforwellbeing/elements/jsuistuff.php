<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<script type="text/javascript">
    $(document).ready( function() {
        // Accordion
        if ( $('.accordion').length > 0 ) {
            $('.accordion').each(function() {
                $(this).find('li').each(function() {
                    if( !$(this).hasClass('open') ) {
                        $(this).find('.accordion-content').hide();
                    }
                });
            });
            $('.accordion .accordion-btn').click(function() {
                if ( $(this).parent('li').hasClass('open') ) {
                    $(this).parent('li').removeClass('open');
                    $(this).siblings('.accordion-content').slideUp();
                } else {
                    $(this).closest('.accordion').find('li').removeClass('open').find('.accordion-content').slideUp();
                    $(this).parent('li').addClass('open');
                    $(this).siblings('.accordion-content').slideDown();
                }
                return false;
            });
        }

        // refocus clicked (assessment results)
        $('.refocus').click( function(e) {
            e.stopPropagation(); // stop the accordion happening
            if( $(this).hasClass('held') ) {
                // can't unhold unless there is an available 'slot' to unhold into
                if ($('.refocus.unheld').length > 1) {
                    alert('Please put another theme on hold first before unholding this one');
                    e.preventDefault();
                }
            }
        });

        //Edit focus choices (assessment results)
        $('.choose-focus-items input[type="checkbox"]').change(function() {
            if($('.choose-focus-items input[type="checkbox"]:checked').length==2 || ($('.choose-focus-items input[type="checkbox"]:checked').length==3 && $('.btn-save-focus').attr('data-force-checked')==3)) {
                $('.choose-focus-items .btn-save-focus').removeClass('hide');
            }
            else {
                $('.choose-focus-items .btn-save-focus').addClass('hide');
            }
        });

        //Submit choices (assessment results)
        $('.choose-focus-items').submit(function() {
            if($('.choose-focus-items input[type="checkbox"]:checked').length==2 || ($('.choose-focus-items input[type="checkbox"]:checked').length==3 && $('.btn-save-focus').attr('data-force-checked')==3)) {
                $('.choose-focus-items input[type="checkbox"]').removeAttr('disabled');
                return true;
            }
            else {
                return false;
            }
        });

        // Trigger update (assessment results)
        $('.choose-focus-items input[type="checkbox"]').last().change();

        // Provider info read more clicked (assessment results)
        $('.p1-provider-more').on('click', function(e) {
            e.preventDefault();
            var provid = $(this).attr('provider-id');
            $('.p1-provider-icons').fadeOut(500);
            $('.provider-pop-' + provid).fadeIn(500);
        });

        // Provider info image clicked (assessment results)
        $('.provider-img').on('click', function(e) {
            e.preventDefault();
            var provid = $(this).parents('.provider-container').find('.p1-provider-more').attr('provider-id');
            $('.p1-provider-icons').fadeOut(500);
            $('.provider-pop-' + provid).fadeIn(500);
        });

        // Back to providers clicked (assessment results)
        $('.back-to-providers').on('click', function(e) {
            e.preventDefault();
            var id = $(this).attr('provider');
            $('.provider-pop-' + id).fadeOut(500);
            $('.p1-provider-icons').fadeIn(500);
        });

        // P1 Email Button clicked (assessment results)
        $('.send-p1-email').click( function(e) {
            window.location.replace('/myassessment/confirmprovider/' + $(this).attr('data-rcid') + '/' + $(this).attr('providername'));
        });

        //Save type comment (assessment results)
        $('.save-note').click(function() {
            var content = $(this).closest('.accordion-content').find('.form-edit textarea').val();
            var field = $(this).closest('.accordion-content').find('.form-edit textarea').attr('name');
            var contentDiv = $(this).closest('.accordion-content');
            if(content == '') {
                alert('You must enter some information');
            } else {
                $.ajax({
                    method: "POST",
                    url: '/ajax/savenote',
                    data: { field: field, content: content, rcid: $(this).attr('data-rcid') }
                }).success(function( data ) {
                    alert('Your note has been saved');
                    contentDiv.find('.result-text p').html(content.replace(/\n/g,"<br />"));
                    contentDiv.find('.result-text').show();
                    //contentDiv.find('.form-edit').hide();
                });
            }
            return false;
        });

        // get twitter stuff
        if( $('.footer-social .tweet-box').length > 0 ) {

        }

        //Edit type comment (assessor view)
        $('.assessor-add-note').click(function() {
            $(this).closest('.accordion-content').find('.result-text').hide();
            $(this).closest('.accordion-content').find('.form-edit').show();
            return false;
        });

        //Cancel type comment (assessor view)
        $('.assessor-cancel-note').click(function() {
            $(this).closest('.accordion-content').find('.result-text').show();
            $(this).closest('.accordion-content').find('.form-edit').hide();
            return false;
        });

        // Validate customer note fields (assessor view)
        $('#form-notes').submit( function(e) {
            if( $('#user_note').val() == '' || $('#type_of_contact').val() == null || $('#followup_action').val() == null ) {
                alert('Please complete all customer note fields before saving');
                e.preventDefault();
                return false;
            }
        });

        // Get postcode selection (assessment contact info)
        $('.postcode-choices').change( function() {
            var address_text = '';
            var choice = $('#postcode-choices option:selected').val();
            $.each(addresses, function( key, value ) {
                if( choice == value.Uprn ) {
                    $('#address_1').val( value.HouseNumber + " " + value.Street ).change();
                    $('#address_2').val( value.DependentLocality ).change();
                    $('#town').val( value.PostTown ).change();
                    $('#county').val( 'Northamptonshire' ).change();
                }
            });
            $('.postcode-hide').slideDown('slow');
        });


    });
</script>