<script type="text/javascript">
        $(document).ready( function() {

            // handle edit button click
            $('.edit-button').on('click', function(e) {
                $(this).parent().hide();
                $(this).parent().siblings('.edit').fadeIn(500);
            });

            // handle cancel button click
            $('.cancel').on('click', function(e) {
                $(this).parent().parent().hide();
                $(this).parent().parent().siblings('.data').show();
            });

            // handle update button click
            $('.update').on('click', function(e) {
                // gather field data
                var fields = $(this).parents('.edit').find('input, select');
                var data = {};
                $(fields).each( function() {
                    if( $(this).is('[type=radio]') ) {
                        if( $(this).is(':checked') ) {
                            data[ $(this).attr('name') ] = $(this).val();
                        }
                    } else {
                        data[ $(this).attr('name') ] = $(this).val();
                    }
                });
                $.ajax({
                    method: "POST",
                    url: "/ajax/profileupdate",
                    data: { custid:$('#custid').val(), data:JSON.stringify(data) },
                }).success(function( data ) {
                    location.reload();
                });
            });

            // handle display of pregnant
            $('input[name=gender]').on('click', function(e) {
                var gender = $('input[name=gender]:checked').val();
                if(gender == 'Female') {
                    $('.pregnant').show(500);
                } else {
                    $('.pregnant').hide(500);
                }
            });

            // handle display of religion other
            $('input[name=religion]').on('click', function(e) {
                var religion = $('input[name=religion]:checked').val();
                if(religion == 'Other') {
                    $('.religion-other').show(500);
                } else {
                    $('.religion-other').hide(500);
                }
            });

            // handle display of marital other
            $('input[name=maritalstatus]').on('click', function(e) {
                var marital = $('input[name=maritalstatus]:checked').val();
                if(marital == 'Other') {
                    $('.marital-other').show(500);
                } else {
                    $('.marital-other').hide(500);
                }
            });

            // handle display of ethnic other
            $('input[name=ethnicorigin]').on('click', function(e) {
                var ethnic = $('input[name=ethnicorigin]:checked').val();
                if(ethnic == 'Other') {
                    $('.ethnic-other').show(500);
                } else {
                    $('.ethnic-other').hide(500);
                }
            });

            // handle display of sexuality other
            $('input[name=sexuality]').on('click', function(e) {
                var sexuality = $('input[name=sexuality]:checked').val();
                if(sexuality == 'Other') {
                    $('.sexuality-other').show(500);
                } else {
                    $('.sexuality-other').hide(500);
                }
            });
        });
</script>