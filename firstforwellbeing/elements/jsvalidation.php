<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>
<script>
    postcodes_done = false;
    function ffw_validate_all( form, changed_thing ) {
        if(!postcodes_done && typeof user_postcode !== 'undefined' && user_postcode.length > 0) {
            var data = get_postcode_choices(user_postcode.replace(' ',''));
            postcodes_done = true;
        }
        var valid = true;
        $('.validation-error').removeClass('validation-error');
        $('.validation-message').remove();
        // mandatory fields
        $(form).find('.mandatory, .mandatorya, .specval.touched').each( function() {
            // ignore rule-hidden fields
            if($(this).parents('.question-row').is('.hidden-by-rule')) {
                return true; // continue
            }
            var validthis = true;
            if($(this).is('.specval.touched') && $(this).attr('name') == $(changed_thing).attr('name')) {
                // Special validation required
                var specval = $(this).attr('specval');
                switch(parseInt(specval)) {
                    case 1: // Postcode in a region
                        if( $(this).val().length == 0 || !checkpostcode($(this).val().replace(' ', '')) ) {
                            if($(this).is('.touched')) {
                                $(this).addClass('validation-error');
                                $(this).before('<div class="validation-message">' + $(this).attr('data-valmsg') + '</div>');
                            }
                            valid = false;
                            validthis = false;
                        }
                        break;
                    case 2: // All password fields must match
                        var pwfirstel = $('input[type="password"]').first();
                        var pwfirst = $(pwfirstel).val();
                        regxl = /[a-z]/; regxu = /[A-Z]/; regxd = /\d/;
                        $('input[type="password"]').each( function(e) {
                            if( ! ( $(this).val().length > 7 && regxl.test(pwfirst) && regxu.test(pwfirst) && regxd.test(pwfirst) ) ) {
                                valid = false;
                                validthis = false;
                                if ($(this).is('.touched')) {
                                    $(this).addClass('validation-error');
                                    $(this).before('<div class="validation-message">' + $(this).attr('data-valmsg') + '</div>');
                                }
                            }
                            if(validthis && $(this).attr('name') != $(pwfirstel).attr('name')) {
                                if ($(this).val() != pwfirst) {
                                    if ($(this).is('.touched')) {
                                        $(this).addClass('validation-error');
                                        $(this).before('<div class="validation-message">' + $(this).attr('data-valmsg') + '</div>');
                                    }
                                    valid = false;
                                    validthis = false;
                                }
                            }
                            if (validthis) {
                                $(this).parents('.question-row').find('.tick').removeClass("invalid").addClass('valid');
                            } else {
                                $(this).parents('.question-row').find('.tick').removeClass("valid").addClass('invalid');
                            }
                        });
                        break;
                    case 3: // Username check
                        // must be alphanumeric, no spaces
                        regx = /^[a-z0-9]*$/i;
                        if( !regx.test($(this).val()) ) {
                            if($(this).is('.touched')) {
                                $(this).addClass('validation-error');
                                $(this).before('<div class="validation-message">' + $(this).attr('data-valmsg') + '</div>');
                            }
                            valid = false;
                            validthis = false;
                        } else if( checkusername($(this).val()) ) {
                            if( $('#is_advisor').val() == 1) {
                                // trust the advisor got it right. Make password non-mandatory
                                $('[type=password]').removeClass('mandatory mandatorya').css('opacity', '0.5').attr('disabled', '').val('');
                            } else {
                                if($(this).is('.touched')) {
                                    $(this).addClass('validation-error');
                                    $(this).before('<div class="validation-message">' + $(this).attr('data-valmsg') + '</div>');
                                }
                                valid = false;
                                validthis = false;
                            }
                        } else {
                            $('[type=password]').addClass('mandatory mandatorya').css('opacity', '').removeAttr('disabled');
                        }
                        break;
                    case 4: // Postcode lookup - moved to top of validation as it needs running on page load
                        break;
                    case 5: // Email
                        regx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        if( !regx.test($(this).val()) ) {
                            valid = false;
                            validthis = false;
                        }
                        break;
                    case 6: // Date
                        regx = /^\d\d\/\d\d\/\d\d\d\d$/;
                        if( !regx.test($(this).val()) ) {
                            valid = false;
                            validthis = false;
                        }
                        break;
                    case 6: // Prevent submission
                        valid = false;
                        validthis = false;
                        break;
                    default:
                        console.log('Unknown special validation ID');
                        break;
                };
            } else if(!$(this).is('.specval')) {
                // normal validation
                if($(this).is('select') && $(this).val() == 0) {
                    // shouldn't be zero
                    if($(this).is('.touched')) {
                        $(this).addClass('validation-error').parent().siblings('.question-text').addClass('validation-error');
                        $(this).before('<div class="validation-message">' + $(this).attr('data-valmsg') + '</div>');
                    }
                    valid = false;
                    validthis = false;
                } else if($(this).is('[type="checkbox"]') && $(this).parents('.question-row').find('[type=checkbox]:checked').length == 0) {
                    // nothing selected
                    if($(this).is('.touched')) {
                        $(this).addClass('validation-error').parent().siblings('.question-text').addClass('validation-error');
                        $(this).before('<div class="validation-message">' + $(this).attr('data-valmsg') + '</div>');
                    }
                    valid = false;
                    validthis = false;
                } else if($(this).is('[type="radio"]') && !$(this).is(':checked') && $(this).siblings('input:checked').length == 0) {
                    // nothing selected
                    if($(this).is('.touched')) {
                        $(this).addClass('validation-error').parent().siblings('.question-text').addClass('validation-error');
                        $(this).before('<div class="validation-message">' + $(this).attr('data-valmsg') + '</div>');
                    }
                    valid = false;
                    validthis = false;
                } else if($(this).val() == '') {
                    // nothing selected
                    if($(this).is('.touched')) {
                        $(this).addClass('validation-error').parent().siblings('.question-text').addClass('validation-error');
                        $(this).before('<div class="validation-message">' + $(this).attr('data-valmsg') + '</div>');
                    }
                    valid = false;
                    validthis = false;
                }
            }
            if($(this).is('.touched') && $(this).is('.mandatory') && $(this).is('input[type=text], input[type=email], select')) { // no ticks for sliders or tickboxes
                if (validthis) {
                    $(this).parents('.question-row').find('.tick').removeClass("invalid").addClass('valid');
                } else {
                    $(this).parents('.question-row').find('.tick').removeClass("valid").addClass('invalid');
                }
            }
        });
        if(valid) {
            $('#submit-next').fadeIn(500);
        } else {
            $('#submit-next').fadeOut(500);
        }
        return valid;
    }


    ////////////////////////////
//
// FUNCTIONS
    function checkpostcode(p) {
        // check postcode in postcodes table
        var a = $.ajax({
            method: "POST",
            async: false,
            url: "/ajax/checkpostcode",
            data: { 'p' : p },
        });
        if(a.responseJSON.status == 'error') {
            return false;
        } else {
            return a.responseJSON.valid;
        }
    }

    function get_postcode_choices(p) {
        // check postcode in postcodes table
        var a = $.ajax({
            method: "POST",
            async: false,
            url: "/ajax/postcode_lookup",
            data: { 'p' : p },
        });
        var data = a.responseJSON;
if(typeof data === 'undefined') { return; }
        if(typeof data.status !== 'undefined') {
            alert('First For Wellbeing only assesses users from Northamptonshire postcodes');
            $('#postcode').val('');
            return false;
        }
        addresses = data;
        $('select.contact-postcode-choices').empty().attr('name', 'pc'); // changing name prevents it being saved as it has no real choices in it
        $('select.contact-postcode-choices').prepend('<option value="" selected="selected" class="please-select">Please select your address or enter it below</option>');
        if(addresses.length == 0) {
            $('.postcode-choices-wrapper').remove();
            $('.postcode-hide').slideDown('slow');
            $('#manual-address').hide();
        } else {
            $.each( addresses, function( key, value ) {
                $('select.contact-postcode-choices').append( '<option value="' + value.Uprn + '">' + value.HouseNumber + ' ' + value.Street + '</option>' );
                //$('select.contact-postcode-choices').siblings('ul.options').append( '<li data-raw-value="' + value.Uprn + '">' + value.HouseNumber + ' ' + value.Street + '</li>' );
            });
        }
    }

    function checkusername(p) {
    // check username is available
    var a = $.ajax({
        method: "POST",
        async: false,
        url: "/ajax/checkusername",
        data: { 'p' : p },
    });
    if(a.responseJSON.status == 'error') {
        return false;
    } else {
        return a.responseJSON.exists;
    }
}

</script>
