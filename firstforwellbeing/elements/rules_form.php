<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<?php
	// Build options lists
	$questions_options = $operator_options = $rvt_options = '';
	$qlist = [];
	while($questions_list !== null and $row = $questions_list->fetch() ) {
		$questions_options .= '<option value="' . $row['id'] . '">' . htmlentities($row['title']) . '</option>';
		$qlist[$row['id']] = $row['title'];
	}

	while($ruletypes_list !== null and $row = $ruletypes_list->fetch() ) {
		$operator_options .= '<option value="' . $row['id'] . '">' . htmlentities($row['name']) . '</option>';
	}

	while($rulevaluetypes_list !== null and $row = $rulevaluetypes_list->fetch() ) {
		$rvt_options .= '<option value="' . $row['id'] . '">' . htmlentities($row['name']) . '</option>';
	}

	// Show existing rules
	if(count($rules) == 0) {
		echo'<div class="row"><div class="col-md-12 alert alert-warning">This item has no rules defined yet</div></div>';
	} else {
		$c = 1;
		echo'<table class="table table-striped">';
		foreach($ruleset as $ruleid => $dummy) {
			echo'<tr class="ruleset-container"><td class="col-md-1">' . $c . '</td><td class="col-md-11">';
			show_rule($ruleid, $rules);
			echo'</td></tr><tr><td class="col-md-1">OR...</td><td></td></tr>';
			$c++;
		}
		echo'</table>';
	}
?>

<table class="table">
    <tr id="add-new-rule-container">
        <td class="col-md-1">&nbsp;</td>
        <td class="vol-md-11">
            <button type="button" class="btn btn-primary" id="add-new-rule">Add a new rule</button>
        </td>
    </tr>
    <tr id="add-condition" style="display:none;">
        <form method="post" action="add">
            <td class="col-md-4 text-right">
                <input type="hidden" name="athemeid" value="<?php echo $athemeid; ?>" />
                <input type="hidden" name="parent_true" id="parent-true" value="0" />
                <select name="var_left" id="var-left" style="width:15em;">
                    <option value="0">Value...</option>
                    <option value="0" disabled="disabled">--- Builtin Types ---</option>
                    <?php echo $rvt_options; ?>
                    <option value="0" disabled="disabled">--- Questions ---</option>
                    <?php echo $questions_options; ?>
                </select>
                <select name="left-choices" id="left-choices" id="left-choices" style="display:none; max-width:10em;">
                </select>
                <br />
                <input type="text" name="manual_left" id="manual-left" style="display:none;" placeholder="Enter value" />
            </td>
            <td class="col-md-2 text-center">
                <select name="operator">
                    <option value="0">Comparison...</option>
                    <?php echo $operator_options; ?>
                </select>
            </td>
            <td class="col-md-4">
                <select name="var_right" id="var-right" style="width:15em;" id="right-term">
                    <option value="0">Value...</option>
                    <option value="0" disabled="disabled">--- Builtin Types ---</option>
                    <?php echo $rvt_options; ?>
                    <option value="0" disabled="disabled">--- Questions ---</option>
                    <?php echo $questions_options; ?>
                </select>
                <select name="right_choices" id="right-choices" id="right-choices" style="display:none; max-width:10em;">
                    <br />
                    <input type="text" name="manual_right" id="manual-right" style="display:none;" placeholder="Enter value" />
            </td>
            <td class="col-md-1">
                <button type="submit" class="btn btn-success">Add</button>
            </td>
            <td class="col-md-1">
                <button type="button" class="btn" id="cancel">Cancel</button>
            </td>
        </form>
    </tr>
</table>







<script type="text/javascript">
$(document).ready( function() {
	// show text input boxes
	$('#var-left').change( function(e) {
		if( $(this).val() < 0 ) {
			$('#manual-left').slideDown(500);
			$('#right-choices, #left-choices').hide(0)
			$('#var-right, #var-left').fadeIn(500);
		} else {
			// check for dropdown choices for selected field
			$('#manual-left').slideUp(500);
			getchoices( $('#var-left').val(), 'l' );
		}
	});
	$('#var-right').change( function(e) {
		if( $(this).val() < 0 ) {
			$('#manual-right').slideDown(500);
			$('#right-choices, #left-choices').hide(0)
			$('#var-right, #var-left').fadeIn(500);
		} else {
			$('#manual-right').slideUp(500);
			getchoices( $('#var-right').val(), 'r' );
		}
	});
	// handle adding a condition
	$('.condition-add').click( function(e) {
		$(this).parents('tr.add-condition-button').hide().after($('#add-condition'));
		$('#parent-true').val( $(this).attr('parentid') );
		$('#add-condition').show();
	});
	// cancel adding a condition
	$('#cancel').click( function(e) {
		$('#add-condition').hide();
		$('.add-condition-button').show();
		$('#add-new-rule-row').slideDown(500);
        $('#add-new-rule-container').show();
	});
	// handle deleting a condition
	$('.condition-delete').click( function(e) {
		if(confirm('Please confirm you wish to delete this condition from this rule\n\You cannot undo this action') ) {
			location.href = 'delete/' + $(this).attr('ruleid') + '/' + <?php echo $athemeid; ?> + '/' + $(this).attr('truepath');
		}
	});
	// handle adding a brand new rule
	$('#add-new-rule').click( function(e) {
        $(this).parents('#add-new-rule-container').hide().after($('#add-condition'));
        $('#add-condition').show();
	});
	// handle selecting a choice field's choice
	$('#left-choices').on('change', function(e) {
		$('#var-left').val(-1);
		$('#manual-left').val( $(this).val() );
	});
	// handle selecting a choice field's choice
	$('#right-choices').on('change', function(e) {
		$('#var-right').val(-1);
		$('#manual-right').val( $(this).val() );
	});
});


/////////////////////////////////////////
//
// FUNCTIONS

// Get applicable field choices
function getchoices(id, side) {
	busy();
	$.post('/ffw/ajax/getchoices', { 'id' : id },
		function(d) {
			unbusy();
			if(d.status != 'ok') {
				alert('An error occurred while retrieving the choices. Please try again');
				return;
			} else {
				if(d.data.length == 0) {
					// not a choice field
					$('#right-choices, #left-choices').hide(0)
					$('#var-right, #var-left').fadeIn(500).change();
					return;
				} else {
					// Put choices in dropdown
					if(side == 'l') {
						$('#right-choices').empty();
					} else {
						$('#left-choices').empty();
					}
					$.each(d.data, function(i, v) {
						if(side == 'l') {
							$('#right-choices').append('<option value="' + htmlEntities(v.value) + '">' + htmlEntities(v.choice) + '</option>');
						} else {
							$('#left-choices').append('<option value="' + htmlEntities(v.value) + '">' + htmlEntities(v.choice) + '</option>');
						}
					});
					if(side == 'l') {
						$('#var-right, #manual-right').hide(0);
						$('#right-choices').fadeIn(500);
						$('#right-choices').change(); // trigger updates
					} else {
						$('#var-left, #manual-left').hide(0);
						$('#left-choices').fadeIn(500);
						$('#left-choices').change();
					}
				}
			}
		}
		, 'json');
};

// PHP Htmlentities equivalent
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}
// 'Busy' whilst ajaxing
function busy() {
	$('body').attr('disabled', "disabled");
}
// Un-Busy
function unbusy() {
	$('body').removeAttr('disabled');
}



</script>

<?php
/////////////////////////////////////////
//
// FUNCTIONS

// Recursively output ruleset rules
/*
function show_rule($ruleid, $rules, $indent = 0) {
    $offset = '';
    $r = $rules[$ruleid];
    if($indent == 0) {
        echo'<div class="row rules-list">';
    } else {
        $offset = ' class="offset-md-1"';
    }
    echo'<div class="row">';
    echo'<div class="col-md-4 text-right" ' . $offset . '><span class="condition-term">' . ($r['left_valuetype'] == 4 ? $r['left_title'] : $r['left_value'])  . '</span></div>';
    echo'<div class="col-md-2 text-center"><span class="condition-operator">' . $r['name'] . '</span></div>';
    echo'<div class="col-md-4"><span class="condition-term">' . ($r['right_valuetype'] == 4 ? $r['right_title'] : $r['right_value']) . '</span></div>';
    echo'<div class="col-md-1"><span class="condition-delete" ruleid="' . $ruleid . '" truepath="' . $r['true_path'] . '">X</span></div>';
    echo'</div>';
    if($r['true_path'] > 0) {
        echo'<div class="row"><div class=""col-md-12">AND</div></div>';
        show_rule($r['true_path'], $rules, 1);
    } else {
        echo'<div class="row"><div class="col-md-12"><span class="condition-add" parentid="' . $ruleid . '"><br />Add condition</span></div></div>';
    }
    if($indent == 0) {
        echo'</div>';
    }
}*/

// Recursively output ruleset rules
function show_rule($ruleid, $rules, $indent = 0) {
	$offset = '';
	$r = $rules[$ruleid];
	if($indent == 0) {
		echo'<table class="table rules-list"><thead>';
        echo'<tr>';
        echo'<th class="col-md-4 text-right">Left hand value</th>';
        echo'<th class="col-md-2 text-center">Comparison</th>';
        echo'<th class="col-md-4">Right hand value</th>';
        echo'<th class="col-md-1">&nbsp;</th>';
        echo'<th class="col-md-1">&nbsp;</th>';
        echo'</tr></thead>';
	} else {
		$offset = ' class="offset-md-1"';
	}
	echo'<tr>';
	echo'<td class="col-md-4 text-right" ' . $offset . '><span>' . ($r['left_valuetype'] == 4 ? $r['left_title'] : (!empty($left_choice) ? $left_choice : $r['left_value']))  . '</span></td>';
	echo'<td class="col-md-2 text-center"><span>' . $r['name'] . '</span></td>';
	echo'<td class="col-md-4"><span>' . ($r['right_valuetype'] == 4 ? $r['right_title'] : (!empty($r['left_choice']) ? $r['left_choice'] : $r['right_value'])) . '</span></td>';
    echo'<td class="col-md-1">&nbsp;</td>';
    echo'<td class="col-md-1"><span class="btn btn-danger condition-delete" ruleid="' . $ruleid . '" truepath="' . $r['true_path'] . '">X</span></td>';
	echo'</tr>';
    echo'<tr><td class=""col-md-4">AND...</td><td></td><td></td><td></td><td></td></tr>';
	if($r['true_path'] > 0) {
		show_rule($r['true_path'], $rules, 1);
	} else {
		echo'<tr class="add-condition-button"><td class="col-md-4"><span class="condition-add btn btn-success" parentid="' . $ruleid . '">Add condition</span></td><td></td><td></td><td></td><td></td></tr>';
	}
	if($indent == 0) {
		echo'</table>';
	}
}
