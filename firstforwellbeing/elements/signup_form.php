<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<form id="signupform" method="post">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="row">
				<div class="col-md-12">
					<h3>Registration</h3>
					Please provide your email address and a username/password to secure your assessment data
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">Email</div>
				<div class="col-md-6"><input type="email" name="email" placeholder="Your email address" id="email" class="mandatory" /></div>
			</div>
			<div class="row">
				<div class="col-md-3">Username</div>
				<div class="col-md-6"><input type="text" name="username" id="username" placeholder="Choose a username" class="mandatory" /></div>
				<div class="col-md-1" style="display:none;" id="username-ok">&nbsp;</div>
			</div>
			<div class="row">
				<div class="col-md-3">Password</div>
				<div class="col-md-6"><input type="password" name="pwd" id="pwd" placeholder="Choose a password" class="mandatory" /></div>
				<div class="col-md-1" style="display:none;" id="pwd-ok">&nbsp;</div>
			</div>
			<div class="row">
				<div class="col-md-3">Confirm password</div>
				<div class="col-md-6"><input type="password" name="pwdc" id="pwdc" placeholder="Confirm your password" class="mandatory" /></div>
				<div class="col-md-1" style="display:none;" id="pwdc-ok">&nbsp;</div>
			</div>
			<div class="row">
				<div class="col-md-3">&nbsp;</div>
				<div class="col-md-6"><button type="submit" id="dosignup">Create user</button></div>
			</div>
		</div>
	</div>
</form>


<script type="text/javascript">
$(document).ready( function() {
	// check username on the fly
	$('#username').change( function(e) {
		if($('#username').val() == '') {
			$('#username-ok').fadeOut(250);
			return;
		}
		// check username is available
		$.post('/ajax/checkusername', { 'username' : $('#username').val() },
			function(d) {
				if(d.status != 'ok') {
					return;
				} else {
					if(d.exists) {
						$('#username-ok').css('background-color', 'red').text('Not available').fadeIn(250);
					} else {
						// username ok
						$('#username-ok').css('background-color', 'green').text('Username available').fadeIn(250);
					}
				}
			}
			, 'json'
		);
	});
	// check passwords match
	$('#pwd, #pwdc').change( function(e) {
		if( $('#pwd').val() != $('#pwdc').val() ) {
			$('#pwd-ok').css('background-color', 'red').text('Does not match').fadeIn(250);
			$('#pwdc-ok').css('background-color', 'red').text('Does not match').fadeIn(250);
		} else {
			$('#pwd-ok, #pwdc-ok').fadeOut(250);
		}
	});
});
</script>
