<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<script type="text/javascript" src="/packages/firstforwellbeing/js/radios-to-slider/js/jquery.radios-to-slider.min.js"></script>
<link rel="stylesheet" href="/packages/firstforwellbeing/js/radios-to-slider/css/radios-to-slider.css">

<?php
if($step === null) {
    $step = 0;
} else {
    $step--; // needs to be zero-aligned

}
$questions = $assessment->themes[$step]['questions'];
?>
<section class="section col-md-10 col-md-offset-1"><br /><br />
    <div class="progress-bar progress-step-<?php echo $step + 1; ?>"></div>
    <h1 class="text-pink">Step <?php echo ($step + 1) . ' - ' . htmlentities($assessment->themes[$step]['name']); ?></h1>
    <div class="col-md-12">
        <form method="post" action="" id="form" style="opacity:0;">
            <?php
                if(in_array('FFW Advisors', $usergroups)) {
                    echo'<input type="hidden" name="is_advisor" id="is_advisor" value="1" />';
                } else {
                    echo'<input type="hidden" name="is_advisor" id="is_advisor" value="0" />';
                }
                echo'<input type="hidden" name="step" value="' . ($step + 1) . '" />';
                echo'<input type="hidden" name="aid" value="' . $assessment->id . '" />';
                $theme_name = $assessment->themes[$step]['theme_name'];
                //output questions
                foreach($questions as $row) {
                    // Look for existing val
                    if(isset($response_collection['responses'][$row['id']]) and !empty($response_collection['responses'][$row['id']]['value'])) {
                        $value = $response_collection['responses'][$row['id']]['value'];
                    } else {
                        // check for response id if not value
                        if ($response_collection['responses'][$row['id']]['response_id']) {
                            $value = $response_collection['responses'][$row['id']]['response_id'];
                        } else {
                            $value = null;
                        }
                    }
                    echo'<div class="row question-row ' . ($row['invisible'] ? '' : $row['css'] . ' ') . ($row['invisible'] ? 'invisible ' : '') . '" id="row-' . $row['id'] . '" data-handle="' . $row['data_handle'] . '">';
                    if($row['show_label']) {
                        if($row['label_pos']) {
                            echo '<div class="col-md-6 col-sm-6 col-xs-5 question-text" ' . ($row['question_type'] == 10 ? ' style="padding-top:40px;"' : '') . '>' . ($row['question_type'] != 14 ? htmlentities($row['question_text']) : '') . (($row['question_type'] == 7 and ($row['mandatory'] == 1 or $row['assessor_mandatory'] == 1 and in_array($usergroups, 'Assessor'))) ? ' *' : '') . '</div>';
                            echo'<div class="col-md-5 col-sm-5 col-xs-4 text-right">' . "\n" . get_question_element($row, $value, $response_collection, $usergroups) . "\n" . '</div>';
                            echo'<div class="col-md-1 col-sm-2 col-xs-4 tick"></div>';
                        } else {
                            echo '<div class="col-md-12 col-sm-12 col-xs-12 question-text">' . ($row['question_type'] != 14 ? htmlentities($row['question_text']) : '') . (($row['question_type'] == 7 and ($row['mandatory'] == 1 or $row['assessor_mandatory'] == 1 and in_array($usergroups, 'Assessor'))) ? ' *' : '') . '</div>';
                            echo'<div class="col-md-11 col-sm-10 col-xs-9">' . "\n" . get_question_element($row, $value, $response_collection, $usergroups) . "\n" . '</div>';
                            echo'<div class="col-md-1 col-sm-2 col-xs-3  tick"></div>';
                        }
                    } else {
                        echo'<div class="col-md-11 col-sm-10 col-xs-9">' . "\n" . get_question_element($row, $value, $response_collection, $usergroups) . "\n" . '</div>';
                        echo'<div class="col-md-1 col-sm-2 col-xs-3 tick"></div>';
                    }
                    echo'</div>';
                }
                echo'<div class="row">';
                echo'<div class="col-md-3 text-left">' . ($step > 0 ? '<button type="button" name="prev" value="previous" id="submit-prev" class="button button-prev button-green button-lrg button-block">Previous</button>' : '&nbsp;' ) . '</div>';
                echo'<div class="col-md-3 col-md-offset-6 text-right"><button type="submit" name="next" value="next" id="submit-next" class="button button-green button-lrg button-block button-next button-progress" style="display:none;">' . (($step - 1) < $maxstep ? 'Next' : 'Finish') . '</button></div>';
            ?>
            </div>
        </form>
        <div id="dummy">&nbsp;</div>
    </div>
</section>

    <div style="z-index: 10000; display: none; padding: 14px; background-color: white; border: black 3px solid; font-size: 70%; width: 50%; position: relative; float:left;" id="infopane-tandc" class="infopane">
                            <strong>Terms &amp; Conditions</strong><br />
                            <p><b>Why do we collect personal data about you?</b><br />
We collect personal data from you for the purpose of providing wellbeing services to you. These services can be provided by a range of partner organisations which could include, but are not limited to:
<ul>
<li>Relevant Northamptonshire County Council teams</li>
<li>Relevant Health professionals</li>
<li>Relevant local organisations</li>
</ul>
        </p>
        <p>
We will use the information to identify and make referrals to appropriate services; to monitor our work; to report on progress made; and to fulfil our statutory obligations and statutory returns as set by the law. The primary data owner will be First for Wellbeing.</p>

<p><b>How FFW protects and uses your personal data?</b><br />
All personal data we collect from you complies with the data protection principles, as stated in the Data Protection Act 1998 (DPA) and NCCs Data Protection Registration with the Information Commissioners Office; for full list please check this website: https://ico.org.uk/ESDWebPages/DoSearch?reg=165079</p>
<p>The personal data we collect may be held as an electronic record on data systems managed by First for Wellbeing (FFW) or as a paper record. The records are only seen by staff who need the personal data so they can do their job. The security of the data follows FFW policies on Information Management. We make every effort to keep your personal data accurate. If you tell us of any changes in your circumstances, we can update the records with the personal data you choose to share with us.</p>
<p>We will keep the personal data for no longer than is necessary. Sometimes, the law sets the length of time personal data must be kept.</p>

<p><b>Sharing personal data</b><br />
So we can provide the right services at the right level, we may share your personal data within county council services or with relevant organisations in line with FFW data registration, please see link above. Where this is necessary we will comply with all aspects of the Data Protection Act 1998.</p>

<p><b>Your rights</b><br />
You have the right to ask us to stop processing your personal data in relation to any FFW service. However, this may stop us delivering a service to you. Where possible, we will do as you ask, but we may need to hold or process personal data to comply with a legal requirement. If you find that the personal data that we hold is no longer accurate, you have the right to have this corrected. Please contact the service holding the personal data or our Customer Services Centre for this.</p>

<p><b>Further information</b><br />
If you would like further information about the personal data we hold or if you have a complaint about how your personal data has been used, please contact us at info@firstforwellbeing.co.uk with an email titled Freedom of Information and Data Protection.</p>
                            </div>

    <div style="z-index: 10000; display: none; padding: 14px; background-color: white; border: black 3px solid; font-size: 70%; width: 20%; position: relative; float:left;" id="infopane-on_benefits" class="infopane">
                            <p><b>Why we need to know this?</b><br>
                            If you receive benefits you may be eligible to access some of our paid-for services and programmes for free.</p>
                            </div>

    <div style="z-index: 10000; display: none; padding: 14px; background-color: white; border: black 3px solid; font-size: 70%; width: 20%; position: relative; float:left;" id="infopane-iagreeyoucansharemyinformationwithyourpartner" class="infopane">
										<p><b>Why we share your information?</b><br>
										We will only share your details if you need to be referred to one of our trusted partners, so you can be registered for one of the First for Wellbeing’s programmes or services</p>
										</div>
<?php
    // load js elements
    View::element('jscalcfields', ['rc' => $response_collection ], 'firstforwellbeing');
	View::element('jsvalidation', [], 'firstforwellbeing');
    View::element('jsruleparser', ['questions' => $questions], 'firstforwellbeing');
    View::element('jsextradata', [], 'firstforwellbeing');
    View::element('bootstrap-datepicker.min', [], 'firstforwellbeing');
?>



<script type="text/javascript">
    uID = <?php echo ($uID === null ? 'null' : $uID); ?>;
    username = '<?php echo $username; ?>';
    dovalidation = <?php echo (count($response_collection) > 0 ? 'true' : 'false'); ?>;
    user_postcode = '<?php echo $user_postcode;?>';
    $(document).ready( function() {
        // put info icons in where needed
        $('[data-handle=tandc] label').append('&nbsp;<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/i.png" class="info-icon" data-for="tandc"/>');
        $('[data-handle=on_benefits] .question-text').append('&nbsp;<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/i.png" class="info-icon" data-for="on_benefits"/>');
        $('[data-handle=iagreeyoucansharemyinformationwithyourpartner] .question-text').append('&nbsp;<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/i.png" class="info-icon" data-for="on_benefits"/>');
        // info click
        $('.info-icon').on('mouseenter', function(e) {
            var offset = $(this).offset();
            $('#infopane-' + $(this).attr('data-for')).fadeIn(500).offset({left:offset.left, top:offset.top});
        });
        $('.infopane').on('mouseleave', function(e) {
            $(this).fadeOut(500);
        });
        // Force rule check on initial load
        $('.question').first().trigger('change');
        if(dovalidation) {
            ffw_validate_all($('#form'), null);
        }
        dovalidation = true;
        // Sliderise radio sliders
        $('.slider').radiosToSlider();
        $('#form').fadeTo(1000,1);

        // save some info about select elements
        var selecth = $('select').height();
        var selectoh = $('select').outerHeight();

        // Make 'please select' unselectable after something else selected in a dropdown
        $('#form select').on('change', function(e) {
            $(this).find('.please-select').remove();
        });

        // Expand select on focus
        $('select').on('focus', function(e) {
            var count = $(this).find('option').length;
            $(this).css('height', selectoh);
            $(this).attr('size', count);
            var tweak = 1;
            if(navigator.userAgent.indexOf('Edge') != -1) {
                tweak = 6; // Edge kludge. MS doesn't seem to know the real height of its options. Quel surprise.
            }
            $(this).animate( { 'height' : selectoh + ($(this).height() + tweak) * (count - 1) }, 500 );
        });

        // Collapse select on change
        $('select').on('change', function(e) {
            $(this).animate( { 'height' : selectoh }, 500, 'swing', function() {
                $(this).attr('size', 1).css('height', 'auto').blur();
            } );
        });


        // Collapse select on blur
        $('select').on('blur', function(e) {
            $(this).animate( { 'height' : selectoh }, 500, 'swing', function() {
                $(this).attr('size', 1).css('height', 'auto');
            } );
        });


        // handle prev click
        $('#submit-prev').click( function(e) {
            $('#form').attr('novalidate', 'novalidate'); // defeat email validation, etc
            $('#form').attr('action', '/assessment/prevstep'); // shouldn't need to validate or save
            $('#form').trigger('submit');
        });
        // handle next click
        $('#submit-next').click( function(e) {
            var valid = ffw_validate_all( $('#form'), null );
            if(valid) {
                // remove anything hidden by a rule so it's not submitted, preventing empty values being attempted in the DB
                $('.hidden-by-rule').remove();
                $('#form').attr('action', '/assessment/nextstep');
            } else {
                e.preventDefault();
            }
        });
        // Get around date input dd-mm-yyy browser-provided placeholder
        $('.dateinput').focus(function(e) {
            //$(this).attr('type', 'date');
        });
        $('.dateinput').blur(function(e) {
            //$(this).attr('type', 'text');
        });
        // Mark questions after they've been touched
        $('.question').focus( function(e) {
           $(this).addClass('touched');
        });
        // validate on change
        $('.question').change( function(e) {
            if(dovalidation) {
                valid = ffw_validate_all($('#form'), this);
            }
        });
        // force username check if not empty
        if($('input.Username').val() != '') {
            $('input.Username').change();
        }
        // check for username fields and run their special validation
        $('[specval="3"]').change();

        // Bootstrap Datepicker
        $('input.dateinput').datepicker({
            format: 'dd/mm/yyyy'
        }).on('changeDate', function(e){
            //
        });

        // Get postcode selection
        $('.contact-postcode-choices').change( function() {
            var address_text = '';
            var choice = $('.contact-postcode-choices option:selected').val();
            $.each(addresses, function( key, value ) {
                if( choice == value.Uprn ) {
                    $('.contact-address1').val( value.HouseNumber + " " + value.Street ).addClass('touched').change();
                    $('.contact-address_2').val( value.DependentLocality ).addClass('touched').change();
                    $('.contact-town').val( value.PostTown ).addClass('touched').change();
                    $('.contact-county').val( 'Northamptonshire' ).addClass('touched').change();
                }
            });
            $('.postcode-hide').slideDown('slow');
        });

        // make password field
        $('[type=password]').on('change', function(e) {
            $('#password-hidden').remove();
            pw = $(this).val();
            $('form').prepend('<input type="hidden" id="password-hidden" name="password" value="' + pw + '" />')
        });

        // drop-down arrow focuses select
        $('.select-arrow').on('click', function(e) {
            var id = $(this).attr('data-id');
            $('[name=' + id + ']').focus();
        });
    });

    function to_db_date(ukdate) {
        return ukdate.substring(6) + '-' + ukdate.substring(3, 5) + '-' + ukdate.substring(0,2);
    }

</script>

<?php
/////////////////////////////////////
//
// Functions

function get_question_element($row, $value = null, $response_collection, $usergroups) { // don't seem to be able to globalise $response_collection
    $classes = [];
    if($response_collection['user_id'] > 0 and !in_array('FFW Advisors', $response_collection['usergroups'])) {
        switch($row['data_handle']) {
            case 'username':
                // prefill
                $value = $response_collection['username'];
                break;
            case 'password':
            case 'confirmpassword':
                // hide password fields for logged in users
                $row['mandatory'] = $row['assessor_mandatory'] = 0;
                $row['invisible'] = 1;
                $row['special_validation'] = '';
                $row['disabled'] = 1;
                break;
        }
    }
	// check for mandatory-ness
	$mandtext = '';
	if($row['mandatory'] and !in_array('Registered user', $usergroups)) {
		$classes[] = 'mandatory';
		$mandtext = ' *';
	}
	if($row['assessor_mandatory'] and in_array('Registered user', $usergroups)) {
		$classes[] = 'mandatorya';
        $mandtext = ' *';
	}
	if($row['special_validation'] > 0) {
		$classes[] = 'specval';
		$specval = 'specval="' . $row['special_validation'] . '"';
	}
	$class = 'class=" question ' . implode(' ', $classes) . ' ' . $row['css'] . ' ';
	// handle question type
	if( $row['question_type'] != 1 and $value === null and !empty($row['default_value']) ) {
	    if(empty($row['choices'])) {
	        $value = $row['default_value'];
        } else {
	        // multiple choice, so default is a response id. Find it
            foreach($row['choices'] as $choice) {
                if($row['default_value'] == $choice['value']) {
                    $value = $choice['id'];
                    break; // done
                }
            }
        }
	}
	// prefill profile fields
    if(!empty($response_collection['user_profile'])) {
	    if(!empty($row['profile_mapping'])) {
            $value = $response_collection['user_profile'][$row['profile_mapping']];
            if($value = '') { $value = ''; }
        }
    }
	switch($row['question_type']) {
        case 1:
            // Date
            if ($value === null and !empty($row['default_value'])) {
                $value = preg_replace('#(\d+)(.*?)(\d+)(.*?)(\d+)#', "$5-$3-$1", $row['default_value']);
            }
            $html = '<input type="text" name="' . $row['id'] . '" placeholder="' . htmlentities($row['placeholder']) . ' (DD/MM/YYYY)' . $mandtext . '" ' . $class . ' dateinput" value="' . htmlentities($value) . '"' . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '"" />';
            break;
        case 2:
            // Text short
            $html = '<input type="text" name="' . $row['id'] . '" placeholder="' . htmlentities($row['placeholder']) . $mandtext . '" ' . $class . '" value="' . htmlentities($value) . '"' . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '"" />';
            break;
        case 3:
            // Text long
            $html = '<textarea name="' . $row['id'] . '" placeholder="' . htmlentities($row['placeholder']) . $mandtext . '" ' . $class . "\" $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '" >' . htmlentities($value) . '</textarea>';
            break;
        case 4:
            // Number int
            $html = '<input type="number" name="' . $row['id'] . '" placeholder="' . htmlentities($row['placeholder']) . $mandtext . '" ' . $class . ' validate-int" value="' . htmlentities($value) . '"' . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '" />';
            break;
        case 5:
            // Number decimal
            $html = '<input type="number" name="' . $row['id'] . '" placeholder="' . htmlentities($row['placeholder']) . $mandtext . '" ' . $class . ' validate-dec" value="' . htmlentities($value) . '"' . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '" />';
            break;
        case 6:
            // Dropdown
            $html = '<select name="' . $row['id'] . '" ' . $class . ' qchoice"' . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '">';
            if (!isset($response_collection['responses'][$row['id']]['response_id'])) {
                $html .= '<option value="" class="please-select">' . htmlentities($row['placeholder']) . $mandtext . '</option>';
            }
			foreach($row['choices'] as $choice) {
				$html .= '<option value="' . htmlentities($choice['id']) . '" ' . ($response_collection['responses'][$row['id']]['response_id'] == $choice['id'] ? 'selected="selected"' : '') . 'data-score="' . $choice['value'] . '">' . htmlentities($choice['choice']) . '</option>';
			}
			$html .= '</select><div class="select-arrow" data-id="' . $row['id'] . '">&nbsp;</div>';
			break;
		case 7:
			// Tickboxes
			$html = ''; $count = 0;
			$rcmr = $response_collection['responses'][$row['id']]['multiple_responses'];
			foreach($row['choices'] as $choice) {
				$html .= '<div><input type="checkbox" name="' . $row['id'] . '[]" id="' . $row['id'] . $count . '" ' . $class . ' qchoice" value="' . $choice['id'] . '" textval="' . htmlentities($choice['value']) . '" ' . ($rcmr[$choice['id']] == 1 ? 'checked="checked"' : '') . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '" data-score="' . $choice['value'] . '" /><label for="' . $row['id'] . $count++ . '">' . htmlentities($choice['choice']) . '</label></div><br />';
			}
			break;
		case 8:
			// Radio buttons
			$html = ''; $count = 0;
			foreach($row['choices'] as $choice) {
				$html .= '<input type="radio" name="' . $row['id'] . '" id="' . $row['id'] . $count . '" ' . $class . ' qchoice" value="' . $choice['id'] . '" textval="' . htmlentities($choice['choice']) . '" ' . ($value == $choice['value'] ? 'checked="checked"' : '') . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '" data-score="' . $choice['value'] . '" /><label for="' . $row['id'] . $count++ . '">' . htmlentities($choice['choice']) . '</label><br />';
			}
			break;
		case 9:
			// Yes/No icons
			$html = '<input type="radio" name="' . $row['id'] . '"  ' . $class . ' yesnoicon-yes" value="1" id="' . $row['id'] . '-yes"' . ($value == 1 ? 'checked="checked"' : '') . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '" data-score="1" /><label for="' . $row['id'] . '-yes" class="yesnoicon-yes"></label>';
			$html.= '<input type="radio" name="' . $row['id'] . '" ' . $class . ' yesnoicon-no" value="0" id="' . $row['id'] . '-no"' . ($value === 0 ? 'checked="checked"' : '') . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '" data-score="0" /><label for="' . $row['id'] . '-no" class="yesnoicon-no"></label>';
			break;
		case 10:
			// Radio icons
			$html = '';
			foreach($row['choices'] as $choice) {
				$html .= '<input type="radio" name="' . $row['id'] . '" id="' . $row['id'] . $count . '" ' . $class . ' radio-icon" value="' . $choice['id'] . '" textval="' . htmlentities($choice['choice']) . '" ' . ($value == $choice['id'] ? 'checked="checked"' : '') . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '" data-score="' . $choice['value'] . '" style="display:none;" /><div style="display:inline-block; vertical-align:top;"><label for="' . $row['id'] . $count . '" class="radio-icon" style="background-image:url(' . ( is_numeric($choice['icon']) ? \File::getByID($choice['icon'])->getURL() : '/packages/firstforwellbeing/firstforwellbeing/ffw_assessment/images/missing-icon.png' ) . ');"></label><br /><label for="' . $row['id'] . $count++ . '" class="radio-icon-text">' . htmlentities($choice['choice']) . '</label></div>';
			}
			break;
		case 11:
			// Email
			$html = '<input type="email" name="' . $row['id'] . '" placeholder="' . htmlentities($row['placeholder']) . $mandtext . '" ' . $class . '" value="' . htmlentities($value) . '"' . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '" />';
			break;
		case 12:
			// Password
			$html = '<input type="password" name="' . $row['id'] . '" placeholder="' . htmlentities($row['placeholder']) . $mandtext . '" ' . $class . '" ' . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '" ' . ($row['disabled'] == 1 ? 'disabled' : '') . ' />';
			break;
		case 13:
			// Slider
			$html = '';
			$html .= '<div class="slider" id="sliderdiv' . $row['id'] . '">';
			$count = 0;
			foreach($row['choices'] as $choice) {
				$html .= '<input type="radio" name="' . $row['id'] . '" id="' . $row['id'] . $count . '" ' . $class . ' qchoice" value="' . $choice['id'] . '" textval="' . htmlentities($choice['choice']) . '" ' . ($value == $choice['id'] ? 'checked="checked"' : '') . " $specval" . ' data-valmsg="' . htmlentities($row['validation_message']) . '" data-score="' . $choice['value'] . '" /><label for="' . $row['id'] . $count++ . '">' . htmlentities($choice['choice']) . '</label>';
			}
			$html .= '</div>';
			break;
        case 14:
            // Plain text copy
            $html = '<span name="' . $row['id'] . '"' . $class . " $specval " . '">' . htmlentities($row['default_value']) . '</span>';
            break;
        case 15:
            // Calculated field
            $html = '<span name="' . $row['id'] . '" ' . $class . 'calculation" data-qid="' . $row['id'] . '" data-val="' . htmlentities($row['default_value']) . '"></span>';
            $html .= '<input type="hidden" name="' . $row['id'] . '" id="' . $row['id'] . '" />';
            break;
        case 16:
            // HTML field
            $html = $row['question_text'];
            break;
		default:
			$html = 'Error question type not supported';
			break;
	}
	return $html;
};
