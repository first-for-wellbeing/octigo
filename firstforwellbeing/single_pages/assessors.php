<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<div id="content" class="container">
    <div class="row">
        <section class="section col-md-12">
            <div class="wysiwyg">
                <?php //todo echo $post->post_content; ?>
                <div class="row">
                    <?php if(in_array('FFW Advisors', $usergroups)): ?>
                        <div class="col-md-3"><a href="/assessment-tool/assessments-entries/?view=assessor" class="button button-black" style="height:56px;line-height:56px;width:100%">My Assessments</a></div>
                    <?php endif; ?>
                    <?php if(in_array('FFW Advisors', $usergroups)): ?>
                        <div class="col-md-3"><a href="/assessment-tool/assessments-entries/?view=region&region=<?php // todo print end(get_user_meta($user->ID, 'region')); ?>" class="button button-black" style="height:56px;line-height:56px;width:100%">View Region</a></div>
                    <?php endif; ?>
                    <?php if(in_array('FFW Advisors', $usergroups)): ?>
                        <div class="col-md-3"><a href="/assessment-tool/assessments-entries/?view=all" class="button button-black" style="height:56px;line-height:56px;width:100%">View All</a></div>
                    <?php endif; ?>
                    <?php if(in_array('FFW Advisors', $usergroups)): ?>
                        <div class="col-md-3"><a href="/assessment-tool/assessments-entries/?view=retakes" class="button button-black" style="height:56px;line-height:56px;width:100%">Awaiting Retake</a></div>
                    <?php endif; ?>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h2>Filter Results</h2>
                    </div>
                    <div class="col-md-6">
                        <?php if ( findHealthyWeightAssessmentFlag > 0 ) { // todo ?>
                            <a class="button button-green button-block js-export-entries" href="<?php echo get_template_directory_uri() ?>/users/assessment-update.php" title="Update Assessments ->REQUIRED">Update Assessments -> REQUIRED</a>
                        <?php }
                        ?>
                    </div>
                </div>
                <form method="POST" action="" class="js-filter-entries" id="filter_form">
                    <input type="hidden" name="current_page" id="current_page" />
                    <div class="row">
                        <div class="input-wrapper input-wrapper-sml col-md-3">
                            <div class="form-control form-control-sml">
                                <input type="text" name="fromdate" id="fromdate" placeholder="From dd/mm/yyyy" title="From date" style="font-size:18px" <?php if(!empty($_POST['fromdate'])){echo 'value="' . htmlentities($_POST['fromdate']) . '"';}?>>
                            </div>
                        </div>
                        <div class="input-wrapper input-wrapper-sml col-md-3">
                            <div class="form-control form-control-sml">
                                <input type="text" name="todate" id="todate" placeholder="To dd/mm/yyyy" title="To date" style="font-size:18px" <?php if(!empty($_POST['todate'])){echo 'value="' . htmlentities($_POST['todate']) . '"';}?>>
                            </div>
                        </div>
                        <div class="input-wrapper input-wrapper-sml col-md-3">
                            <div class="form-control form-control-sml">
                                <input type="text" name="custname" id="custname" placeholder="Name" title="Name" style="font-size:18px" <?php if(!empty($_POST['custname'])){echo 'value="' . htmlentities($_POST['custname']) . '"';}?>>
                            </div>
                        </div>
                        <div class="input-wrapper input-wrapper-sml col-md-3">
                            <div class="form-control form-control-sml">
                                <input type="text" name="email" id="email" placeholder="Email" title="Email" style="font-size:18px" <?php if(!empty($_POST['email'])){echo 'value="' . htmlentities($_POST['email']) . '"';}?>>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-wrapper input-wrapper-sml col-md-3">
                            <div class="form-control form-control-sml">
                                <input type="text" name="assessment_id" id="assessment_id" placeholder="Assessment ID" title="Assessment ID" style="font-size:18px" <?php if(!empty($_POST['assessment_id'])){echo 'value="' . htmlentities($_POST['assessment_id']) . '"';}?>>
                            </div>
                        </div>
                        <div class="input-wrapper input-wrapper-sml col-md-3">
                            <div class="form-control form-control-sml">
                                <input type="text" name="assessor_user_id" id="assessor_user_id" placeholder="Assessor ID" title="Assessor ID" style="font-size:18px" <?php if(!empty($_POST['assessor_user_id'])){echo 'value="' . htmlentities($_POST['assessor_user_id']) . '"';}?>>
                            </div>
                        </div>
                        <div class="input-wrapper input-wrapper-sml col-md-3">
                            <div class="form-control form-control-sml">
                                <input type="text" name="customer_id" id="customer_id" placeholder="Cust ID" title="Cust ID" style="font-size:18px" <?php if(!empty($_POST['customer_id'])){echo 'value="' . htmlentities($_POST['customer_id']) . '"';}?>>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="button button-black" style="height:56px;line-height:56px;width:100%">Filter</button>
                        </div>
                    </div>
                </form>

                <form id="formPagination" class="form-horizontal" action="" method="post">
                    <div class="input-wrapper input-wrapper-sml">
                        <label for="number_per_page">NUMBER PER PAGE</label>
                        <select id="number_per_page" class="fancy-select fancy-select-tidy" name="number_per_page" onchange="document.getElementById('formPagination').submit();">
                            <option value="20" <?php echo $perpage==20?'selected="selected"':'';?>>20</option>
                            <option value="30"<?php echo $perpage==30?'selected="selected"':'';?>>30</option>
                            <option value="40"<?php echo $perpage==40?'selected="selected"':'';?>>40</option>
                        </select>
                    </div>
                </form>

            </div>

                <table class="table">
                    <thead>
                    <tr>
                        <th>Assessment ID</th>
                        <th>Assessor ID</th>
                        <th>Cust ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <?php  if(in_array('weight_watchers', $usergroups)) { ?><th>Status</th><?php } // todo ?>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($total > 0) {
                        $custid_seen = [];
                        foreach($assessments as $aid => $assessment) {
                            ?>
                            <tr data-assessment-id="<?= $assessment->assessment_id ?>" data-customer-id="<?= $assessment->customer_id ?>" data-name="<?= $assessment->first_name . ' ' . $assessment->last_name ?>" data-email="<?= $assessment->email ?>">
                                <td><?= $assessment['id'] ?></td>
                                <td><?= $assessment['assigned_to'] ?></td>
                                <td><?= $assessment['user_id'] ?></td>
                                <td><?= $assessment['firstname']['value'] . ' ' . $assessment['last_name']['value'] ?></td>
                                <td><?= $assessment['email']['value'] ?></td>
                                <?php if(in_array('weight_watchers', $usergroups)) { ?><td><?= ($assessment->status == 1) ? 'Contacted' : 'Waiting' ?></td><?php } // todo ?>
                                <td><a href="/assessors/show/<?php echo $assessment['id']; ?>" title="View"><strong>View</strong></a>

                                    <?php if ($assessment['over12weeks'] == 1 and !isset($custid_seen[$assessment['user_id']]) ) {
                                        echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="/myassessment/' . $assessment['id'] . '" title="View"><strong>Retake</strong></a>';
                                    }   ?>

                                </td>
                            </tr>
                            <?php
                            $custid_seen[$assessment['user_id']] = 1; // help to hide retake link for older entries
                        }

                    } else {
                        ?>
                        <tr>
                            <td colspan="6">No assessments found</td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <?php
                if( !empty($assessments) ) {
                    echo show_pagination($current_page, $total, $perpage);
                }
                ?>
            <?php
            if(in_array('canExportAssessments', $usergroups)) { ?>
                <div class="row">
                    <div class="col-md-3 col-md-push-9">
                        <form method="post" id="exportform" action="<?php echo get_template_directory_uri() ?>/users/assessment-export.php">
                            <input type="hidden" name="xfromdate" id="xfromdate" <?php if(!empty($_POST['fromdate'])){echo 'value="' . htmlentities($_POST['fromdate']) . '"';}?>>
                            <input type="hidden" name="xtodate" id="xtodate" <?php if(!empty($_POST['todate'])){echo 'value="' . htmlentities($_POST['todate']) . '"';}?>>
                            <input type="hidden" name="xcustname" id="xcustname" <?php if(!empty($_POST['custname'])){echo 'value="' . htmlentities($_POST['custname']) . '"';}?>>
                            <input type="hidden" name="xemail" id="xemail" <?php if(!empty($_POST['email'])){echo 'value="' . htmlentities($_POST['email']) . '"';}?>>
                            <input type="hidden" name="xassessment_id" id="xassessment_id" <?php if(!empty($_POST['assessment_id'])){echo 'value="' . htmlentities($_POST['assessment_id']) . '"';}?>>
                            <input type="hidden" name="xcustomer_id" id="xcustomer_id" <?php if(!empty($_POST['customer_id'])){echo 'value="' . htmlentities($_POST['customer_id']) . '"';}?>>
                            <a class="button button-green button-block js-export-entries" href="" onclick="document,getElementById('exportform').submit();return false;" title="Export list">Export</a>
                        </form>
                    </div>
                </div>
            <?php } ?>
        </section>

    </div>
</div>



<?php
View::element('jsuistuff', [], 'firstforwellbeing');

//////////////////////////////////////////////////////////
///
/// Functions
///

function show_pagination($currentPage, $total, $perpage) {
    $pages = ceil($total / $perpage);
    $a = max(1, $current_page - 2);
    $b = min($pages, $current_page + 2);
    $html = '';
    $html .= '<div class="row"><div class="col-md-6 col-md-offset-3" style="text-align:center;">';
    if($current_page == 1) {
        $html .= '<div class="page-prev">Prev</div>';
    } else {
        $html .= '<div class="page-prev go-prev">Prev</div>';
    }
    for($i = $a; $i <= $b; $i++) {
        $html .= '<div class="go-page" data-page="' . $i . '">' . $i . '</div>';
    }
    if($current_page == $pages) {
        $html .= '<div class="page-next">Next</div>';
    } else {
        $html .= '<div class="page-next go-next">Next</div>';
    }
    return $html;
}