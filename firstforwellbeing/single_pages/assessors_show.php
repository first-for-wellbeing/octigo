<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<div id="content" class="container">
    <div class="row">

        <section class="section">
            <?php
            if( isset($_GET['status_output']) && $_GET['status_output'] == 1 ) {
                ?>
                <p class="msg-success element-fade-out">Contact status has been updated</p>
                <?php
            } else if(isset($_GET['status_output'])) {
                ?>
                <p class="msg-error">Sorry there was an error updating the contact status please try again.</p>
                <?php
            }
            ?>

            <div class="wysiwyg">
            </div>
            <h1 class="text-primary underline"><?php echo $assessment['responses']['firstname'] . ' ' .$assessment['responses']['lastname'] ?>'s Details</h1>

                <a href="/myassessment/<?php echo $assessment['id'] ?>" class="button button-green" style="display:inline-block;margin-top:-20px;">View Assessement Result Page</a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="/assessors/userprofile/<?php echo $assessment['user_id']; // todo ?>" class="button button-green" style="display:inline-block;margin-top:-20px;">View Customer Profile</a>
                <br /><br /><br />
            <?php
            if(in_array('Assessor Admins', $usergroups)){
                ?>
                <label>Change Assessor:</label>
                <input type="hidden" name="assessment_id" value="<?php echo $_GET['assessment_id']; ?>">
                <input type="hidden" name="current_assessor" value="<?php echo $assessment->assessor_user_id; ?>">
                <p id="conf-text" style="display:none;"><strong>Assessor Saved</strong></p>
                <select name="change_assessor" id="change_assessor" >
                    <?php
                    foreach($assessors as $assessor => $k){
                        ?>
                        <option value="<?php echo $k; ?>" <?php if($assessment['assigned_to'] == $k){ echo'selected="selected"'; } ?>><?php echo $assessor; ?></option>
                        <?php
                    }
                    ?>
                </select>
                <script type="text/javascript">
                    $(document).ready( function() {
                        $('#change_assessor').on('change', function(e) {
                            location.replace('/assessors/changeassessor/' + $(this).val() + '/<?php echo $assessment['id'];?>');
                        });
                    });
                </script>
            <?php } ?>
            <?php ////////////// DIV/LI for the user notes ?>
            <div class="assessment-priority">
                <ul class="accordion">
                </ul>
            </div>



            <div class="row">
                <div class="col-md12 assessment-priority">
                    <ul class="accordion">
                        <li><!-- contact details -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-green text-center">&nbsp;</div>
                                <span>Contact details</span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr >
                                            <th>User ID</th>
                                            <td><?php echo $assessment['user_id']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Name</th>
                                            <td><?php echo $assessment['responses']['firstname'] . ' ' .$assessment['responses']['lastname'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td><?php echo $assessment['responses']['email'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Address</th>
                                            <td><?php echo $assessment['responses']['addressline1']; ?>,<?php echo $assessment['responses']['addressline2']; ?><br />
                                                <?php echo $assessment['responses']['town']; ?>,<br />
                                                <?php echo $assessment['responses']['county']; ?><br />
                                                <?php echo $assessment['responses']['postcode']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Telephone number</th>
                                            <td><?php echo $assessment['responses']['telephonenumber']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Mobile</th>
                                            <td><?php echo $assessment['responses']['mobile']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Preferred method of contact</th>
                                            <td><?php if( $assessment['responses']['preferredmethodofcontact'] == 1) echo 'Email'; else if( $assessment['responses']['preferredmethodofcontact'] == 2) echo 'Telephone'; else echo 'Post'; ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li><!-- assessment details -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-green text-center">&nbsp;</div>
                                <span>Assessment details</span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr class="date-row">
                                            <th>Date</th>
                                            <td><?php echo date('d/m/Y H:i:s', strtotime($assessment['created_datetime']) ); ?></td>
                                        </tr>
                                        <tr>
                                            <th>Customer ID</th>
                                            <td><?php echo $assessment['user_id']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Assessment ID</th>
                                            <td><?php echo $assessment['id']; ?></td>
                                        </tr>
                                        <!--
												<tr>
													<th>Security Word</th>
													<td><?php echo $assessment->security_word; ?></td>
												</tr>
-->
                                        <tr>
                                            <th>Assessment number</th>
                                            <td><?php echo $assessment['assessment_number']; // todo ?></td>
                                        </tr>
                                        <tr>
                                            <th>Assessment completed by</th>
                                            <td><?php if ($assessment['responses']['onbehalfof'] == 0) { echo 'Myself'; } else if($assessment['responses']['relationtoassessee'] != 'Other') { echo $assessment['responses']['relationtoassessee']; } else { echo $assessment['responses']['relationother']; } ?></td>
                                        </tr>
                                        <tr>
                                            <th>Assessment conducted at</th>
                                            <td><?php if (empty($assessment['responses']['assessmentlocation']) || $assessment['responses']['assessmentlocation'] == "error") { echo 'N/A'; } else if($assessment['responses']['assessmentlocation'] != 'Other') { echo $assessment['responses']['assessment_location']; } else { echo $assessment['responses']['assessmentlocationother']; } ?></td>
                                        </tr>
                                        <tr>
                                            <th>Location</th>
                                            <td>
                                                <?php
                                                switch($assessment['responses']['assessmentlocation']) {
                                                    case 'GP':
                                                        echo $assessment['responses']['gplocations'];
                                                        break;
                                                    case 'Community Centres':
                                                        echo $assessment['responses']['communitycentres'];
                                                        break;
                                                    case 'Libraries':
                                                        echo $assessment['responses']['libraries'];
                                                        break;
                                                    case 'Pharmacy':
                                                        echo 'Pharmacy';
                                                        break;
                                                }
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li style="margin-bottom:50px;"><!-- customer info -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-green text-center">&nbsp;</div>
                                <span>Customer info</span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr>
                                            <th>NHS number</th>
                                            <td><?php echo $assessment['responses']['nhsnumber']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Date of birth</th>
                                            <td><?php echo $assessment['responses']['dateofbirth']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Has customer had a health check in last 4 years?</th>
                                            <td><?php echo $assessment['responses']['health_check_in_the_last_4_years']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Gender</th>
                                            <td><?php echo $assessment['responses']['whatgenderareyou']; ?></td>
                                        </tr>
                                        <?php
                                        if($assessment->gender == 2) {
                                            ?>
                                            <tr>
                                                <th>Pregnant?</th>
                                                <td><?php echo $assessment['responses']['pregnant']; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                            <th>Religion</th>
                                            <td><?php echo ($assessment['responses']['religion'] == 'Other') ? $assessment['responses']['religion_other'] : $assessment['responses']['religion']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Marital status</th>
                                            <td><?php echo ($assessment['responses']['whatisyourmaritalstatus'] == 'Other') ? $assessment['responses']['marital_other'] : $assessment['responses']['whatisyourmaritalstatus']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Parental status</th>
                                            <td><?php echo $assessment['responses']['parental_status']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Number of Dependants</th>
                                            <td><?php echo $assessment['responses']['dependants']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Disability Status</th>
                                            <td><?php echo ($assessment['responses']['disability_status'] == 'Other') ? $assessment['responses']['disability_status_other'] : (empty($assessment['responses']['disability_status']) || $assessment['responses']['disability_status'] == "error" ? "N/A" : $assessment['responses']['disability_status']) ; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Ethnic origin</th>
                                            <td><?php echo ($assessment['responses']['ethnic_origin'] == 'Other') ? $assessment['responses']['ethnic_origin_other'] : $assessment['responses']['ethnic_origin']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Sexuality</th>
                                            <td><?php echo $assessment['responses']['sexuality']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Are you pregnant?</th>
                                            <td><?php echo $assessment['responses']['pregnant'] ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li style="margin-bottom:50px;"><!-- customer notes -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-grey text-center">&nbsp;</div>
                                <span>Customer assessment notes</span>
                            </div>
                            <div class="accordion-content">
                                <div class="result-text">
                                    <div class="text-right">
                                        <a class="button button-green btn-edit-result assessor-add-note" href="#" title="Edit">Add note</a>
                                    </div><br />
                                    <?php
                                    if( $assessor_notes !== false ) {
                                        echo '<table width="100%" class="table table-fixed">';
                                        echo '<tr style="font-weight:bold;"><td width="20%">Date</td><td width="20%">Time</td><td width="20%">Assessor (ID)</td><td width="20%">Contact type</td><td width="20%">Action</td></tr>';
                                        foreach($assessor_notes as $key=>$row) {
                                            echo '<tr style="border-top:#444 1px dotted;"><td colspan="5">';
                                            echo '<table width="100%"><tr>';
                                            echo '<td width="20%" style="padding-bottom:15px;">' . $row['datepart'] . '</td>';
                                            echo '<td width="20%">' . $row['timepart'] . '</td>';
                                            echo '<td width="20%">' . htmlentities($row['display_name']) . " ({$row['created_by']})</td>";
                                            echo '<td width="20%">' . htmlentities($row['type_of_contact']) . '</td>';
                                            echo '<td width="20%">' . htmlentities($row['followup_action']) . '</td></tr>';
                                            echo '<tr><td colspan="5" style="padding-bottom:15px; line-height:2em;">'. nl2br(htmlentities($row['notes'])) . ($row['visible_to'] != '' ? '<div style="padding-top:10px; border-top:#444 1px dotted;"><b>VISIBLE TO:<br />' . preg_replace('/,/', ', ', $row['visible_to']) . "</b><br />" : '' )  . '</td></tr>';
                                            echo '</table>';
                                            echo '</td></tr>';
                                        }
                                        echo'</table>';
                                    }
                                    ?>

                                    <?php if(count($assessor_notes) > 1) { ?>
                                        <div class="text-right">
                                            <a class="button button-green btn-edit-result assessor-add-note" href="#" title="Edit">Add note</a>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="form-edit" style="display:none;">
                                    <form method="post" class="form-notes" id="form-notes" action="/assessors/saveassessornote">
                                        <input type="hidden" name="aid" value="<?php echo $assessment['id']; ?>" />
                                        <div class="input-wrapper input-wrapper-sml">
                                            <label for="user_note" class="sr-only">Add your own notes related to your assessment here</label>
                                            <div class="form-control form-control-textarea form-control-sml">
                                                <textarea id="user_note" placeholder="Add your own notes related to your assessment here" name="user_note" rows="3"></textarea>
                                            </div>
                                            <br />
                                            <select name="type_of_contact" class="col-md-6 fancy-select" id="type_of_contact">
                                                <option value="error" selected="selected" disabled="">Type of contact *</option>
                                                <?php
                                                foreach($types_of_contact as $key=>$val) {
                                                    echo'<option value="' . $val['id'] . '">' . htmlentities($val['type_of_contact']) . '</option>';
                                                }
                                                ?>
                                            </select><br /><br />
                                            <select name="followup_action" id="followup_action" class="fancy-select">
                                                <option value="error" selected="selected" disabled="">Followup actions *</option>
                                                <?php
                                                foreach($followup_actions as $key=>$val) {
                                                    echo'<option value="' . $val['id'] . '">' . htmlentities($val['followup_action']) . '</option>';
                                                }
                                                ?>
                                            </select><br /><br />
                                            Make this note visible to Providers of:
                                            <div class="row tickboxen">
                                                <style type="text/css">
                                                    .tickboxen input[type="checkbox"] {
                                                        visibility:visible;
                                                        display:inline-block;
                                                        float:none;
                                                        width:auto;
                                                    }
                                                    .tickboxen label::after, .tickboxen label::before {
                                                        content:none !important;
                                                        background:none !important;
                                                    }
                                                    .tickboxen label {
                                                        font-size:1em;
                                                        width:auto !important;
                                                        line-height:auto;
                                                        display:inline-block;
                                                        background:none !important;
                                                        margin-left:1em;

                                                    }
                                                </style>
                                                <div class="col-md-3">
                                                    <input type="checkbox" name="provider[]" value="smoking" style="-webkit-appearance:checkbox;" id="p-sm" /> <label for="p-sm" class="normal">Smoking</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="checkbox" name="provider[]" value="alcohol" style="-webkit-appearance:checkbox;" id="p-al" /> <label for="p-al" class="normal">Alcohol</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="checkbox" name="provider[]" value="finance" style="-webkit-appearance:checkbox;" id="p-fi" /> <label for="p-fi" class="normal">Financial</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="checkbox" name="provider[]" value="housing" style="-webkit-appearance:checkbox;" id="p-ho" /> <label for="p-ho" class="normal">Housing</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="checkbox" name="provider[]" value="social" style="-webkit-appearance:checkbox;" id="p-so" /> <label for="p-so" class="normal">Social</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="checkbox" name="provider[]" value="emotional" style="-webkit-appearance:checkbox;" id="p-em" /> <label for="p-em" class="normal">Emotional</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="checkbox" name="provider[]" value="employment" style="-webkit-appearance:checkbox;" id="p-ep" /> <label for="p-ep" class="normal">Employment</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="checkbox" name="provider[]" value="weight" style="-webkit-appearance:checkbox;" id="p-wm" /> <label for="p-wm" class="normal">Weight Management</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <a class="button button-black assessor-cancel-note" href="#" title="Cancel">Cancel</a>
                                            <button class="button button-green assessor-save-note" type="submit">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>
                        <li><!-- employment -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-<?php echo ($assessment['results']['Employment_priority'] > 0 ? $assessment['results']['Employment_priority'] : 'grey'); ?> text-center"><?php echo ($assessment['results']['Employment_priority'] > 0 ? 'Priority '. $assessment['results']['Employment_priority'] : 'N/A'); ?></div>
                                <span>Employment details </span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr>
                                            <th>Score</th>
                                            <td><?php echo ($assessment['responses']['employmentscore'] > 0 ? $assessment['responses']['employmentscore']:'N/A'); ?></td>
                                        </tr>
                                        <tr>
                                            <th>Employment status</th>
                                            <td><?php echo ($assessment['responses']['employment_status'] == 'Other') ? $assessment['responses']['employment_status_unable_to_work'] : $assessment['responses']['employment_status']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Employment details</th>
                                            <td><?php echo ($assessment['responses']['employment_details'] == 'error' || empty($assessment['responses']['employment_details'])) ? "N/A" : $assessment['responses']['employment_details']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Last day of employment</th>
                                            <td><?php echo ($assessment['responses']['employment_last_working_day'] == 'error' || empty($assessment['responses']['employment_last_working_day'])) ? "N/A" : $assessment['responses']['employment_last_working_day']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Manual Worker?</th>
                                            <td><?php echo (($assessment['responses']['employment_status'] != 'Employed' && $assessment['responses']['employment_status'] != 'Self Employed') ? "N/A" : $assessment['responses']['manual_worker']) ?></td>
                                        </tr>
                                        <tr>
                                            <th>On benefits?</th>
                                            <td><?php echo $assessment['responses']['on_benefits']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Type of benefits</th>
                                            <td><?php echo ($assessment['responses']['benefits_type'] == 'Other') ? $assessment['responses']['benefits_type_other'] : ((empty($assessment['responses']['benefits_type']) || $assessment['responses']['benefits_type'] == "error") ? "N/A" : $assessment['responses']['benefits_type']) ?></td>
                                        </tr>
                                        <tr>
                                            <th>I would be interested in volunteering opportunities?</th>
                                            <td><?php echo $assessment['responses']['interested_in_volunteering'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>I would be interested in adult learning courses?</th>
                                            <td><?php echo $assessment['responses']['interested_in_adult_learning_courses'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>I would be interested in learning new skills to improve my employability?</th>
                                            <td><?php echo $assessment['responses']['interested_in_learning_new_skills'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>I would benefit from employment advice?</th>
                                            <td><?php echo $assessment['responses']['benefit_from_employment_advice'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Customer notes</th>
                                            <td><?php echo htmlentities($personal_notes['employment']); // todo ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li><!-- smoking -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-<?php echo ($assessment['results']['Smoking_priority'] > 0 ? $assessment['results']['Smoking_priority'] : 'grey'); ?> text-center"><?php echo ($assessment['results']['Smoking_priority'] > 0 ? 'Priority '. $assessment['results']['Smoking_priority'] : 'N/A'); ?></div>
                                <span>Smoking </span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr>
                                            <th>Do you smoke?</th>
                                            <td><?php echo $assessment['responses']['smoke']; ?></td>
                                        </tr>
                                        <?php if($assessment['responses']['smoke'] == 'Yes') { ?>)
                                            <tr>
                                                <th>Score</th>
                                                <td><?php echo ($assessment['responses']['smokingscore'] > 0 ? $assessment['responses']['smokingscore']:'N/A'); ?></td>
                                            </tr>
                                            <tr>
                                                <th>I want to stop smoking?</th>
                                                <td><?php echo $assessment['responses']['stop_smoking']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>I would benefit from advice / support on quitting?</th>
                                                <td><?php echo $assessment['responses']['advice_on_quitting']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Customer notes</th>
                                                <td><?php echo htmlentities($personal_notes['smoking']); // todo ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li><!-- social -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-<?php echo ($assessment['results']['Social_priority'] > 0 ? $assessment['results']['Social_priority'] : 'grey'); ?> text-center"><?php echo ($assessment['results']['Social_priority'] > 0 ? 'Priority '. $assessment['results']['Social_priority'] : 'N/A'); ?></div>
                                <span>Social </span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr>
                                            <th>Score</th>
                                            <td><?php echo ($assessment['responses']['socialscore'] > 0 ? $assessment['responses']['socialscore'] : 'N/A'); ?></td>
                                        </tr>
                                        <tr>
                                            <th>I often feel lonely?</th>
                                            <td><?php echo $assessment['responses']['often_feel_lonely'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>I have few opportunities to socialise with other people?</th>
                                            <td><?php echo $assessment['responses']['opportunities_to_socialise'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>I am unable to get out and about/ I do not feel close to the people in my local area?</th>
                                            <td><?php echo $assessment['responses']['unable_to_get_out_and_about'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>I struggle to mix well with other people?</th>
                                            <td><?php echo $assessment['responses']['struggle_to_mix_well'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Customer notes</th>
                                            <td><?php echo htmlentities($personal_notes['social']); // todo ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li><!-- emotional -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-<?php echo ($assessment['results']['Emotional_priority'] > 0 ? $assessment['results']['Emotional_priority'] : 'grey'); ?> text-center"><?php echo ($assessment['results']['Emotional_priority'] > 0 ? 'Priority '.$assessment['results']['Emotional_priority'] : 'N/A'); ?></div>
                                <span>Emotional </span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr>
                                            <th>Score</th>
                                            <td><?php echo ($assessment['responses']['emotionalscore'] > 0 ? $assessment['responses']['emotionalscore']:'N/A'); ?></td>
                                        </tr>
                                        <tr>
                                            <th>Are you currently receiving any specialist mental health services?</th>
                                            <td><?php echo $assessment['responses']['receiving_specialist_mh_services']; ?></td>
                                        </tr>

                                        <?php
                                        if( $assessment['responses']['feeling_useful'] != '' ) {
                                            ?>
                                            <tr>
                                                <th>I've been feeling optimistic about the future?</th>
                                                <td><?php echo $assessment['responses']['feeling_optimistic'] ?></td>
                                            </tr>
                                            <tr>
                                                <th>I've been feeling useful?</th>
                                                <td><?php echo $assessment['responses']['feeling_useful'] ?></td>
                                            </tr>
                                            <tr>
                                                <th>I've been feeling relaxed</th>
                                                <td><?php echo $assessment['responses']['feeling_relaxed']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>I've been dealing with problems well?</th>
                                                <td><?php echo $assessment['responses']['dealing_with_problems_well'] ?></td>
                                            </tr>
                                            <tr>
                                                <th>I've been thinking clearly?</th>
                                                <td><?php echo $assessment['responses']['thinking_clearly'] ?></td>
                                            </tr>
                                            <tr>
                                                <th>I've been feeling close to other people?</th>
                                                <td><?php echo $assessment['responses']['feeling_close_to_other_people'] ?></td>
                                            </tr>
                                            <tr>
                                                <th>I've been able to make up my own mind about things?</th>
                                                <td><?php echo $assessment['responses']['able_to_make_up_my_own_mind_about_things'] ?></td>
                                            </tr>

                                            <?php
                                        } else { // todo show older responses
                                            ?>

                                            <!--<tr>
													<th>I often feel down or worried?</th>
													<td><?php echo $assessment['responses']['often_feel_down_or_worried']; ?></td>
												</tr>-->
                                            <tr>
                                                <th>I lack confidence to make my own decisions?</th>
                                                <td><?php echo $assessment['responses']['lack_confidence']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>I find it difficult to cope with my own problems?</th>
                                                <td><?php echo $assessment['responses']['find_it_difficult_to_cope_with_my_own_problems']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>I feel gloomy about the future?</th>
                                                <td><?php echo $assessment['responses']['feel_gloomy_about_the_future']; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>

                                        <?php if($assessment['responses']['receiving_specialist_mh_services'] == 'Yes') { ?>
                                            <tr>
                                                <th>I am receiving support from?</th>
                                                <?php if($assessment['responses']['receiving_emotional_support'] == 'other') { ?>
                                                    <td><?php echo $assessment['responses']['receiving_emotional_support_other']; ?></td>
                                                <?php } else { ?>
                                                    <td><?php echo $assessment['responses']['receiving_emotional_support']; ?></td>
                                                <?php } ?>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <th>Customer notes</th>
                                            <td><?php echo htmlentities($personal_notes['emotional']); // todo ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li><!-- housing -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-<?php echo ($assessment['results']['Housing_priority'] > 0 ? $assessment['results']['Housing_priority'] : 'grey'); ?> text-center"><?php echo ($assessment['results']['Housing_priority'] > 0 ? 'Priority '. $assessment['results']['Housing_priority'] : 'N/A'); ?></div>
                                <span>Housing </span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr>
                                            <th>Score</th>
                                            <td><?php echo ($assessment['responses']['housingscore'] > 0 ? $assessment['responses']['housingscore']:'N/A'); ?></td>
                                        </tr>
                                        <tr>
                                            <th>Housing Support Question</th>
                                            <td><?php echo $assessment['responses']['housing_needs']; ?></td>
                                        </tr>

                                        <tr>
                                            <th>Housing Needs</th><?php // todo ?>
                                            <td><?php if($assessment['responses']['home_improvements'] == 1) {
                                                    echo "Home improvements  ";
                                                }
                                                if($assessment['responses']['housing_guidance'] == 'Yes') {
                                                    echo "Housing guidance  ";
                                                }
                                                if($assessment['responses']['personal_care'] == 'Yes') {
                                                    echo "Personal care  ";
                                                } ?></td>
                                        </tr>
                                        <tr>
                                            <th>I consider myself to be at risk of falling?</th>
                                            <td><?php echo (empty($assessment['responses']['consider_myself_to_be_at_risk_of_falling'])) ? "N/A" : $assessment['responses']['consider_myself_to_be_at_risk_of_falling']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>I do not feel independent in my home?</th>
                                            <td><?php echo (empty($assessment['responses']['do_not_feel_independent_in_my_home'])) ? "N/A" : $assessment['responses']['do_not_feel_independent_in_my_home']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>I would benefit from technologies to help me stay independent at home?</th>
                                            <td><?php echo (empty($assessment['responses']['benefit_from_technologies_to_help_me_stay_independent_at_home'])) ? "N/A" : $assessment['responses']['benefit_from_technologies_to_help_me_stay_independent_at_home']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Customer notes</th>
                                            <td><?php echo htmlentities($personal_notes['housing']); ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li><!-- financial -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-<?php echo ($assessment['results']['finance_priority'] > 0 ? $assessment['results']['finance_priority'] : 'grey'); ?> text-center"><?php echo ($assessment['results']['finance_priority'] > 0 ? 'Priority '. $assessment['results']['finance_priority'] : 'N/A'); ?></div>
                                <span>Financial </span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr>
                                            <th>Are you worried about money?</th>
                                            <td><?php echo $assessment['responses']['are_you_in_debt']; ?></td>
                                        </tr>
                                        <?php if($assessment->are_you_in_debt == 'Yes') { ?>
                                            <tr>
                                                <th>Score</th>
                                                <td><?php echo ($assessment['responses']['financescore'] > 0 ? $assessment['responses']['financescore'] : 'N/A'); ?></td>
                                            </tr>
                                            <tr>
                                                <th>I am concerned about managing my debts?</th>
                                                <td><?php echo $assessment['responses']['concerned_about_managing_my_debts']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>I worry about losing my home?</th>
                                                <td><?php echo $assessment['responses']['worry_about_losing_my_home']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>I struggle with paying bills?</th>
                                                <td><?php echo $assessment['responses']['struggle_with_paying_bills']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>I rarely have the money to do the things I really enjoy?</th>
                                                <td><?php echo $assessment['responses']['often_worry_about_money']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Customer notes</th>
                                                <td><?php echo htmlentities($personal_notes['financial']); // todo ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li><!-- weight -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-<?php echo ($assessment['result']['Weight_priority'] > 0 ? v : 'grey'); ?> text-center"><?php echo ($assessment['result']['Weight_priority'] > 0 ? 'Priority '. $assessment['result']['Weight_priority'] : 'N/A'); ?></div>
                                <span>Weight management </span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <?php if($assessment['responses']['concerns_regarding_weight'] == 'Yes') { ?>
                                            <tr>
                                                <th>Score</th>
                                                <td><?php echo ($assessment['responses']['weightscore'] > 0 ? $assessment['responses']['weightscore'] : 'N/A'); ?></td>
                                            </tr>
                                            <tr>
                                                <th>I am concerned about my weight?</th>
                                                <td><?php echo $assessment['responses']['concerns_regarding_weight']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>I rarely exercise 30 mins + a day?</th>
                                                <td><?php echo $assessment['responses']['rarely_exercise_30_mins_a_day']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>I consider myself to be overweight?</th>
                                                <td><?php echo $assessment['responses']['feel_overweight']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>I am motivated to maintain a healthy weight?</th>
                                                <td><?php echo $assessment['responses']['feel_motivated_to_maintain_a_healthy_weight']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>I struggle to eat a healthy diet?</th>
                                                <td><?php echo $assessment['responses']['struggle_to_eat_a_healthy_diet']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>I don't get enough sleep?</th><?php // todo ?>
                                                <td><?php echo (empty($assessment['responses']['dont_worry_about_sleep'])) ? "N/A" : sliderValueToText($assessment['responses']['dont_worry_about_sleep']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Height</th>
                                                <td><?php echo (empty($assessment['responses']['height_cm'])) ? "N/A" : $assessment['responses']['height_cm'].'cm'; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Weight</th>
                                                <td><?php echo (empty($assessment['responses']['weight_kgs'])) ? "N/A" : $assessment['responses']['weight_kgs'].'kg'; ?></td>
                                            </tr>
                                            <tr>
                                                <th>BMI</th>
                                                <td><?php echo (empty($assessment['responses']['bmi'])) ? "N/A" : $assessment['responses']['bmi']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Customer notes</th>
                                                <td><?php echo htmlentities($personal_notes['weight-management']); // todo ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li><!-- alcohol -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-<?php echo ($assessment['results']['Alcohol_priority'] > 0 ? $assessment['results']['Alcohol_priority'] : 'grey'); ?> text-center"><?php echo ($assessment['results']['Alcohol_priority'] > 0 ? 'Priority ' . $assessment['results']['Alcohol_priority'] : 'N/A'); ?></div>
                                <span>Alcohol </span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr>
                                            <th>Do you drink alcohol?</th>
                                            <td><?php echo $assessment['responses']['drink_alcohol']; ?></td>
                                        </tr>
                                        <?php if($assessment['responses']['drink_alcohol'] == 'Yes') { ?>
                                        <tr>
                                            <th>Score</th>
                                            <td><?php echo ($assessment['responses']['alcoholscore'] > 0 ? $assessment['responses']['alcoholscore'] : 'N/A'); ?></td>
                                        </tr>
                                        <?php
                                            if( $assessment['responses']['alcohol_units'] == '' ) { // answers from oct 4th 2016 onwards
                                                ?>
                                                <tr>
                                                    <th>How often do you have a drink containing alcohol?</th>
                                                    <td><?php echo $assessment['responses']['alcohol_how_often']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>How many units of alcohol do you drink on a typical day when you are drinking?</th>
                                                    <td><?php echo $assessment['responses']['alcohol_units_per_day']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>How often have you had 6 or more units if female, or 8 or more if male, on a single occasion in the last year?</th>
                                                    <td><?php echo $assessment['responses']['alcohol_six_units_how_often']; ?></td>
                                                </tr>

                                                <?php
                                                if( $assessment['responses']['alcoholreversedscore'] >= 5 ) {
                                                    ?>
                                                    <tr>
                                                        <th>How often during the last year have you found that you were not able to stop drinking once you had started?</th>
                                                        <td><?php echo $assessment['responses']['unable_stop_drinking_how_often']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>How often during the last year have you failed to do what was normally expected from you because of your drinking?</th>
                                                        <td><?php echo $assessment['responses']['failed_due_to_drinking_how_often']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>How often during the last year have you needed an alcoholic drink in the morning to get yourself going after a heavy drinking session?</th>
                                                        <td><?php echo $assessment['responses']['hair_of_the_dog_how_often']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>How often during the last year have you had a feeling of guilt or remorse after drinking?</th>
                                                        <td><?php echo $assessment['responses']['remorse_after_drinking_how_often']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>How often during the last year have you been unable to remember what happened the night before because you had been drinking?</th>
                                                        <td><?php echo $assessment['responses']['amnesia_due_to_drinking_how_often']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Have you or somebody else been injured as a result of your drinking?</th>
                                                        <td><?php echo $assessment['responses']['injured_due_to_drinking']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Has a relative or friend, doctor or other health worker been concerned about your drinking or suggested that you cut down?</th>
                                                        <td><?php echo $assessment['responses']['suggested_cut_down_drinking']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Customer notes</th>
                                                        <td><?php echo htmlentities($personal_notes['alcohol']); // todo ?></td>
                                                    </tr>
                                                <?php } ?>

                                                <?php
                                            } else { // todo otherwise just show the alcohol_units answer
                                                ?>
                                                <tr>
                                                    <th>How often have you had 6 or more units if female, or 8 or more if male, on a single occasion in the last year?</th>
                                                    <td><?php echo $assessment['responses']['alcohol_units']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Customer notes</th>
                                                    <td><?php echo htmlentities($personal_notes['alcohol']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li style="margin-bottom:50px;"><!-- cold homes -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-1 text-center">N/A</div>
                                <span>Cold homes</span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr>
                                            <th>I would like some advice on keeping my home warm</th>
                                            <td><?php echo $assessment['responses']['warm_home_advice'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>My ability to heat my home affects my family's health</th>
                                            <td><?php echo $assessment['responses']['cold_home_affects_health']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>My home feels cold and draughty</th>
                                            <td><?php echo $assessment['responses']['home_feels_cold']; ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li><!-- additional -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-grey text-center">&nbsp;</div>
                                <span>Additional info</span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr>
                                            <th>Is there anything else you wish to tell us at this time?</th>
                                            <td><?php echo nl2br($assessment['responses']['anything_else']); ?></td>
                                        </tr>
                                        <tr>
                                            <th>How did you hear about us?</th>
                                            <td><?php echo ($assessment['responses']['how_did_you_hear_about_us'] == 'Other' ) ? $assessment['responses']['how_did_you_hear_about_us_other'] : $assessme['responses']['how_did_you_hear_about_us']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Receive newsletter?</th>
                                            <td><?php echo $assessment['responses']['newsletter']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Receive information and offers from First for Wellbeing?</th>
                                            <td><?php echo $assessment['responses']['offers']; ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>



            <a href="/assessors" class="button button-green button-green-drk">Back</a>
        </section>

    </div>
</div>

<?php
View::element('jsuistuff', [], 'firstforwellbeing');

