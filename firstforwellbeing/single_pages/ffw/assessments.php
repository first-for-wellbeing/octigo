<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<div class="row row-page-title">
	<h1>Manage your Assessment</h1>
</div>

<div class="row" id="row-add-assessment" <?php echo (isset($editrecord) ? '' : 'style="display:none;"'); ?>>
	<form method="post" action="<?php echo $this->action('save'); ?>">
		<input type="hidden" name="editid" value="<?php echo (isset($editrecord['id']) ? $editrecord['id'] : ''); ?>" />
		<div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                        <td class="col-md-3">Assessment name</td>
                        <td class="col-md-9"><input type="text" style="width:100%" name="atitle" placeholder="Your assessment's title" value="<?php echo (isset($editrecord['name']) ? htmlentities($editrecord['name']) : ''); ?>" /></td>
                    </tr>
                    <tr>
                        <td class="col-md-3">Assessment description</td>
                        <td class="col-md-9"><textarea name="adesc" style="width:100%; height: 2em;" placeholder="Your assessment's description"><?php echo (isset($editrecord['description']) ? htmlentities($editrecord['description']) : ''); ?></textarea></td>
                    </tr>
                </table>
            </div>
		</div>


		<div class="row">
			<div class="col-md-6">
				<div class="row row-theme-list">
					<div class="col-md-11">
						<?php
							if($themes_list !== null) {
								echo'<table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            <p><b>Click a theme to add it to this assessment</b></p>
                                        </th>
                                    </tr>
                                </thead>';
								while($row = $themes_list->fetch()) {
									echo'<tr class="item-container" qid="' . $row['id'] . '"><td class="theme-item">' . htmlentities($row['name']) . '</td></tr>';
								}
								echo'</table>';
							} else {
								echo'No themes defined, unable to add themes to assessment.';
							}
						?>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
                    <table class="table table-striped" id="row-added-themes">
                        <thead>
                            <tr>
                                <th>
                                    <p><b>Added themes:</b></p>
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-11">
				<div class="button-admin-red">Cancel</div>
			</div>
			<div class="col-md-1">
				<button type="submit" class="button-admin-green">Save</button>
			</div>
		</div>
	</form>
</div>



<?php
	if(!isset($editrecord)) {
?>
		<?php
			if($totalrecs > 0) {
				// Calc prev/next links
				$prevlink = $this->action('') . '/' . ($pagenum > 1 ? $pagenum - 1 : 1) . '/' . $perpage;
				$nextlink = $this->action('') . '/' . ($pagenum * $perpage < $totalrecs ? $pagenum + 1 : $pagenum) . '/' . $perpage;
		?>
				<div class="row">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="col-md-2">Assessment</th>
                                <th class="col-md-3">Description</th>
                                <th class="col-md-1">Themes</th>
                                <th class="col-md-2">&nbsp;</th>
                                <th class="col-md-2">Edit</th>
                                <th class="col-md-2">Delete</th>
                            </tr>
                        </thead>
				</div>
					<?php
						while($row = $assessments_list->fetch()) {
						    echo'<tr>';
							echo'<td>' . htmlentities($row['name']) . '</td>';
							echo'<td>' . htmlentities($row['description']) . '</td>';
							echo'<td>' . $row['cnt'] . '</td>';
							echo'<td>&nbsp;</td>';
							echo'<td><a href="/ffw/assessments/' . $pagenum . '/' . $perpage . '/' . $row['id'] . '">Edit</a></td>';
							echo'<td><span class="delete-item" qid="' . $row['id'] . '">X</span></td>';
							echo'</tr>';
						}
					?>
                </table>
		<?php
			} else {
				echo'No assessments defined yet';
			}
	} // double outdent due to php tags
		?>


<script type="text/javascript">
ffw_editing = <?php echo (count($editrecord) > 1 ? 1 : 0); ?>;
<?php
// generate JS question list if needed
if(isset($editrecord['themes']) and count($editrecord['themes']) > 0) {
	echo 'ffw_themes = ';
	$q = [];
	foreach($editrecord['themes'] as $row) {
		$q[] = $row['theme_id'];
	}
	echo json_encode($q) . ";\n\n";
}
?>
$(document).ready( function() {
	// Add question
	$('#button-add-assessment').click( function(e) {
		$('#row-add-assessment').fadeIn(500);
	});

	// Add question
	$('.item-container').click( function(e) {
		if($(this).hasClass('disabled')) {
			return; // Unselectable item
		}
		var qid = $(this).attr('qid');
		var el = '<tr class="added-theme added-item" qid="' + qid + '">' + $(this).html() + '</td><td class="delete-added-item"><input type="hidden" name="qid[]" value="' + qid + '">X</td></tr>';
		$(this).addClass('disabled');
		$('#row-added-themes tbody').append(el);
	});

	// Delete added question
	$('#row-added-themes').on('click', '.delete-added-item', function(e) { // note use of on() to bind to future DOM objects with event-delegation
		var qid = $(this).parent().attr('qid');
		$('.item-container[qid="' + qid + '"]').removeClass('disabled');
		$(this).parent().remove();
	});

	// Delete item
	$('.delete-item').click( function(e) {
		var id = $(this).attr('qid');
		if(!confirm('Delete this item?')) {
			return;
		}
		window.location.href = 'assessments/delete/' + id;
	});

	// If edit then 'prefill' themes, needs to be at end of scripts
	if(ffw_editing) {
		$.each( ffw_themes, function(i, v) {
			$('.item-container[qid="' + v + '"]').click(); // fake a click to a question
			$('#h' + v).val( v );
		});
	}

    // make items sortable
    $('#row-added-themes tbody').sortable({
        axis : 'y',
        cursor: 'move',
        placeholder : 'sort-dest'
    });
});
</script>
