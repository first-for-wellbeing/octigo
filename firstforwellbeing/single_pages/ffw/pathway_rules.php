<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<div class="row row-page-title">
	<h1>Manage Pathway Rules</h1>
</div>
<div class="row row-buttons">
	<div class="col-md-12">
		<div class="button-admin button-blue" id="button-add-assessment">Managing rules for <?php echo ' "' . $rulefor . '"'; ?></div>
	</div>
</div>


<?php
	View::element('rules_form', [
		'athemeid' => $athemeid,
		'questions_list' => $questions_list,
		'ruletypes_list' => $ruletypes_list,
		'ruleset' => $ruleset,
		'rules' => $rules,
		'rulevaluetypes_list' => $rulevaluetypes_list
	], 'firstforwellbeing');
?>


