<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>
<div class="row row-page-title">
    <h1>Manage Pathways</h1>
</div>

<div class="row row-buttons">
    <div class="col-md-12">
        <?php
        if(isset($editrecord)) {
            echo'Editing Pathway "' . htmlentities($editrecord['title']) . '"';
            echo'&nbsp;&nbsp;<div class="button-admin button-blue"><a href="/ffw/question_rules/' . $editrecord['id'] . '" style="color:inherit;">Edit rules for this Pathway</a></div>';
        } else {
            echo'<div class="button-admin button-blue" id="button-add-question">Add Pathway</div>';
        }
        ?>
    </div>
</div>

<div class="row" id="row-add-question" <?php echo (isset($editrecord) ? '' : 'style="display:none;"'); ?>>
    <form method="post" action="<?php echo $this->action('save'); ?>">
        <input type="hidden" name="editid" value="<?php echo (isset($editrecord['id']) ? $editrecord['id'] : ''); ?>" />
        <div class="row">
            <div class="col-md-3">Pathway name</div>
            <div class="col-md-8 col-md-offset-1"><input type="text" style="width:100%" name="atitle" id="atitle" placeholder="Your Pathway's title" value="<?php echo (isset($editrecord['name']) ? htmlentities($editrecord['name']) : ''); ?>" /></div>
        </div>
        <div class="row">
            <div class="col-md-3">Pathway description</div>
            <div class="col-md-8 col-md-offset-1"><textarea name="adesc" id="adesc" style="width:100%; height: 2em;" placeholder="Your Pathway's description"><?php echo (isset($editrecord['description']) ? htmlentities($editrecord['description']) : ''); ?></textarea></div>
        </div>
        <div class="row" id="row-qprovider">
            <div class="col-md-3">Pathway Provider type</div>
            <div class="col-md-8 col-md-offset-1">
                <select name="qprovider" id="provider">
                    <option class="select-placeholder" value="">Select Provider type</option>
                    <?php
                    foreach($providers as $provider) {
                        echo '<option value="' . $provider . '" ' . ( (count($editrecord) > 0 and $provider == $editrecord['provider_type']) ? 'selected="selected"' : '' ) . '>' . htmlentities($provider) . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row" id="row-qoutcome">
            <div class="col-md-3">Outcomes</div>
            <div class="col-md-8 col-md-offset-1">
                <select name="qoutcome" id="outcome">
                    <option class="select-placeholder" value="">Select outcome</option>
                    <?php
                    foreach($outcomes as $outcome) {
                        echo '<option value="' . $outcome['id'] . '" ' . ( (count($editrecord) > 0 and $outcome['id'] == $editrecord['outcome_id']) ? 'selected="selected"' : '' ) . '>' . htmlentities($outcome['name']) . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11">
                <div class="button-admin-red"><a href="/ffw/pathways" style="color:inherit;">Cancel</a></div>
            </div>
            <div class="col-md-1">
                <button type="submit" class="button-admin-green">Save</button>
            </div>
        </div>
    </form>
</div>

<?php
if(!isset($editrecord)) {
    // Calc prev/next links
    $prevlink = $this->action('') . '/' . ($pagenum > 1 ? $pagenum - 1 : 1) . '/' . $perpage;
    $nextlink = $this->action('') . '/' . ($pagenum * $perpage < $totalrecs ? $pagenum + 1 : $pagenum) . '/' . $perpage;
    ?>
    <div class="row hide-on-add">
        <div class="col-md-12">
            <table class="table table-striped">
            <tr>
                <th class="col-md-2">Title</th>
                <th class="col-md-3">Text</th>
                <th class="col-md-2">Provider type</th>
                <th class="col-md-2">Edit</th>
                <th class="col-md-2">Delete</th>
            </tr>
            <?php
            while($row = $pathways_list->fetch()) {
                echo'<tr>';
                echo'<td class="col-md-2">' . htmlentities($row['name']) . '</td>';
                echo'<td class="col-md-3">' . htmlentities($row['description']) . '</td>';
                echo'<td class="col-md-2">' . htmlentities($row['provider_type']) . '</td>';
                echo'<td class="col-md-2"><a href="/ffw/pathways/1/20/' . $row['id'] . '">Edit</a>&nbsp;&nbsp;<a href="/ffw/pathway_rules/' . $row['id'] . '">Rules</a>' . ' (' . $row['cnt'] . ')</td>';
                echo'<td class="col-md-2"><span class="delete-item" qid="' . $row['id'] . '">X</span></td>';
                echo'</tr>';
            }
            ?>
            </table>
        </div>
    </div>
    <div class="row row-pagination row-pagination-footer">
        <div class="col-md-1"><a href="<?php echo $prevlink;?>" class="<?php echo $prevclass; ?>">Prev</a></div>
        <div class="col-md-8 col-md-offset-1">Showing questions <?php echo ($pagenum - 1) * $perpage + 1; ?> - <?php echo ( ($pagenum) * $perpage < $totalrecs ? ($pagenum) * $perpage : $totalrecs ); ?> of <?php echo $totalrecs; ?></div>
        <div class="col-md-1 col-md-offset-1"><a href="<?php echo $nextlink;?>" class="<?php echo $nextclass; ?>">Next</a></div>
    </div>
    <?php
}
?>

<script type="text/javascript">
    $(document).ready( function() {
        // Copy title to text, placeholder if text is empty on focus
        $('#atitle').change( function(e) {
            if($('#adesc').val() == '') {
                $('#adesc').val ($('#atitle').val() );
            }
        });

        // Add pathway
        $('#button-add-question').click(function (e) {
            $('#row-add-question').fadeIn(500);
            $('.hide-on-add').fadeOut(500);
        });

        // Delete item
        $('.delete-item').click( function(e) {
            var id = $(this).attr('qid');
            if(!confirm('Delete this item?')) {
                return;
            }
            window.location.href = '/ffw/pathways/delete/' + id;
        });
    });

</script>