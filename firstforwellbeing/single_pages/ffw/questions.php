<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>
<div class="row row-page-title">
	<h1>Manage Questions</h1>
</div>

<div id="icon-picker" class="icon-picker">
	<div class="icon-picker-header">
		Pick your icon or click cancel
		<div id="icon-picker-cancel">
			CANCEL
		</div>
	</div>
	<div class="icon-picker-icons">
		<?php
			foreach($icons as $icon) {
				echo'<div class="icon-picker-image" style="background-image:url(' . $icon->getURL() . ');" data-url="' . $icon->getURL() . '" data-id="' . $icon->getFileID() . '">';
				echo'</div>';
			}
		?>
	</div>
</div>


<div class="row row-buttons">
	<div class="col-md-12">
			<?php
			if(isset($editrecord)) {
				echo'Editing question "' . htmlentities($editrecord['title']) . '"';
				echo'&nbsp;&nbsp;<div class="button-admin button-blue"><a href="/ffw/question_rules/' . $editrecord['id'] . '" style="color:inherit;">Edit rules for this question</a></div>';
			} else {
				echo'<div class="button-admin button-blue" id="button-add-question">Add question</div>';
			}
			?>
	</div>
</div>

<div class="row" id="row-add-question" <?php echo (isset($editrecord) ? '' : 'style="display:none;"'); ?>>
	<form method="post" action="<?php echo $this->action('save'); ?>">
		<input type="hidden" name="editid" value="<?php echo (isset($editrecord['id']) ? $editrecord['id'] : ''); ?>" />
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped">
                        <tr>
                            <td class="col-md-3">Question title</td>
                            <td class="col-md-9"><input type="text" style="width:100%" name="qtitle" id="qtitle" placeholder="Your question's title" value="<?php echo (isset($editrecord['title']) ? htmlentities($editrecord['title']) : ''); ?>" /></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">Question text</td>
                            <td class="col-md-9"><textarea style="width:100%" name="qtext" id="qtext" placeholder="Your question's text"><?php echo (isset($editrecord['question_text']) ? htmlentities($editrecord['question_text']) : ''); ?></textarea></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">Show label?</td>
                            <td class="col-md-9"><input type="radio" id="showlabely" name="qshowlabel" value="1" <?php echo ((count($editrecord) > 0 and $editrecord['show_label'] == 1) ? 'checked="checked"' : '' ); ?> /> <label for="showlabely">Yes</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="showlabeln" name="qshowlabel" value="0" <?php echo ((count($editrecord) == 0 or $editrecord['show_label'] == 0) ? 'checked="checked"' : ''); ?>" /> <label for="showlabeln">No</label></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">Label position</td>
                            <td class="col-md-9"><input type="radio" id="labelposa" name="qlabelpos" value="1" <?php echo ((count($editrecord) > 0 and $editrecord['label_pos'] == 1) ? 'checked="checked"' : '' ); ?> /> <label for="labelposa">Left</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="labelposb" name="qlabelpos" value="0" <?php echo ((count($editrecord) == 0 or $editrecord['label_pos'] == 0) ? 'checked="checked"' : ''); ?>" /> <label for="labelposb">Above</label></td>
                        </tr>                        <tr>
                            <td class="col-md-3">Question placeholder</td>
                            <td class="col-md-9"><input type="text" style="width:100%" name="qplaceholder" id="qplaceholder" placeholder="Your question's placeholder text" value="<?php echo (isset($editrecord['placeholder']) ? htmlentities($editrecord['placeholder']) : ''); ?>" /></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">Question data handle</td>
                            <td class="col-md-9"><input type="text" style="width:100%" name="qhandle" id="qhandle" placeholder="Your question's data handle" value="<?php echo (isset($editrecord['data_handle']) ? htmlentities($editrecord['data_handle']) : ''); ?>" /></td>
                        </tr>

                        <tr>
                            <td class="col-md-3">Invisible?</td>
                            <td class="col-md-9"><input type="radio" id="invisibley" name="qinvisible" value="1" <?php echo ((count($editrecord) > 0 and $editrecord['invisible'] == 1) ? 'checked="checked"' : '' ); ?> /> <label for="invisibley">Yes</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="invisiblen" name="qinvisible" value="0" <?php echo ((count($editrecord) == 0 or $editrecord['invisible'] == 0) ? 'checked="checked"' : ''); ?>" /> <label for="invisiblen">No</label></td>
                        </tr>
                        <tr style="display:none;">
                            <td class="col-md-3">Read-only?</td>
                            <td class="col-md-9"><input type="radio" id="readonlyy" name="qreadonly" value="1" <?php echo ((count($editrecord) > 0 and $editrecord['read_only'] == 1) ? 'checked="checked"' : '' ); ?> /> <label for="readonlyy">Yes</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="readonlyn" name="qreadonly" value="0" <?php echo ((count($editrecord) == 0 or $editrecord['read_only'] == 0) ? 'checked="checked"' : ''); ?>" /> <label for="readonlyn">No</label></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">CSS class(es)</td>
                            <td class="col-md-9"><input type="text" style="width:100%" name="qcss" id="qcss" placeholder="CSS classes, space-separated" value="<?php echo (isset($editrecord['css']) ? htmlentities($editrecord['css']) : ''); ?>" /></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">Validation error message</td>
                            <td class="col-md-9"><input type="text" style="width:100%" name="qvalmsg" id="qvalmsg" placeholder="CSS classes, space-separated" value="<?php echo (isset($editrecord['validation_message']) ? htmlentities($editrecord['validation_message']) : 'Please enter a value'); ?>" /></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">Question style</td>
                            <td class="col-md-9">
                                <select name="qstyle" id="question-style">
                                    <option class="select-placeholder">Please select a question style</option>
                                    <?php
                                    while($type = $questions_types_list->fetch()) {
                                        echo '<option value="' . $type['id'] . '">' . htmlentities($type['type_name']) . '</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3">Mandatory?</td>
                            <td class="col-md-9"><input type="radio" id="mandy" name="qmandatory" value="1" <?php echo ((count($editrecord) == 0 or $editrecord['mandatory'] == 1) ? 'checked="checked"' : '' ); ?> /> <label for="mandy">Yes</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="mandn" name="qmandatory" value="0" <?php echo ((count($editrecord) > 0 and $editrecord['mandatory'] == 0) ? 'checked="checked"' : ''); ?>" /> <label for="mandn">No</label></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">Mandatory (Assessors)?</td>
                            <td class="col-md-9"><input type="radio" id="amandy" name="qmandatorya" value="1" <?php echo ((count($editrecord) == 0 or $editrecord['assessor_mandatory'] == 1) ? 'checked="checked"' : '' ); ?> /> <label for="amandy">Yes</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="amandn" name="qmandatorya" value="0" <?php echo ((count($editrecord) > 0 and $editrecord['assessor_mandatory'] == 0) ? 'checked="checked"' : ''); ?> /> <label for="amandn">No</label></td>
                        </tr>
                        <tr id="row-qdefault" <?php echo ($editrecord['question_type'] == 12 ? 'style="display:none;"' : '') ; ?>>
                            <td class="col-md-3">Default value</td>
                            <td class="col-md-9"><input type="text" style="width:100%" name="qdefault" id="qdefault" placeholder="Your question's default value, if any" value="<?php echo (isset($editrecord['default_value']) ? htmlentities($editrecord['default_value']) : ''); ?>" /></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">Special validation</td>
                            <td class="col-md-9">
                                <select name="qspecial" id="question-special">
                                    <option class="select-placeholder" value="">Special validation (optional)</option>
                                    <?php
                                    while($type = $special_validation->fetch()) {
                                        echo '<option value="' . $type['id'] . '" ' . ( (count($editrecord) > 0 and $type['id'] == $editrecord['special_validation']) ? 'selected="selected"' : '' ) . '>' . htmlentities($type['name']) . '</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3">Profile field mapping</td>
                            <td class="col-md-9">
                                <select name="qprofile" id="question-profile">
                                    <option class="select-placeholder" value="">Map this field to a profile field (optional)</option>
                                    <option value="forename" <?php echo($editrecord['profile_mapping'] == 'forename' ? 'selected="selected"' : '');?>>First name</option>
                                    <option value="surname" <?php echo($editrecord['profile_mapping'] == 'surname' ? 'selected="selected"' : '');?>>Last name</option>
                                    <option value="email" <?php echo($editrecord['profile_mapping'] == 'email' ? 'selected="selected"' : '');?>>Email</option>
                                    <option value="address1" <?php echo($editrecord['profile_mapping'] == 'address1' ? 'selected="selected"' : '');?>>Address 1</option>
                                    <option value="address2" <?php echo($editrecord['profile_mapping'] == 'address2' ? 'selected="selected"' : '');?>>Address 2</option>
                                    <option value="town" <?php echo($editrecord['profile_mapping'] == 'town' ? 'selected="selected"' : '');?>>Town</option>
                                    <option value="county" <?php echo($editrecord['profile_mapping'] == 'county' ? 'selected="selected"' : '');?>>County</option>
                                    <option value="postcode" <?php echo($editrecord['profile_mapping'] == 'postcode' ? 'selected="selected"' : '');?>>Postcode</option>
                                    <option value="tel1" <?php echo($editrecord['profile_mapping'] == 'tel1' ? 'selected="selected"' : '');?>>Tel 1</option>
                                    <option value="tel2" <?php echo($editrecord['profile_mapping'] == 'tel2' ? 'selected="selected"' : '');?>>Tel 2</option>
                                    <option value="nhsnumber" <?php echo($editrecord['profile_mapping'] == 'nhsnumber' ? 'selected="selected"' : '');?>>NHS no</option>
                                    <option value="dob" <?php echo($editrecord['profile_mapping'] == 'dob' ? 'selected="selected"' : '');?>>DOB</option>
                                    <option value="healthcheck" <?php echo($editrecord['profile_mapping'] == 'healthcheck' ? 'selected="selected"' : '');?>>Healthcheck in last 4 years</option>
                                    <option value="gender" <?php echo($editrecord['profile_mapping'] == 'gender' ? 'selected="selected"' : '');?>>Gender</option>
                                    <option value="pregnant" <?php echo($editrecord['profile_mapping'] == 'pregnant' ? 'selected="selected"' : '');?>>Pregnant</option>
                                    <option value="religion" <?php echo($editrecord['profile_mapping'] == 'religion' ? 'selected="selected"' : '');?>>Religion</option>
                                    <option value="maritalstatus" <?php echo($editrecord['profile_mapping'] == 'maritalstatus' ? 'selected="selected"' : '');?>>Marital status</option>
                                    <option value="parentalstatus" <?php echo($editrecord['profile_mapping'] == 'parentalstatus' ? 'selected="selected"' : '');?>>Parental status</option>
                                    <option value="dependants" <?php echo($editrecord['profile_mapping'] == 'dependants' ? 'selected="selected"' : '');?>>Dependants</option>
                                    <option value="disabilitystatus" <?php echo($editrecord['profile_mapping'] == 'disabilitystatus' ? 'selected="selected"' : '');?>>Disability status</option>
                                    <option value="ethnicorigin" <?php echo($editrecord['profile_mapping'] == 'ethnicorigin' ? 'selected="selected"' : '');?>>Ethnic origin</option>
                                    <option value="sexuality" <?php echo($editrecord['profile_mapping'] == 'sexuality' ? 'selected="selected"' : '');?>>Sexuality</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
		<div class="row">
			<div class="col-md-11">
				<div class="button-admin-red"><a href="/ffw/questions" style="color:inherit;">Cancel</a></div>
			</div>
			<div class="col-md-1">
				<button type="submit" class="button-admin-green">Save</button>
			</div>
		</div>



		<div class="row" id="question-choices" style="display:none;">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th colspan="6">
                                <h2>Question choices (required for this style)</h2>
                                <p>Do not use text values for choices unless you are certain you do not need to generate a score from the answer. Instead use a numeric score or a unique number</p>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="row-add-choice">
                        <tr class="row-question-choice">
                            <td class="col-md-1 question-no">1</td>
                            <td class="col-md-3 question-choice"><input type="text" style="width:100%" name="qchoice[]" placeholder="Enter a choice" /></td>
                            <td class="col-md-3 question-value"><input type="text" style="width:100%" name="qvalue[]" placeholder="Enter a value for this choice" /></td>
                            <td class="col-md-1 question-default"><div class="set-default">Default</div></td>
                            <td class="col-md-2 question-icon">
                                <div class="picker-button">
                                    <?php if(!isset($editrecord) or $editrecord['choices'][0]['icon'] < 1 ) { echo'<span>Icon</span>'; } ?>
                                    <input type="hidden" name="qicon[]" value="" />
                                </div>
                            </td>
                            <td class="col-md-1 question-delete"><div class="delete-choice">X</div></td>
                        </tr>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-12">
                        <div class="button-admin button-blue" id="add-choice">Add another choice</div>
                    </div>
                </div>
            </div>
		</div>

        <div class="row" id="calculated-field" style="display:none;">
            <h2>Enter required calcuation</h2>
            <select name="questions" id="questions" style="max-width:15em;">
                <?php
                    while($row = $questions->fetch()) {
                        echo'<option value="' . $row['id'] . '">' . htmlentities($row['title']) . '</option>';
                    }
                ?>
            </select>
            <div style="display:inline-block; width:32px; height:32px; margin:2px; background-color:#ccc; font-weight:bold; text-align:center;" data-val="+" id="calc-add">+</div>
            <div style="display:inline-block; width:32px; height:32px; margin:2px; background-color:#ccc; font-weight:bold; text-align:center;" data-val="-" id="calc-sub">-</div>
            <div style="display:inline-block; width:32px; height:32px; margin:2px; background-color:#ccc; font-weight:bold; text-align:center;" data-val="*" id="calc-mul">x</div>
            <div style="display:inline-block; width:32px; height:32px; margin:2px; background-color:#ccc; font-weight:bold; text-align:center;" data-val="/" id="calc-div">/</div>
            <div style="display:inline-block; width:32px; height:32px; margin:2px; background-color:#ccc; font-weight:bold; text-align:center;" id="calc-c">C</div>
        </div>
	</form>
</div>

<?php
	if(!isset($editrecord)) {
		// Calc prev/next links
		$prevlink = $this->action('') . '/' . ($pagenum > 1 ? $pagenum - 1 : 1) . '/' . $perpage;
		$nextlink = $this->action('') . '/' . ($pagenum * $perpage < $totalrecs ? $pagenum + 1 : $pagenum) . '/' . $perpage;
		?>
        <table class="table table-striped hide-on-add">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Text</th>
                    <th>Style</th>
                    <th>Choices</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php
                while($row = $questions_list->fetch()) {
                    echo'<tr>';
                    echo'<td>' . htmlentities($row['title']) . '</td>';
                    echo'<td>' . htmlentities($row['question_text']) . '</td>';
                    echo'<td>' . htmlentities($row['type_name']) . '</td>';
                    echo'<td>' . ( in_array($row['question_type'], [6, 7, 8]) ? $row['cnt'] : 'N/A') . '</td>';
                    echo'<td><a href="/ffw/questions/' . $pagenum . '/' . $perpage . '/' . $row['id'] . '">Edit</a></td>';
                    echo'<td><span class="delete-item" qid="' . $row['id'] . '">X</span></td>';
                    echo'</tr>';
                }
                ?>
            </tbody>
        </table>
		<div class="row row-pagination row-pagination-footer hide-on-add">
			<div class="col-md-1"><a href="<?php echo $prevlink;?>" class="<?php echo $prevclass; ?>">Prev</a></div>
			<div class="col-md-8 col-md-offset-1">Showing questions <?php echo ($pagenum - 1) * $perpage + 1; ?> - <?php echo ( ($pagenum) * $perpage < $totalrecs ? ($pagenum) * $perpage : $totalrecs ); ?> of <?php echo $totalrecs; ?></div>
			<div class="col-md-1 col-md-offset-1"><a href="<?php echo $nextlink;?>" class="<?php echo $nextclass; ?>">Next</a></div>
		</div>
<?php
	}
?>


<script type="text/javascript">
ffw_editing = <?php echo (count($editrecord) > 0 ? 1 : 0); ?>;
<?php
// generate JS question list if needed
if(isset($editrecord['choices']) and count($editrecord['choices']) > 0) {
	echo 'ffw_choices = ';
	$q = [];
	foreach($editrecord['choices'] as $row) {
		$a = [ $row['choice'], $row['value'], $row['icon'] ];
		$q[ $row['id'] ] = $a;
	}
	echo json_encode($q) . ";\n\n";
} else {
    echo "ffw_choices = new Array();\n";
}
echo "choice_count = " . count($editrecord['choices']) . ";\n";
?>

// global for icons
icon_clicker = null;

$(document).ready( function() {
	// Copy title to text, placeholder if text is empty on focus
    $('#qtitle').change( function(e) {
        if($('#qtext').val() == '') {
            $('#qtext').val ($('#qtitle').val() );
        }
        if($('#qplaceholder').val() == '') {
            $('#qplaceholder').val ($('#qtitle').val() );
        }
    });
	// Add question
	$('#button-add-question').click( function(e) {
		$('#row-add-question').fadeIn(500);
		$('.hide-on-add').fadeOut(500);
	});
	// require field choices for some input types
	$('#question-style').change( function(e) {
		$(this).find('.select-placeholder').remove(); // once changed, remove the placeholder element
		switch($(this).val()) {
			case '1': // date
			case '2': // text short
			case '3': // text long
			case '4': // number int
			case '5': // number decimal
			case '9': // Yes/No Icons
			case '11': // Email
			case '14': // Plain text
                $('#calculated-field').fadeOut(500);
				$('#question-choices').fadeOut(500);
				$('#row-qdefault').fadeIn(500);
				$('.question-icon').hide();
				break;
			case '12': // Password
                $('#calculated-field').fadeOut(500);
				$('#question-choices').fadeOut(500);
				$('#row-qdefault').fadeOut(500);
				$('.question-icon').hide();
				break;
			case '6': // Dropdown
			case '7': // Tickbox
			case '8': // Radio
			case '13': // Slider
				$('#question-choices').fadeIn(500);
				$('#row-qdefault').fadeIn(500);
				$('.question-icon').hide();
				break;
            case '10': // Radio icons
                $('#question-choices').fadeIn(500);
                $('#row-qdefault').fadeIn(500);
                $('.question-icon').show();
                break;
            case '15': // Radio icons
                $('#calculated-field').fadeIn(500);
                break;
			default:
				break;
		}
	});

	// Set default
	$('.set-default').click( function(e) {
		$('#qdefault').val( $(this).parent().parent().find('.question-value input').val() );
	});

	// Add another choice
	$('#add-choice').click( function(e) {
		var clone = $('.row-question-choice').first().clone(true, true);
		if(ffw_editing) {
			$(clone).find('.row-question-choice').show(0);
		}
		$(clone).find('input').val('');
		$(clone).find('.question-no').text( $('.row-question-choice').length + 1);
		$('.row-add-choice').append(clone);
	});

	// Delete choice
	$('.delete-choice').on('click', function(e) {
		if( $('.row-question-choice').length == 1) {
			alert('This style of question requires at least one choice');
			return;
		}
		$(this).parents('.row-question-choice').remove(); // delete row
		$('.row-question-choice').each( function(i) {
			// renumber rows
			$(this).find('.question-no').text(i + 1);
		});
	});

	// Delete item
	$('.delete-item').click( function(e) {
		var id = $(this).attr('qid');
		if(!confirm('Delete this item?')) {
			return;
		}
		window.location.href = '/ffw/questions/delete/' + id;
	});

	// Icon button click
	$('.picker-button').on('click', function(e) {
		$('#icon-picker').fadeIn(500);
		icon_clicker = this;
	});

	// Icon picker cancel click
	$('#icon-picker-cancel').on('click', function(e) {
		$('#icon-picker').fadeOut(500);
	});

	// Icon image click
	$('.icon-picker-image').on('click', function(e) {
		$(icon_clicker).css('background', 'url(' + $(this).attr('data-url') + ') no-repeat 0 0 / 32px 64px').find('input').val( $(this).attr('data-id') );
		$(icon_clicker).find('span').remove();
		$('#icon-picker').fadeOut(500);
	});

	// If edit then 'prefill' questions, needs to be at end of scripts
	if(ffw_editing) {
		// preselect question type
		$('#question-style').val(<?php echo $editrecord['question_type']; ?>);
		// clone question sections into existence
		var el = $('.row-question-choice').clone(true, true);
		$(el).first().attr('style', '');
		if(choice_count > 0) {
		    $('.row-question-choice').remove();
        }
		var c = 1;
		$.each( ffw_choices, function(i, v) {
			$(el).find('.question-no').text( c++ );
			$(el).find('.question-choice input').val( v[0] );
			$(el).find('.question-value input').val( v[1] );
			$('.row-add-choice').append($(el).clone(true, true));
			if(!isNaN(parseInt(v[2])) && parseInt(v[2]) != 0) {
				icon_clicker = $('.row-add-choice').find('.picker-button').last();
				$('.icon-picker-image[data-id="' + v[2] + '"]').click();
			}
		});
	}

	// calculation buttons
    $('#calc-add, #calc-sub, #calc-mul, #calc-div').click( function(e) {
        if($('#qdefault').val() == '') {
            $('#qdefault').val( '{' + $('#questions').val() + '} ' );
        } else {
            $('#qdefault').val( $('#qdefault').val() + ' ' + $(this).attr('data-val') + ' ' + '{' + $('#questions').val() + '}' );
        }

    });
	$('#calc-c').click( function(e) {
	   $('#qdefault').val('');
    });

	$('#question-style').change();
});
</script>
