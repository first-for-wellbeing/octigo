<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<div class="row row-page-title">
	<h1>Manage Step Rules</h1>
</div>
<div class="row row-buttons">
	<div class="col-md-12">
		<div class="alert alert-info" id="button-add-assessment">Managing rules for <?php echo $ruletype . ' "' . $rulefor . '"'; ?></div>
	</div>
</div>


<?php
	View::element('rules_form', [
		'athemeid' => $athemeid,
		'questions_list' => $questions_list,
		'ruletypes_list' => $ruletypes_list,
		'ruleset' => $ruleset,
		'rules' => $rules,
		'rulevaluetypes_list' => $rulevaluetypes_list
	], 'firstforwellbeing');
?>


