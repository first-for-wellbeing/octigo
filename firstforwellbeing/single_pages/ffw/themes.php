<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>
<div class="row row-page-title">
	<h1>Manage Steps</h1>
</div>

	<div class="row row-buttons">
		<div class="col-md-12">
			<?php
			if(isset($editrecord)) {
				echo'Editing step "' . htmlentities($editrecord['name']) . '"';
				echo'&nbsp;&nbsp;<div class="button-admin button-blue"><a href="/ffw/theme_rules/' . $editrecord['id'] . '" style="color:inherit;">Edit rules for this step</a></div>';
			} else {
				echo'<div class="button-admin button-blue" id="button-add-theme">Add step</div>';
			}
			?>
		</div>
	</div>

<div class="row" id="row-add-theme" <?php echo (isset($editrecord) ? '' : 'style="display:none;"'); ?>>
	<form method="post" action="<?php echo $this->action('save'); ?>">
		<input type="hidden" name="editid" value="<?php echo (isset($editrecord['id']) ? $editrecord['id'] : ''); ?>" />
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                        <td class="col-md-3">Step name</td>
                        <td class="col-md-9"><input type="text" style="width:100%" name="ttitle" placeholder="Your Step's title" value="<?php echo (isset($editrecord['name']) ? htmlentities($editrecord['name']) : ''); ?>" /></td>
                    </tr>
                    <tr>
                        <td class="col-md-3">Step description</td>
                        <td class="col-md-9"><input type="text" style="width:100%" name="tdesc" placeholder="Your Step's description" value="<?php echo (isset($editrecord['description']) ? htmlentities($editrecord['description']) : ''); ?>" /></td>
                    </tr>
                </table>
            </div>
        </div>
		<br />
		<div class="row">
			<div class="col-md-6">
				<div class="row">
                    <div class="col-md-12">
                            <?php
                                if($questions_list !== null) {
                                    echo <<<END
                        <table class="table table-striped row-question-list">
                            <thead>
                                <tr>
                                    <th>
                                        <b>Click questions to add them to this Step.</b><br />
                                        <input type=text" placeholder="filter" id="filter" autocomplete="off" /> <span id="clear-filter">Clear filter</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
END;
                                    $count = 0;
                                    while($row = $questions_list->fetch()) {
                                        echo'<tr class="item-container filterable" qid="' . $row['id'] . '"><td><span class="question-item" title="' . htmlentities($row['question_text']) . '">' . htmlentities($row['title'] . ' [' . htmlentities(($row['data_handle'])) . ']') . '</span></td></tr>';
                                    }
                                    echo'</tbody></table>';
                                } else {
                                    echo'No questions defined, unable to add questions to Step.';
                                }
                            ?>
                    </div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped" id="row-added-questions">
                            <thead>
                                <tr>
                                    <th>
                                        <p>Added questions (drag to reorder):</p>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-11">
                <div class="button-admin-red"><a href="/ffw/themes" style="color:inherit;">Cancel</a></div>
			</div>
			<div class="col-md-1">
				<button type="submit" class="button-admin-green">Save</button>
			</div>
		</div>
	</form>
</div>


<?php
	if(!isset($editrecord)) {
?>
		<div class="row hide-on-add">
			<div class="col-md-12">
				<p>Your saved Steps</p>
			</div>
		</div>
		<?php
			if($totalrecs > 0) {
				// Calc prev/next links
				$prevlink = $this->action('') . '/' . ($pagenum > 1 ? $pagenum - 1 : 1) . '/' . $perpage;
				$nextlink = $this->action('') . '/' . ($pagenum * $perpage < $totalrecs ? $pagenum + 1 : $pagenum) . '/' . $perpage;
		?>
				<div class="row hide-on-add">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                                <th>Step</th>
                                <th>Description</th>
                                <th>Questions</th>
                                <th>&nbsp;</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </thead>
				</div>
					<?php
						while($row = $themes_list->fetch()) {
							echo'<tr>';
                                echo'<td>' . htmlentities($row['name']) . '</td>';
                                echo'<td>' . htmlentities($row['description']) . '</td>';
                                echo'<td>' . $row['cnt'] . '</td>';
                                echo'<td>&nbsp;</td>';
                                echo'<td><a href="/ffw/themes/' . $pagenum . '/' . $perpage . '/' . $row['id'] . '">Edit</a></td>';
                                echo'<td><span class="delete-item" qid="' . $row['id'] . '">X</span></td>';
							echo'</tr>';
						}
					?>
                        </table>
                    </div>
                </div>
				<div class="row row-pagination row-pagination-footer hide-on-add">
					<div class="col-md-1"><a href="<?php echo $prevlink;?>" class="<?php echo $prevclass; ?>">Prev</a></div>
					<div class="col-md-8 col-md-offset-1">Showing Steps <?php echo ($pagenum - 1) * $perpage + 1; ?> - <?php echo ( ($pagenum) * $perpage < $totalrecs ? ($pagenum) * $perpage : $totalrecs ); ?> of <?php echo $totalrecs; ?></div>
					<div class="col-md-1 col-md-offset-1"><a href="<?php echo $nextlink;?>" class="<?php echo $nextclass; ?>">Next</a></div>
				</div>
		<?php
			} else {
				echo'No Steps defined yet';
			}
	} // double outdent due to php tags
		?>


<script type="text/javascript">
ffw_editing = <?php echo (count($editrecord) > 1 ? 1 : 0); ?>;
<?php
// generate JS question list if needed
if(isset($editrecord['questions']) and count($editrecord['questions']) > 0) {
	echo 'ffw_questions = ';
	$q = [];
	foreach($editrecord['questions'] as $row) {
		$q[ $row['display'] ] = $row['question_id'];
	}
	echo json_encode($q) . ";\n\n";
}
?>
$(document).ready( function() {
	// make items sortable
	$('#row-added-questions tbody').sortable({
        axis : 'y',
        cursor: 'move',
        placeholder : 'sort-dest'
    });

	// Add question
	$('#button-add-theme').click( function(e) {
		$('#row-add-theme').fadeIn(500);
		$('.hide-on-add').fadeOut(500);
	});

	// Add another choice
	$('#add-choice').click( function(e) {
		var clone = $('.row-question-choice').first().clone(true, true);
		$(clone).find('input').val('');
		$(clone).find('.question-no').text( $('.row-question-choice').length + 1);
		$('.row-add-choice').before(clone);
	});

	// Delete choice
	$('.delete-choice').on('click', function(e) {
		if( $('.row-question-choice').length == 1) {
			alert('This style of question requires at least one choice');
			return;
		}
		$(this).parents('.row-question-choice').remove(); // delete row
		$('.row-question-choice').each( function(i) {
			// renumber rows
			$(this).find('.question-no').text(i + 1);
		});
	});

	// Add question
	$('.item-container').click( function(e) {
		if($(this).hasClass('disabled')) {
			return; // Unselectable item
		}
		var qid = $(this).attr('qid');
		var el = '<tr class="added-item" qid="' + qid + '">' +
            $(this).html() +
            '<td><input type="hidden" name="qid[]" value="' + qid + '" id="h' + qid + '"><a href="/ffw/questions/1/20/' + qid + '">Edit</a></td>' +
            '<td><a href="/ffw/question_rules/' + qid + '">Rules</a></td>' +
            '<td><span class="delete-added-item">X</span></td></tr>';
		$(this).addClass('disabled');
		$('#row-added-questions tbody').append(el);
	});

	// Delete added question
	$('#row-added-questions').on('click', '.delete-added-item', function(e) { // note use of on() to bind to future DOM objects with event-delegation
		var qid = $(this).parent().parent().attr('qid');
		$('.item-container[qid="' + qid + '"]').removeClass('disabled');
		$(this).parent().parent().remove();
	});

	// Delete item
	$('.delete-item').click( function(e) {
		var id = $(this).attr('qid');
		if(!confirm('Delete this item?')) {
			return;
		}
		window.location.href = 'themes/delete/' + id;
	});

	// If edit then 'prefill' questions, needs to be at end of scripts
	if(ffw_editing) {
		$.each( ffw_questions, function(i, v) {
			$('.item-container[qid="' + v + '"]').click(); // fake a click to a question
			$('#h' + i).val( i );
		});
	}

	// filter change
    $('#filter').change( function(e) {
       $('.filterable').each( function() {
           var rx = new RegExp($('#filter').val(), "gi");
           if( $(this).find('span.question-item').text().match(rx)) {
               $(this).show();
           } else {
               $(this).hide();
           }
       }) ;
    });

	// hit enter in filter
    $('#filter').keyup( function(e) {
        $(this).change();
    })

	// clear filter
    $('#clear-filter').click( function(e) {
       $('#filter').val('').change();
    });
});
</script>

