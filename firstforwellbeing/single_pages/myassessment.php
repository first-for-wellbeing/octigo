<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<?php

// lookup
$topic_names = array(
'alcohol' => 'Alcohol',
	'employment' => 'Employment and Adult Learning',
	'emotional' => 'Emotional Wellbeing',
	'cold-homes' => 'Cold Homes',
	'finance' => 'Financial',
	'housing' => 'Housing Support',
	'social' => 'Social Inclusion',
	'weight' => 'Weight Management',
	'smoking' => 'Stop Smoking'
);
// isolate priorities
$priorities = [];
foreach($response_collection['response_results'] as $k => $v) {
    // get priority scores
    if(strpos($k, '_priority') !== false) {
        $priorities[$k] = $v;
    }
}
asort($priorities);

// get provider blurb
$provider_blurb = get_provider_blurb();

if($response_collection['response_results']['on_hold'] == 1 and in_array('FFW Advisors', $usergroups)) {
    // if on hold, display unhold for assessor or non-interative content for user
        echo'<br /><h1 class="text-green underline">Wellbeing Assessment - Plan On Hold</h1>';
        echo'<a href="/myassessment/unhold/' . $response_collection['id'] . '" class="button button-green button-lrg button-block" />Unhold this plan</a><br />&nbsp;';
} else if ($response_collection['response_results']['focus_needed'] == 1) {
    output_focus_items($response_collection);
} else {
    // display plan results
    echo top_part($response_collection);
    // get content en masse
    $blurbtypes = [];
    foreach($priorities as $k => $priority) {
        $type = strtolower(str_replace('_priority', '', $k));
        // retrieve priority blurb from wordpress
        for($a = $priority; $a< 4; $a++) {
            $blurbtypes[] = [$type, $a];
        }
    }
    $blurbs = get_blurb($blurbtypes);
    // restructure blurbs
    $t = [];
    foreach($blurbs as $k => $v) {
        $t[$v[0]][$v[1]] = [ $v[2], $topic_names[$v[0]]];
    }
    $blurbs = $t;
    unset($t); // save memory

    // output priorities
    foreach($priorities as $k => $priority) {
        $type = strtolower(str_replace('_priority', '', $k));
        if($type == 'cold homes') { continue; } // skip
        echo'<div class="row"><div class="col-md-12 col-sm-12">';
        echo blurb_row($priority, $topic_names[$type], $blurbs[$type], $type, $response_collection, $usergroups, $provider_blurb);
        echo '</div>' . $provider_pops . '</div>';
    }
}

View::element('jsuistuff', [], 'firstforwellbeing');


////////////////////////////////////////////////////////////////////////
///
/// FUNCTIONS
///

function get_blurb($blurbtypes) {
    // get blurb from wordpress
    $wpurl = FFW_CONFIG[getenv('FFW_PLATFORM')]['wpurl'];
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $wpurl . '/wp-content/themes/wellbeing/functions/_get_content.php');
    curl_setopt($ch,CURLOPT_POST, 3);
    curl_setopt($ch,CURLOPT_POSTFIELDS, ['apikey' => '6le5VfcifI6a3mgin3VPPQ', 'data' => json_encode($blurbtypes), 'rnd' => rand()] );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    return unserialize($result);
}

function get_provider_blurb() {
    // get providers blurb from wordpress
    $wpurl = FFW_CONFIG[getenv('FFW_PLATFORM')]['wpurl'];
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $wpurl . '/wp-content/themes/wellbeing/functions/_get_providers.php');
    curl_setopt($ch,CURLOPT_POST, 2);
    curl_setopt($ch,CURLOPT_POSTFIELDS, ['apikey' => '6le5VfcifI6a3mgin3VPPQ', 'rnd' => rand()] );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    return unserialize($result);
}

function top_part($data) {
    $create_datetime = date('d/m/Y', strtotime($data['create_datetime']));
    // get octavo image
    $params = [
        'alcohol' => $data['response_results']['Alcohol_priority'],
        'employment' => $data['response_results']['Employment_priority'],
        'social' => $data['response_results']['Social_priority'],
        'emotional' => $data['response_results']['Emotional_priority'],
        'smoking' => $data['response_results']['Smoking_priority'],
        'weight' => $data['response_results']['Weight_priority'],
        'financial' => $data['response_results']['Finance_priority'],
        'housing' =>  $data['response_results']['Housing_priority']
    ];
    $b64 = octavo( $params );
    $html = <<<END
    <div class="row">
        <div class="col-md-6">
            <div class="wysiwyg">
                <h1 style="color:#486b2a; font-size:3.25rem; text-align:left;margin-bottom:0.25rem">Welcome to your Personal Plan</h1>
                <img src="/packages/firstforwellbeing/themes/ffw_assessment/images/img-underline-01.png"/>
                <p>Name: {$data['forename']} {$data['surname']}</p>
                <p>Date of Assessment: $create_datetime</p>
                <p>Assessment number: {$data['id']}</p>
                <p>Date of re-assessment: {$data['twelve_weeks']}</p>

                
                <p>Please see your results below.</p>
                <!--<a class="button button-green" href="#" title="Print results">Print results</a>-->
            </div>
        </div>
        <div class="col-md-6">
            <div class="octavo-graph">
                <img src="data:image/png;base64,$b64" style="width: 100%" />
            </div>
        </div>
    </div>
END;
    return $html;

}

function blurb_row($priority, $type, $blurb, $type_name, $response_collection, $usergroups, $provider_blurb) {
    foreach($response_collection['response_results'] as $k => $v) {
        // determine refocus
        $sent_mails = 0;
        if (strpos($k, '_p1_email_sent') !== false) {
            $sent_mails++;
        }
    }
    $can_refocus = ($sent_mails < 2) * 1;

    if($priority == 1 and $response_collection['response_results']['focus_item_1'] != strtolower($type_name) and $response_collection['response_results']['focus_item_2'] != strtolower($type_name)) {
        $style = 'style="opacity:0.5;"';
    } else {
        $style = '';
    }
    if($style != '') {
        $onhold = ' - On Hold';
    } else {
        $onhold = '';
    }
    $html = <<<END
    <div class="assessment-priority assessment-priority-$priority">
        <ul class="accordion">
            <li>
                <div class="accordion-btn" $style>
                    <div class="accordion-btn-bckgnd"></div>
                    <div class="label-priority label-priority-$priority">Priority $priority</div>
                    <div style="width:100%;margin:0;padding:0;position: relative;">
                        <div style="width: 50%;display: inline-block;margin: 0;padding: 0;color: black;"><strong>$type$onhold</strong></div>
                        <div style="width: 49%;text-align:right;margin: 0;display: inline-block;padding: 0;">
END;
    if($priority == 1) {
        if (in_array('FFW Advisors', $usergroups) and $can_refocus) {
            $show_refocus = '';
        } else {
            $show_refocus = 'style="display:none;"';
        }
        if(($response_collection['response_results']['focus_item_1'] != strtolower($type_name) and $response_collection['response_results']['focus_item_2'] != strtolower($type_name))) {
            $html .= '&nbsp;&nbsp;&nbsp;<a href="/myassessment/unholdp1/' . $response_collection['id'] . '/' . $type_name . '" data-rcid="' . $response_collection['id'] . '" data-type="' . $type_name . '" ' . $show_refocus . ' class="refocus held" theme="' . $type . '">Unhold this theme</a>';
        } else {
            $html .= '&nbsp;&nbsp;&nbsp;<a href="/myassessment/holdp1/' . $response_collection['id'] . '/' . $type_name . '" data-rcid="' . $response_collection['id'] . '" data-type="' . $type_name . '" ' . $show_refocus . ' class="refocus unheld" theme="' . $type . '">Put this theme on hold</a>';
        }
    }
    $html .= <<<END
                        </div>
                    </div>
                </div>
                <div class="accordion-content" style="display: block;">
END;
    // add blurb into html
    $p3_size = 0;
    foreach($blurb as $k => $v) {
        switch($k) {
            case 1:
                $html .= get_p1_html($v, $response_collection, $provider_blurb, $type_name, $usergroups);
                break;
            case 2:
                $html .= get_p2_html($v, $usergroups);
                $p3_size = 1; // if p2 is output then P3 goes into an md4 not an md12
                break;
            case 3:
                $html .= get_p3_html($v, $p3_size);
                break;
        }
    }
    $html .= <<<END
                </div>
            </li>
        </ul>
    </div>
END;
    return $html;
}

function get_p1_html($data, $response_collection, $provider_blurb, $type_name, $usergroups) {
    $html = '';
    $html .= '<div class="row"><div class="col-md-12">';
    if($response_collection['response_results']['on_hold']) {
        // No provider blurb if on hold, just info
        $html .= 'This theme is on hold, contact your First for Wellbeing advisor if you wish to change this.';
    } else {
        $html .= array_shift($data[0])[0];
        if (strtolower($response_collection['response_results']['focus_item_1']) != strtolower($type_name) and strtolower($response_collection['response_results']['focus_item_2']) != strtolower($type_name)) {
            // item not focussed, no provider blurb
        } else {
            $html .= get_provider_items($provider_blurb, $type_name, $response_collection);
        }
    }
    $html .= '</div></div>';
    $html .= output_notes($response_collection, $type_name, $usergroups);
    $html .= '<br />';
    return $html;
}

function get_p2_html($data, $usergroups) {
    $html = '
        <div class="row">
            <div class="col-md-8">
                <h2>Articles related to ' . $data[1] . '</h2>
                <ul>';
    foreach($data[0] as $page) {
        $text = substr( strip_tags($page[0]), 0, 100) . '...';
        $html .= '<li class="col-md-6 col-sm-6 no-accordion">
                    <div class="panel round-corners" style="height:536px;">
                        <a href="' . $page[1] . '">' . $page[2] . '<h2 class="text-green">' . htmlentities($page[3]) . '</h2><p>' . ($text) . '</p></a>
                    </div>
                </li>';
    }
     $html .= '</ul>
        </div>'; // NB close one div less as there will be P3 output in the same top enclosure. P3 will close it
    return $html;
}

function get_p3_html($data, $size) {
    $html = '';
    if($size) {
        $html .= '<div class="col-md-4">';
    } else {
        $html .= '<div class="row"><div class="col-md-12">';
    }
    $html .= '
        <aside class="aside aside-no-padding">
            <div class="mb30">
                <h2 style="font-size:2rem;">Articles related to ' . $data[1] . '</h2>
                <ul class="list-latest-more">';
    foreach($data[0] as $page) {
        $text = htmlentities(substr( strip_tags($page[0]), 0, 100) . '...');
        $html .= '<li ' . $liclass . '>
                        <a href="' . $page[1] . '">
                            <h3 class="text-green">' . htmlentities($page[3]) . '</h3>
                            <p>' . $text . '</p>
                        </a>
                    </li>';
    }
    $html .= '</ul>
                </div>
            </aside>
        </div>';
    if(!$size) {
        $html .= '</div>'; // close div opened at start
    }
    return $html;
}

function output_focus_items($response_collection) {
    // build P1 list - quicker for now to convert array to older Phase format for use with copied code
    $priorityItems = [];
    foreach($response_collection['response_results'] as $k => $v) {
        if(strpos($k, '_priority') !== false) {
            $priorityItems[$v][] = strtolower(substr($k,0, -9));
        }
    }
    $count = count($priorityItems);
    echo <<<END
<section class="section">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-green underline">Wellbeing Assessment</h1>
            <p>Thanks for completing the Wellbeing Assessment.</p>
            <p>Based on your answers you have scored high in the following areas, please choose which two you would like to concentrate on.</p>
            <form method="POST" id="form" action="/myassessment/refocus/{$response_collection['id']}" class="choose-focus-items">
END;
    $forcesocemo = 0;
    $force_checked = false;
    if(in_array('emotional', $priorityItems[1])) {
        $forcesocemo++;
        output_check_box("emotional", true, true);
        echo'<input type="hidden" name="focus_item[emotional]" value="1" />'; // disabled fields won't submit so create a hidden counterpart
    }
    if(in_array('social', $priorityItems[1])) {
        $forcesocemo++;
        output_check_box("social", true, true);
    }
    if ($forcesocemo == 2) { $twochecked = true; } else { $twochecked = false; }
    if(in_array('smoking', $priorityItems[1])) {
        if($twochecked) {
            output_check_box("smoking", false, true);
        } else {
            if($isPregnantSmoking) {
                output_check_box("smoking", true, true);
                $forcesocemo++;
            } else {
                output_check_box("smoking");
            }
        }
    }
    if ($forcesocemo == 2) { $twochecked = true; } else { $twochecked = false; }
    if(in_array('weight', $priorityItems[1])) {
        if($twochecked) {
            output_check_box("weight", false, true);
        } else {
            output_check_box("weight");
        }
    }
    if(in_array('alcohol', $priorityItems[1])) {
        if($twochecked) {
            output_check_box("alcohol", false, true);
        } else {
            output_check_box("alcohol");
        }
    }
    if(in_array('finance', $priorityItems[1])) {
        if($twochecked) {
            output_check_box("finance", false, true);
        } else {
            output_check_box("finance");
        }
    }
    if(in_array('employment', $priorityItems[1])) {
        if($twochecked) {
            output_check_box("employment", false, true);
        } else {
            output_check_box("employment");
        }
    }
    if(in_array('housing', $priorityItems[1])) {
        if($twochecked) {
            output_check_box("housing", false, true);
        } else {
            output_check_box("housing");
        }
    }
    echo <<<END
<div class="text-right">
        <button type="submit" class="button button-green btn-save-focus <?php if($forcesocemo < 2) echo 'hide' ?>" data-force-checked="<?= $checked_items ?>">Save Choices</button>
    </div>
</form>
</div>
</div>
</section>
END;
}

function output_check_box( $type = '', $checked = false, $readonly = false ) {
    if($checked) { $checked = 'checked="checked"'; } else { $checked = ''; }
    if($readonly) { $readonly = 'disabled="disabled"'; } else { $readonly = ''; }
    $type2 = ucwords(str_replace('-', ' ', $type));
    echo <<<HTML
	<div class="input-wrapper radio no-feedback">
		<div class="row no-gutter content-position-outer">
			<div class="col-md-3 content-center-inner">
				<label for="focus_item_$type">$type2</label>
			</div>
			<div class="col-md-3 content-center-inner">
				<div class="row">
					<div class="col-md-5">
						<input type="checkbox" value="1" name="focus_item[$type]" id="focus_item_$type" $checked $readonly>
						<label for="focus_item_$type">&nbsp;</label>
				   </div>
				</div>
			</div>
		</div>
	</div>
HTML;
}

function get_provider_items($blurb, $item, $response_collection) {
    $html = '';
    $results = $response_collection['response_results'];
    if($results['alcohol_gt19'] and $item == 'alcohol') {
        foreach($blurb[$item] as $i => $v) {
            if($v['name'] == 'S2S') {
                $blurb[$item] = [$v];
                break;
            }
        }
    }
    // Add provider buttons
    $count = 0;
    $provider_pops = '';
    $focus_providers = new \stdClass;
    if(isset($results['focus_item_1'], $results['focus_provider_1'])) {
        $focus_providers->{$results['focus_item_1']} = $results['focus_provider_1'];
    }
    if(isset($results['focus_item_2'], $results['focus_provider_2'])) {
        $focus_providers->{$results['focus_item_2']} = $results['focus_provider_2'];
    }
    foreach($blurb[$item] as $k => $provider_info) {
        if(isset($results['focus_provider_1'], $results['focus_provider_2']) and !in_array($k, [ $results['focus_provider_1'], $results['focus_provider_2'] ])) {
            continue; // no point displaying provider choices you can't make if two selected already
        }
        // get provider username
        $puser = $blurb[$item][$k]['user_login'];
        // determine any selected focus providers
        if(!isset($focus_providers->$item)) {
            $focus_providers->$item = '';
        }

        if($item == 'weight' and $results['low_bmi']) { continue; } // no buttons for low bmi. Go to Doctor.
        $user_bio = $provider_info['bio'];
        $user_bio_short = substr(strip_tags($user_bio), 0, 150) . '&nbsp;<b><a href="" class="p1-provider-more" provider-id="' . $k . '">...read more</a></b>';

        if($focus_providers->$item != ''  and  $focus_providers->$item != $puser) {
            continue; // provider for this theme is selected, skip others
        } elseif($focus_providers->$item == $puser or count($blurb[$item]) == 1) {
            // just display the provider pop info
            $provider_button = '<button type="button" class="send-p1-email" provider="' . $k . '" providername="' . $puser . '"' . ($focus_providers->$item == $puser ? ' chosen="chosen"' : ''.$typename) . ($focus_providers->$item != '' ? ' disabled="disabled"' : '') . '>I confirm that my details can be passed to ' . $provider_info['name'] . '</button>';
            $html .= '<div class="row provider-pop provider-pop-' . $k . '"><div class="col-md-12">' . $provider_pop_img[0] . '<p>' . $user_bio . '</p></div></div>';
            $html .= '<div class="row provider-pop provider-pop-' . $k . '"><div class="col-md-4">' . $provider_button . '</div></div>';
            continue;
        }

        if($item == 'housing') {
            if(($results['homeless_nnpc'] or $results['homeless_ld'] or $results['homeless_other']) and $provider_info['user_login'] != 'NAASH') {
                continue; // skip as only NAASH for homeless + housing
            } elseif(!($results['homeless_nnpc'] or $results['homeless_ld'] or $results['homeless_other']) and $provider_info['user_login'] == 'NAASH') {
                continue; // don't show NAASH for anything else
            }
            if($provider_info['user_login'] == 'accommodationconcern' and (!($results['homeless_risk_nnru'] or $results['homeless_risk_weda'] or $results['homeless_risk_other'])) ) {
                continue; // skip
            }
            // always show delos when not homeless
            //if($provider_info['user_login'] == 'delos' and ($assessmentEntry->housing_needs != 'I’m at risk of homelessness') ) {
            //continue; // skip
            //}
            if($provider_info['user_login'] == 'midlandheart' and (!($results['homeless_risk_nnru'] or $results['homeless_risk_weda'] or $results['homeless_risk_other'])) ) {
                continue; // skip
            }
            if($provider_info['user_login'] == 'mayday' and (!($results['homeless_risk_nnru'] or $results['homeless_risk_weda'] or $results['homeless_risk_other'])) ) {
                continue; // skip
            }
            if($provider_info['user_login'] == 'serve' and !$results['housing_pc']) {
                continue;
            }
            if($provider_info['user_login'] == 'daventrycarerepair' and !$results['housing_pc55d'] ) {
                continue;
            }
            // THESE LINES DUPLICATED TO SHOW HOW POSTCODE REGION WORKED WHEN IT'S PUT BACK IN
            //if($provider_info['user_login'] == 'daventrycarerepair' and ($assessmentEntry->home_improvements != 1 or ($diff <56 and $assessmentEntry->disability_status == '' or !in_array($outcode, $daventry)) ) ) {
            //continue;
            //}
            if($provider_info['user_login'] == 'northamtonshire-carerepair' and !$results['housing_pc55d'] ) {
                continue;
            }
            if($provider_info['user_login'] == 'spirecarerepair' and !$results['housing_pc55d'] ) {
                continue;
            }
        }

        if($item == 'alcohol') {
            if($results['alcohol_gt19'] and $provider_info['user_login'] != 'S2S') {
                continue; // skip as only S2S for high score
            } elseif(!$results['alcohol_gt19'] and $provider_info['user_login'] == 'S2S') {
                continue; // don't show S2S for lower scores
            }
        }

        if($item == 'finance') {
            if(in_array($outcode, $response_collection['cab_postcodes'])) { // todo
                // for CAB
                if( $provider_info['user_login'] != 'CAB' ) {
                    continue;
                }
            } else {
                // For Community Law
                if( $provider_info['user_login'] != 'communitylaw' ) {
                    continue;
                }
            }
        }

        // main content
        if( count($blurb[$item]) > 1 ) {
            if($count % 4 == 0) {
                $html .= '<div class="row p1-provider-icons" style="margin-top:1.5em;">';
            }
            $html .= '<div class="col-md-3 provider-container"><div style="height:150px; display:table-cell;">'.$provider_info['user_img'][0].'</div><div height="150px; overflow:hidden;">'.$user_bio_short.'</div></div>';
            $provider_button = '<button type="button" class="send-p1-email" data-rcid="' . $response_collection['id'] . '" provider="' . $k . '" providername="' . $puser . '"' . ($focus_providers->$item == $puser ? ' chosen="chosen"' : ''.$typename) . ($focus_providers->$item != '' ? ' disabled="disabled"' : '') . '>I confirm that my details can be passed to ' . $provider_info['name'] . '</button>';
            // provider popout content
            $provider_pops .= '<div class="row provider-pop provider-pop-' . $k . '" style="display:none;"><div class="col-md-12">' . $provider_info['provider_pop_img'][0] . '<p>' . $user_bio . '</p></div></div>';
            $provider_pops .= '<div class="row provider-pop provider-pop-' . $k . '" style="display:none;"><div class="col-md-4">' . $provider_button . '</div><div class="col-md-4 col-md-offset-4"><button type="button" class="button button-pink back-to-providers" provider="' . $k . '">Go back to the provider list</button></div></div>';
            $count++;
            if($count % 4 == 0) {
                $html .= '</div>';
            }
        } else {
            // Just show the pop info as it's one choice
            // provider popout content
            $provider_button = '<button type="button" class="send-p1-email" data-rcid="' . $response_collection['id'] . '" provider="' . $k . '" providername="' . $puser . '"' . ($focus_providers->$item == $puser ? ' chosen="chosen"' : ''.$typename) . ($focus_providers->$item != '' ? ' disabled="disabled"' : '') . '>I confirm that my details can be passed to ' . $provider_info['name'] . '</button>';
            $provider_pops .= '<div class="row provider-pop provider-pop-' . $k . '"><div class="col-md-12">' . $provider_info['provider_pop_img'][0] . '<p>' . $user_bio . '</p></div></div>';
            $provider_pops .= '<div class="row provider-pop provider-pop-' . $k . '"><div class="col-md-4">' . $provider_button . '</div></div>';
        }
    }
    $html2 .= '<div class="row" style="margin-bottom:30px;"><div class="col-md-12">' . $html . '</div>' . $provider_pops . '</div>';
    return $html2;
}

function output_notes($response_collection, $item, $usergroups) {
    $html = '';
    $results = $response_collection['response_results'];
    $obj_key = 'user_notes_' . $item;
    if( in_array('FFW Advisors', $usergroups) ) {
        if(!empty($results[$obj_key])) {
            $text = stripslashes($results[$obj_key]);
        }
        $html = <<<END
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <br /><br /><br />
                        <label for="user_notes-$item">Add notes on what you have done for your customer here</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-edit ">
                            <div class="input-wrapper input-wrapper-sml">
                                <div class="form-control-textarea form-control-sml">
                                    <textarea id="user_notes-$item" placeholder="Add notes on what you have done for your customer here" name="user_notes_$item" rows="3">$text</textarea>
                                </div>
                            </div>
                            <div class="text-right">
                                <button class="button button-green save-note" type="submit" data-rcid="{$response_collection['id']}">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
END;
    } elseif( !empty($results[$obj_key]) ) {
        if(!empty($results[$obj_key])) {
            $text = nl2br(htmlentities($results[$obj_key]));
        }
        $html = <<<END
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10 col-offset-md1">
                        <h3>Assessor notes:</h3><br />
                        <p>$text</p>
                    </div>
                </div>
            </div>
        </div>
END;
    }
    return $html;
}

function octavo($data) {
    $image = imagecreatefrompng(dirname(__FILE__) . "../../themes/ffw_assessment/images/octavo.png");
    imageAlphaBlending($image, true);
    imageSaveAlpha($image, true);

// For P1/2/3 scoring:
    $priority_score = array(1,15, 10, 4); // lookup for P1/2/3 score values
    $socialDefault = $priority_score[ $data['social']+0 ];
    $emotionalDefault = $priority_score[ $data['emotional']+0 ];
    $financialDefault = $priority_score[ $data['financial']+0 ];
    $weightDefault = $priority_score[ $data['weight']+0 ];
    $employmentDefault = $priority_score[ $data['employment']+0 ];
    $alcoholDefault = $priority_score[ $data['alcohol']+0 ];
    $housingDefault = $priority_score[ $data['housing']+0 ];
    $smokingDefault = $priority_score[ $data['smoking']+0 ];

//Sizes
    $dotSize = 30;
    $enlargeSize = 3;
    $dotsArr = array();

//Dots Social
    $dotPoint1 = 275;
    $dotPoint2 = 25;
    $dotsInnerArr = array();
    $selected = $socialDefault;
    for ($x = 1; $x <= 18; $x++) {
        if($selected == $x) $dotsInnerArr[] = array($dotPoint1, $dotPoint2);
        $dotPoint2 = $dotPoint2+8.15;
    }
    $dotsArr[] = $dotsInnerArr;

    //Dots Angle 1
    $dotPoint1 = 353;
    $dotPoint2 = 25;
    $dotsInnerArr = array();
    for ($x = 1; $x <= 18; $x++) {
        if($selected == $x) { $dotsInnerArr[] = array($dotPoint1, $dotPoint2, 'small'); break; }
        $dotPoint1 = $dotPoint1-3.3;
        $dotPoint2 = $dotPoint2+8.15;
    }
    $dotsArr[] = $dotsInnerArr;

//Dots Emotional
    $dotPoint1 = 409;
    $dotPoint2 = 81;
    $dotsInnerArr = array();
    $lastSelected = $selected;
    $selected = $emotionalDefault;
    for ($x = 1; $x <= 18; $x++) {
        if($lastSelected == $x) $dotsInnerArr[0] = array($dotPoint1, $dotPoint2, 'small');
        if($selected == $x) $dotsInnerArr[1] = array($dotPoint1, $dotPoint2);
        if(count($dotsInnerArr)==2) break;
        $dotPoint1 = $dotPoint1-5.8;
        $dotPoint2 = $dotPoint2+5.8;
    }
    $dotsArr[] = $dotsInnerArr;

    //Dots Angle 2
    $dotPoint1 = 463;
    $dotPoint2 = 135;
    $dotsInnerArr = array();
    for ($x = 1; $x <= 18; $x++) {
        if($selected == $x) { $dotsInnerArr[] = array($dotPoint1, $dotPoint2, 'small'); break; }
        $dotPoint1 = $dotPoint1-8.15;
        $dotPoint2 = $dotPoint2+3.3;
    }
    $dotsArr[] = $dotsInnerArr;

//Dots Financial
    $dotPoint1 = 463;
    $dotPoint2 = 214;
    $dotsInnerArr = array();
    $lastSelected = $selected;
    $selected = $financialDefault;
    for ($x = 1; $x <= 18; $x++) {
        if($lastSelected == $x) $dotsInnerArr[0] = array($dotPoint1, $dotPoint2, 'small');
        if($selected == $x) $dotsInnerArr[1] = array($dotPoint1, $dotPoint2);
        if(count($dotsInnerArr)==2) break;
        $dotPoint1 = $dotPoint1-8.15;
    }
    $dotsArr[] = $dotsInnerArr;

    //Dots Angle 3
    $dotPoint1 = 463;
    $dotPoint2 = 289;
    $dotsInnerArr = array();
    for ($x = 1; $x <= 18; $x++) {
        if($selected == $x) { $dotsInnerArr[] = array($dotPoint1, $dotPoint2, 'small'); break; }
        $dotPoint1 = $dotPoint1-8.15;
        $dotPoint2 = $dotPoint2-3.3;
    }
    $dotsArr[] = $dotsInnerArr;

//Dots Weight
    $dotPoint1 = 409;
    $dotPoint2 = 345;
    $dotsInnerArr = array();
    $lastSelected = $selected;
    $selected = $weightDefault;
    for ($x = 1; $x <= 18; $x++) {
        if($lastSelected == $x) $dotsInnerArr[0] = array($dotPoint1, $dotPoint2, 'small');
        if($selected == $x) $dotsInnerArr[1] = array($dotPoint1, $dotPoint2);
        if(count($dotsInnerArr)==2) break;
        $dotPoint1 = $dotPoint1-5.8;
        $dotPoint2 = $dotPoint2-5.8;
    }
    $dotsArr[] = $dotsInnerArr;

    //Dots Angle 4
    $dotPoint1 = 353;
    $dotPoint2 = 399;
    $dotsInnerArr = array();
    for ($x = 1; $x <= 18; $x++) {
        if($selected == $x) { $dotsInnerArr[] = array($dotPoint1, $dotPoint2, 'small'); break; }
        $dotPoint1 = $dotPoint1-3.3;
        $dotPoint2 = $dotPoint2-8.15;
    }
    $dotsArr[] = $dotsInnerArr;

//Dots Employment
    $dotPoint1 = 275;
    $dotPoint2 = 399;
    $dotsInnerArr = array();
    $lastSelected = $selected;
    $selected = $employmentDefault;
    for ($x = 1; $x <= 18; $x++) {
        if($lastSelected == $x) $dotsInnerArr[0] = array($dotPoint1, $dotPoint2, 'small');
        if($selected == $x) $dotsInnerArr[1] = array($dotPoint1, $dotPoint2);
        if(count($dotsInnerArr)==2) break;
        $dotPoint2 = $dotPoint2-8.15;
    }
    $dotsArr[] = $dotsInnerArr;

    //Dots Angle 5
    $dotPoint1 = 198;
    $dotPoint2 = 399;
    $dotsInnerArr = array();
    for ($x = 1; $x <= 18; $x++) {
        if($selected == $x) { $dotsInnerArr[] = array($dotPoint1, $dotPoint2, 'small'); break; }
        $dotPoint1 = $dotPoint1+3.3;
        $dotPoint2 = $dotPoint2-8.15;
    }
    $dotsArr[] = $dotsInnerArr;

//Dots Alcohol
    $dotPoint1 = 144;
    $dotPoint2 = 345;
    $dotsInnerArr = array();
    $lastSelected = $selected;
    $selected = $alcoholDefault;
    for ($x = 1; $x <= 18; $x++) {
        if($lastSelected == $x) $dotsInnerArr[0] = array($dotPoint1, $dotPoint2, 'small');
        if($selected == $x) $dotsInnerArr[1] = array($dotPoint1, $dotPoint2);
        if(count($dotsInnerArr)==2) break;
        $dotPoint1 = $dotPoint1+5.8;
        $dotPoint2 = $dotPoint2-5.8;
    }
    $dotsArr[] = $dotsInnerArr;

    //Dots Angle 6
    $dotPoint1 = 88;
    $dotPoint2 = 289;
    $dotsInnerArr = array();
    for ($x = 1; $x <= 18; $x++) {
        if($selected == $x) { $dotsInnerArr[] = array($dotPoint1, $dotPoint2, 'small'); break; }
        $dotPoint1 = $dotPoint1+8.15;
        $dotPoint2 = $dotPoint2-3.3;
    }
    $dotsArr[] = $dotsInnerArr;

//Dots Housing
    $dotPoint1 = 88;
    $dotPoint2 = 214;
    $dotsInnerArr = array();
    $lastSelected = $selected;
    $selected = $housingDefault;
    for ($x = 1; $x <= 18; $x++) {
        if($lastSelected == $x) $dotsInnerArr[0] = array($dotPoint1, $dotPoint2, 'small');
        if($selected == $x) $dotsInnerArr[1] = array($dotPoint1, $dotPoint2);
        if(count($dotsInnerArr)==2) break;
        $dotPoint1 = $dotPoint1+8.15;
    }
    $dotsArr[] = $dotsInnerArr;

    //Dots Angle 7
    $dotPoint1 = 88;
    $dotPoint2 = 135;
    $dotsInnerArr = array();
    for ($x = 1; $x <= 18; $x++) {
        if($selected == $x) { $dotsInnerArr[] = array($dotPoint1, $dotPoint2, 'small'); break; }
        $dotPoint1 = $dotPoint1+8.15;
        $dotPoint2 = $dotPoint2+3.3;
    }
    $dotsArr[] = $dotsInnerArr;

//Dots Smoking
    $dotPoint1 = 143;
    $dotPoint2 = 80;
    $lastSelected = $selected;
    $dotsInnerArr = array();
    $selected = $smokingDefault;
    for ($x = 1; $x <= 18; $x++) {
        if($lastSelected == $x) $dotsInnerArr[0] = array($dotPoint1, $dotPoint2, 'small');
        if($selected == $x) $dotsInnerArr[1] = array($dotPoint1, $dotPoint2);
        if(count($dotsInnerArr)==2) break;
        $dotPoint1 = $dotPoint1+5.8;
        $dotPoint2 = $dotPoint2+5.8;
    }
    $dotsArr[] = $dotsInnerArr;

    //Dots Angle 8
    $dotPoint1 = 198;
    $dotPoint2 = 25;
    $dotsInnerArr = array();
    for ($x = 1; $x <= 18; $x++) {
        if($selected == $x) { $dotsInnerArr[] = array($dotPoint1, $dotPoint2, 'small'); break; }
        $dotPoint1 = $dotPoint1+3.3;
        $dotPoint2 = $dotPoint2+8.15;
    }
    $dotsArr['last'] = $dotsInnerArr;

//Dots Last
    $dotPoint1 = 275;
    $dotPoint2 = 25;
    $dotsInnerArr = array();
    $lastSelected = $selected;
    $selected = $socialDefault;
    for ($x = 1; $x <= 18; $x++) {
        if($lastSelected == $x) $dotsInnerArr[0] = array($dotPoint1, $dotPoint2, 'small');
        if($selected == $x) $dotsInnerArr[1] = array($dotPoint1, $dotPoint2, 'small');
        if(count($dotsInnerArr)==2) break;
        $dotPoint2 = $dotPoint2+8.15;
    }
    $dotsArr[] = $dotsInnerArr;

// choose a color for the ellipse
    $col_ellipse = imagecolorallocate($image, 48, 49, 50);
    imagesetthickness($image, 5);
    imagealphablending($image, true);

// draw the white ellipse
    $lastX = 0;
    $lastY = 0;

    foreach($dotsArr as $dotinnerdots) {
        ksort($dotinnerdots);
        foreach($dotinnerdots as $dot) {
            $thisDotSize = $dotSize;
            $thisDotColour = $col_ellipse;
            if(isset($dot[2]) && $dot[2] == 'small') {
                $thisDotSize = 1;
            }
            imagefilledellipse($image, $dot[0]*$enlargeSize, $dot[1]*$enlargeSize, $thisDotSize, $thisDotSize, $thisDotColour);
            if($lastX != 0) imageline($image, $dot[0]*$enlargeSize, $dot[1]*$enlargeSize, $lastX*$enlargeSize, $lastY*$enlargeSize, $thisDotColour);
            $lastX = $dot[0];
            $lastY = $dot[1];
        }
    }
    // convert to base64. Shame GD doesn't have a function for this
    ob_start();
    imagepng($image);
    $img = ob_get_contents();
    ob_end_clean();
    $b64 = base64_encode($img);

    //header('Content-Type: image/png');

    //imagepng($image);
    imagedestroy($image);
    return $b64;
}