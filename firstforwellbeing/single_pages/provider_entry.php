<?php
defined('C5_EXECUTE') or die('Access Denied.');
?>
<div id="content" class="container">
    <div class="row">
        <div class="col-md-12 assessment-priority">
            <div class="wysiwyg">
                <h1 style="text-align:left; margin-bottom:0.25em;">Provider Entry</h1>
                <img src="/packages/firstforwellbeing/themes/ffw_assessment/images/img-underline-01.png"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 assessment-priority">
            <ul class="accordion">
                <li><!-- contact details -->
                    <div class="accordion-btn">
                        <div class="accordion-btn-bckgnd"></div>
                        <div class="label-priority label-priority-green text-center">&nbsp;</div>
                        <span>Contact details</span>
                    </div>
                    <div class="accordion-content">
                        <div class="table-responsive">
                            <table class="table table-fixed">
                                <tbody>
                                <tr >
                                    <th>User ID</th>
                                    <td><?php echo $assessment['user_id']; ?></td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td><?php echo $assessment['forename'] . ' ' .$assessment['surname'] ?></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td><?php echo $assessment['email']; ?></td>
                                </tr>
                                <tr>
                                    <th>Address</th>
                                    <td><?php echo $assessment['address1']; ?>,<?php echo $assessment['address2']; ?><br />
                                        <?php echo $assessment['town']; ?>,<br />
                                        <?php echo $assessment['county']; ?><br />
                                        <?php echo $assessment['postcode']; ?></td>
                                </tr>
                                <tr>
                                    <th>Telephone number</th>
                                    <td><?php echo $assessment['tel1']; ?></td>
                                </tr>
                                <tr>
                                    <th>Mobile</th>
                                    <td><?php echo $assessment['tel2']; ?></td>
                                </tr>
                                <tr>
                                    <th>Preferred method of contact</th>
                                    <td><?php if( $assessment->contact_method == 1) echo 'Email'; else if( $assessment->contact_method == 2) echo 'Telephone'; else echo 'Post'; ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </li>
                <li><!-- Assessment Entry -->
                    <div class="accordion-btn">
                        <div class="accordion-btn-bckgnd"></div>
                        <div class="label-priority label-priority-green text-center">&nbsp;</div>
                        <span>Assessment entry</span>
                    </div>
                    <div class="accordion-content">
                        <div class="table-responsive">
                            <table class="table table-fixed">
                                <tbody>
                                <tr class="date-row">
                                    <th>Date</th>
                                    <td><?php echo date('d/m/Y H:i:s', strtotime($assessment['created']) ); ?></td>
                                </tr>
                                <tr>
                                    <th>Customer ID</th>
                                    <td><?php echo $assessment['user_id']; ?></td>
                                </tr>
                                <tr>
                                    <th>Assessment ID</th>
                                    <td><?php echo $assessment['id']; ?></td>
                                </tr>
                                <tr>
                                    <th>Assessment number</th>
                                    <td><?php echo $assessment['assessment_number']; ?></td>
                                </tr>
                                <tr>
                                    <th>Assessment completed by</th>
                                    <td><?php if ($assessment['onbehalfof']['qc_value'] == 2) { echo 'Myself'; } else if($assessment['relationtoassessee']['qc_choice'] != 'Other') { echo $assessment['relationtoassessee']['qc_value']; } else { echo $assessment['relationother']['qc_value']; } ?></td>
                                </tr>
                                <tr>
                                    <th>Assessment conducted at</th>
                                    <td><?php if (!isset($assessment['assessmentlocation'])) { echo 'N/A'; } else if($assessment['assessmentlocation']['qc_choice'] != 'Other') { echo $assessment['assessment_location']; } else { echo $assessment->assessment_location_other; } ?></td>
                                </tr>
                                <?php
                                $location_array = array("GP", "Community Centre", "Library", "pharmacy");
                                if (in_array($assessment->assessment_location, $location_array)) {
                                    ?>
                                    <tr>
                                        <th>Location</th>
                                        <td><?php echo str_replace("error","",
                                                $assessment->assessment_location_gp.
                                                $assessment->assessment_location_comm_centre.
                                                $assessment->assessment_location_library.
                                                $assessment->assessment_location_pharmacy);
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <th>Date of birth</th>
                                    <td><?php echo date('d/m/Y', strtotime($assessment['dob']) ); ?></td>
                                </tr>
                                <tr>
                                    <th>Has customer had a health check in last 4 years?</th>
                                    <td><?php echo intToYesNo($assessment->health_check_in_the_last_4_years); ?></td>
                                </tr>
                                <tr>
                                    <th>Gender</th>
                                    <td><?php echo $assessment->gender; ?></td>
                                </tr>
                                <?php
                                if($assessment->gender == 2) {
                                    ?>
                                    <tr>
                                        <th>Pregnant?</th>
                                        <td><?php echo intToYesNo($assessment->pregnant); ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <th>Religion</th>
                                    <td><?php echo ($assessment->religion == 'Other') ? $assessment->religion_other : $assessment->religion; ?></td>
                                </tr>
                                <tr>
                                    <th>Marital status</th>
                                    <td><?php echo ($assessment->marital_status == 'Other') ? $assessment->marital_status_other : $assessment->marital_status; ?></td>
                                </tr>
                                <tr>
                                    <th>Parental status</th>
                                    <td><?php echo $assessment->parental_status; ?></td>
                                </tr>
                                <tr>
                                    <th>Number of Dependants</th>
                                    <td><?php echo $assessment->dependants; ?></td>
                                </tr>
                                <?php
                                if($provider_type == 'employment'):
                                    ?>
                                    <tr>
                                        <th>Employment status</th>
                                        <td><?php echo ($assessment->employment_status == 'Other') ? $assessment->employment_status_unable_to_work : $assessment->employment_status; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Employment details</th>
                                        <td><?php echo ($assessment->employment_details == 'error' || empty($assessment->employment_details)) ? "N/A" : $assessment->employment_details; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Last day of employment</th>
                                        <td><?php echo ($assessment->employment_last_working_day == 'error' || empty($assessment->employment_last_working_day)) ? "N/A" : $assessment->employment_last_working_day; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Manual Worker?</th>
                                        <td><?php echo ($assessment->employment_status != 'Employed' && $assessment->employment_status != 'Self Employed') ? "N/A" : intToYesNo($assessment->manual_worker) ?></td>
                                    </tr>
                                    <tr>
                                        <th>On benefits?</th>
                                        <td><?php echo intToYesNo($assessment->on_benefits) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Type of benefits</th>
                                        <td><?php echo ($assessment->benefits_type == 'Other') ? $assessment->benefits_type_other : ((empty($assessment->benefits_type) || $assessment->benefits_type == "error") ? "N/A" : $assessment->benefits_type) ; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Disability Status</th>
                                        <td><?php echo ($assessment->disability_status == 'Other') ? $assessment->disability_status_other : (empty($assessment->disability_status) || $assessment->disability_status == "error" ? "N/A" : $assessment->disability_status) ; ?></td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <th>Ethnic origin</th>
                                    <td><?php echo ($assessment->ethnic_origin == 'Other') ? $assessment->ethnic_origin_white_other : $assessment->ethnic_origin; ?></td>
                                </tr>
                                <tr>
                                    <th>Sexuality</th>
                                    <td><?php echo $assessment->sexuality; ?></td>
                                </tr>
                                <tr>
                                    <th>Are you pregnant?</th>
                                    <td><?php echo intToYesNo($assessment->pregnant) ?></td>
                                </tr>
                                <?php
                                if($provider_type == 'smoking'): ///////////////// smoking
                                    ?>
                                    <tr>
                                        <th>Score</th>
                                        <td><?php echo $scores->smoking_score; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Do you smoke?</th>
                                        <td><?php echo intToYesNo($assessment->smoke) ?></td>
                                    </tr>
                                    <?php if($assessment->smoke == '1') { ?>
                                    <tr>
                                        <th>I want to stop smoking?</th>
                                        <td><?php echo sliderValueToText($assessment->stop_smoking) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I would benefit from advice / support on quitting?</th>
                                        <td><?php echo sliderValueToText($assessment->advice_on_quitting) ?></td>
                                    </tr>
                                <?php } ?>
                                <?php endif; ?>
                                <?php
                                if($provider_type == 'social'): ///////////////// social
                                    ?>
                                    <tr>
                                        <th>Score</th>
                                        <td><?php echo $scores->social_score; ?></td>
                                    </tr>
                                    <tr>
                                        <th>I often feel lonely?</th>
                                        <td><?php echo sliderValueToText($assessment->often_feel_lonely) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I have few opportunities to socialise with other people?</th>
                                        <td><?php echo sliderValueToText($assessment->opportunities_to_socialise) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I am unable to get out and about/ I do not feel close to the people in my local area?</th>
                                        <td><?php echo sliderValueToText($assessment->unable_to_get_out_and_about) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I struggle to mix well with other people?</th>
                                        <td><?php echo sliderValueToText($assessment->struggle_to_mix_well) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Are you currently receiving any specialist mental health services?</th>
                                        <td><?php echo intToYesNo($assessment->receiving_specialist_mh_services); ?></td>
                                    </tr>

                                    <?php
                                    if( $assessment->feeling_useful != '' ) {
                                        ?>
                                        <tr>
                                            <th>I've been feeling optimistic about the future?</th>
                                            <td><?php echo sliderValueToFrequency($assessment->feeling_optimistic) ?></td>
                                        </tr>
                                        <tr>
                                            <th>I've been feeling useful?</th>
                                            <td><?php echo sliderValueToFrequency($assessment->feeling_useful) ?></td>
                                        </tr>
                                        <tr>
                                            <th>I've been feeling relaxed</th>
                                            <td><?php echo sliderValueToFrequency($assessment->feeling_relaxed) ?></td>
                                        </tr>
                                        <tr>
                                            <th>I've been dealing with problems well?</th>
                                            <td><?php echo sliderValueToFrequency($assessment->dealing_with_problems_well) ?></td>
                                        </tr>
                                        <tr>
                                            <th>I've been thinking clearly?</th>
                                            <td><?php echo sliderValueToFrequency($assessment->thinking_clearly) ?></td>
                                        </tr>
                                        <tr>
                                            <th>I've been feeling close to other people?</th>
                                            <td><?php echo sliderValueToFrequency($assessment->feeling_close_to_other_people) ?></td>
                                        </tr>
                                        <tr>
                                            <th>I've been able to make up my own mind about things?</th>
                                            <td><?php echo sliderValueToFrequency($assessment->able_to_make_up_my_own_mind_about_things) ?></td>
                                        </tr>

                                        <?php
                                    } else { // show older responses
                                        ?>

                                        <!--<tr>
										<th>I often feel down or worried?</th>
										<td><?php echo sliderValueToText($assessment->often_feel_down_or_worried) ?></td>
									</tr>-->
                                        <tr>
                                            <th>I lack confidence to make my own decisions?</th>
                                            <td><?php echo sliderValueToText($assessment->lack_confidence) ?></td>
                                        </tr>
                                        <tr>
                                            <th>I find it difficult to cope with my own problems?</th>
                                            <td><?php echo sliderValueToText($assessment->find_it_difficult_to_cope_with_my_own_problems) ?></td>
                                        </tr>
                                        <tr>
                                            <th>I feel gloomy about the future?</th>
                                            <td><?php echo sliderValueToText($assessment->feel_gloomy_about_the_future) ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <?php if(intToYesNo($assessment->receiving_specialist_mh_services) == 'Yes') { ?>
                                    <tr>
                                        <th>I am receiving support from?</th>
                                        <?php if($assessment->receiving_emotional_support == 'other') { ?>
                                            <td><?php echo $assessment->receiving_emotional_support_other; ?></td>
                                        <?php } else { ?>
                                            <td><?php echo $assessment->receiving_emotional_support; ?></td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                                <?php endif; ?>
                                <?php
                                if($provider_type == 'finance'): ///////////////// finance
                                    ?>
                                    <tr>
                                        <th>Score</th>
                                        <td><?php echo $scores->finance_score; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Are you worried about money?</th>
                                        <td><?php echo intToYesNo($assessment->are_you_in_debt); ?></td>
                                    </tr>
                                    <?php if($assessment->are_you_in_debt == '1') { ?>
                                    <tr>
                                        <th>I am concerned about managing my debts?</th>
                                        <td><?php echo sliderValueToText($assessment->concerned_about_managing_my_debts) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I worry about losing my home?</th>
                                        <td><?php echo sliderValueToText($assessment->worry_about_losing_my_home) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I struggle with paying bills?</th>
                                        <td><?php echo sliderValueToText($assessment->struggle_with_paying_bills) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I rarely have the money to do the things I really enjoy?</th>
                                        <td><?php echo sliderValueToText($assessment->often_worry_about_money) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I would like some advice on keeping my home warm</th>
                                        <td><?php echo intToYesNo($assessment->warm_home_advice) ?></td>
                                    </tr>
                                    <tr>
                                        <th>My ability to heat my home affects my family's health</th>
                                        <td><?php echo sliderValueToText($assessment->cold_home_affects_health) ?></td>
                                    </tr>
                                    <tr>
                                        <th>My home feels cold and draughty</th>
                                        <td><?php echo sliderValueToText($assessment->home_feels_cold) ?></td>
                                    </tr>
                                <?php } ?>
                                <?php endif; ?>
                                <?php
                                if($provider_type == 'weight'): ///////////////// weight
                                    ?>
                                    <tr>
                                        <th>Score</th>
                                        <td><?php echo $scores->weight_score; ?></td>
                                    </tr>
                                    <tr>
                                        <th>I am concerned about my weight?</th>
                                        <td><?php echo intToYesNo($assessment->concerns_regarding_weight) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I rarely exercise 30 mins + a day?</th>
                                        <td><?php echo sliderValueToText($assessment->rarely_exercise_30_mins_a_day) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I consider myself to be overweight?</th>
                                        <td><?php echo sliderValueToText($assessment->feel_overweight) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I am motivated to maintain a healthy weight?</th>
                                        <td><?php echo sliderValueToText($assessment->feel_motivated_to_maintain_a_healthy_weight) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I struggle to eat a healthy diet?</th>
                                        <td><?php echo sliderValueToText($assessment->struggle_to_eat_a_healthy_diet) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I don't get enough sleep?</th>
                                        <td><?php echo (empty($assessment->dont_worry_about_sleep)) ? "N/A" : sliderValueToText($assessment->dont_worry_about_sleep); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Height</th>
                                        <td><?php echo (empty($assessment->height_cm)) ? "N/A" : $assessment->height_cm . 'cm'; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Weight</th>
                                        <td><?php echo (empty($assessment->weight_kgs)) ? "N/A" : $assessment->weight_kgs . 'kg'; ?></td>
                                    </tr>
                                    <tr>
                                        <th>BMI</th>
                                        <td><?php echo (empty($assessment->bmi)) ? "N/A" : $assessment->bmi; ?></td>
                                    </tr>
                                    </tr>
                                <?php endif; ?>
                                <?php
                                if($provider_type == 'employment'): ///////////////// employment
                                    ?>
                                    <tr>
                                        <th>Score</th>
                                        <td><?php echo $scores->employment_score; ?></td>
                                    </tr>
                                    <tr>
                                        <th>I would be interested in volunteering opportunities?</th>
                                        <td><?php echo sliderValueToText($assessment->interested_in_volunteering) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I would be interested in adult learning courses?</th>
                                        <td><?php echo sliderValueToText($assessment->interested_in_adult_learning_courses) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I would be interested in learning new skills to improve my employability?</th>
                                        <td><?php echo sliderValueToText($assessment->interested_in_learning_new_skills) ?></td>
                                    </tr>
                                    <tr>
                                        <th>I would benefit from employment advice?</th>
                                        <td><?php echo sliderValueToText($assessment->benefit_from_employment_advise) ?></td>
                                    </tr>
                                <?php endif; ?>
                                <?php
                                if($provider_type == 'alcohol'): ///////////////// alcohol
                                    ?>
                                    <tr>
                                        <th>Score</th>
                                        <td><?php echo $scores->alcohol_score; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Do you drink alcohol?</th>
                                        <td><?php echo intToYesNo($assessment->drink_alcohol); ?></td>
                                    </tr>
                                    <?php
                                    if(intToYesNo($assessment->drink_alcohol) == 'Yes') {
                                        if( $assessment->alcohol_units == '' ) { // answers from oct 4th 2016 onwards
                                            ?>
                                            <tr>
                                                <th>How often do you have a drink containing alcohol?</th>
                                                <td><?php echo sliderValueToTextAlcoholFrequency($assessment->alcohol_how_often); ?></td>
                                            </tr>
                                            <tr>
                                                <th>How many units of alcohol do you drink on a typical day when you are drinking?</th>
                                                <td><?php echo sliderValueToTextAlcoholAmount($assessment->alcohol_units_per_day); ?></td>
                                            </tr>
                                            <tr>
                                                <th>How often have you had 6 or more units if female, or 8 or more if male, on a single occasion in the last year?</th>
                                                <td><?php echo sliderValueToTextAlcohol($assessment->alcohol_six_units_how_often); ?></td>
                                            </tr>
                                            <tr>
                                                <th>How often during the last year have you found that you were not able to stop drinking once you had started?</th>
                                                <td><?php echo sliderValueToTextAlcohol($assessment->unable_stop_drinking_how_often); ?></td>
                                            </tr>
                                            <tr>
                                                <th>How often during the last year have you failed to do what was normally expected from you because of your drinking?</th>
                                                <td><?php echo sliderValueToTextAlcohol($assessment->failed_due_to_drinking_how_often); ?></td>
                                            </tr>
                                            <tr>
                                                <th>How often during the last year have you needed an alcoholic drink in the morning to get yourself going after a heavy drinking session?</th>
                                                <td><?php echo sliderValueToTextAlcohol($assessment->hair_of_the_dog_how_often); ?></td>
                                            </tr>
                                            <tr>
                                                <th>How often during the last year have you had a feeling of guilt or remorse after drinking?</th>
                                                <td><?php echo sliderValueToTextAlcohol($assessment->remorse_after_drinking_how_often); ?></td>
                                            </tr>
                                            <tr>
                                                <th>How often during the last year have you been unable to remember what happened the night before because you had been drinking?</th>
                                                <td><?php echo sliderValueToTextAlcohol($assessment->amnesia_due_to_drinking_how_often); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Have you or somebody else been injured as a result of your drinking?</th>
                                                <td><?php echo sliderValueToTextIncidentAlcohol($assessment->injured_due_to_drinking); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Has a relative or friend, doctor or other health worker been concerned about your drinking or suggested that you cut down?</th>
                                                <td><?php echo sliderValueToTextIncidentAlcohol($assessment->suggested_cut_down_drinking); ?></td>
                                            </tr>
                                            <?php
                                        } else { // otherwise just show the alcohol_units answer
                                            ?>
                                            <tr>
                                                <th>How often have you had 6 or more units if female, or 8 or more if male, on a single occasion in the last year?</th>
                                                <td><?php echo sliderValueToTextAlcoholOld($assessment->alcohol_units); ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                <?php endif; ?>
                                <?php
                                if($provider_type == 'housing'): ///////////////// housing
                                    ?>
                                    <tr>
                                        <th>Score</th>
                                        <td><?php echo $scores->housing_score; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Housing Support Question</th>
                                        <td><?php echo $assessment->housing_needs; ?></td>
                                    </tr>

                                    <tr>
                                        <th>Housing Needs</th>
                                        <td><?php if($assessment->home_improvements == 1) {
                                                echo "Home improvements  ";
                                            }
                                            if(intToYesNo($assessment->housing_guidance) == 'Yes') {
                                                echo "Housing guidance  ";
                                            }
                                            if(intToYesNo($assessment->personal_care) == 'Yes') {
                                                echo "Personal care  ";
                                            } ?>
                                    </tr>
                                    <tr>
                                        <th>I consider myself to be at risk of falling?</th>
                                        <td><?php echo (empty($assessment->consider_myself_to_be_at_risk_of_falling)) ? "N/A" : sliderValueToText($assessment->consider_myself_to_be_at_risk_of_falling); ?></td>
                                    </tr>
                                    <tr>
                                        <th>I do not feel independent in my home?</th>
                                        <td><?php echo (empty($assessment->do_not_feel_independent_in_my_home)) ? "N/A" : sliderValueToText($assessment->do_not_feel_independent_in_my_home); ?></td>
                                    </tr>
                                    <tr>
                                        <th>I would benefit from technologies to help me stay independent at home?</th>
                                        <td><?php echo (empty($assessment->benefit_from_technologies_to_help_me_stay_independent_at_home)) ? "N/A" : sliderValueToText($assessment->benefit_from_technologies_to_help_me_stay_independent_at_home); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Is there anything else you wish to tell us at this time?</th>
                                        <td><?php echo nl2br($assessment->anything_else); ?></td>
                                    </tr>
                                <?php endif; ?>
                                <?php
                                if($provider_type == 'emotional'): ///////////////// emotional
                                    ?>
                                    <tr>
                                        <th>Score</th>
                                        <td><?php echo $scores->emotional_score; ?></td>
                                    </tr>
                                    <tr>
                                        <th>I've been feeling optimistic about the future ?</th>
                                        <td><?php echo sliderValueToFrequency($assessment->feeling_optimistic_about_the_future); ?></td>
                                    </tr>
                                    <tr>
                                        <th>I've been feeling useful ?</th>
                                        <td><?php echo sliderValueToFrequency($assessment->feeling_useful); ?></td>
                                    </tr>
                                    <tr>
                                        <th>I've been feeling relaxed ?</th>
                                        <td><?php echo sliderValueToFrequency($assessment->feeling_relaxed); ?></td>
                                    </tr>
                                    <tr>
                                        <th>I've been dealing with problems well ?</th>
                                        <td><?php echo sliderValueToFrequency($assessment->dealing_with_problems_well); ?></td>
                                    </tr>
                                    <tr>
                                        <th>I've been thinking clearly ?</th>
                                        <td><?php echo sliderValueToFrequency($assessment->thinking_clearly); ?></td>
                                    </tr>
                                    <tr>
                                        <th>I've been feeling close to other people ?</th>
                                        <td><?php echo sliderValueToFrequency($assessment->feeling_close_to_other_people); ?></td>
                                    </tr>
                                    <tr>
                                        <th>I've been able to make up my own mind about things ?</th>
                                        <td><?php echo sliderValueToFrequency($assessment->able_to_make_up_my_own_mind_about_things); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Are you currently receiving any specialist mental health services ?</th>
                                        <td><?php echo intToYesNo($assessment->receiving_specialist_mh_services); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Receiving emotional support from</th>
                                        <td><?php echo nl2br($assessment->receiving_emotional_support); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Emotional support other</th>
                                        <td><?php echo nl2br($assessment->receiving_emotional_support_other); ?></td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <th>Customer personal notes</th>
                                    <td><?php echo htmlentities($personal_note->notes); ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="accordion-btn">
                        <div class="accordion-btn-bckgnd"></div>
                        <div class="label-priority label-priority-green text-center">&nbsp;</div>
                        <span>Customer assessment notes</span>
                    </div>
                    <div class="accordion-content" style="display: none">
                        <div class="result-text">
                            <?php
                            if( $customer_notes !== false ) {
                                echo '<table width="100%" class="table table-fixed">';
                                echo '<tr style="font-weight:bold;"><td width="20%">Date</td><td width="20%">Time</td><td width="20%">Assessor (ID)</td><td width="20%">Contact type</td><td width="20%">Action</td></tr>';
                                foreach($customer_notes as $key=>$row) {
                                    echo '<tr><td colspan="5">';
                                    echo '<table width="100%"><tr>';
                                    echo '<td width="20%" style="padding-bottom:15px;">' . $row->datepart . '</td>';
                                    echo '<td width="20%">' . $row->timepart . '</td>';
                                    echo '<td width="20%">' . htmlentities($row->display_name) . " ($row->user_id)" . '</td>';
                                    echo '<td width="20%">' . htmlentities($row->type_of_contact) . '</td>';
                                    echo '<td width="20%">' . htmlentities($row->followup_action) . '</td></tr>';
                                    echo '<tr><td colspan="5" style="padding-bottom:15px;">' . nl2br(htmlentities($row->notes)) . '</td></tr>';
                                    echo '</table>';
                                    echo '</td></tr>';
                                }
                                echo'</table>';
                            }
                            ?>
                        </div>
                    </div>
                </li>
            </ul>
            <?php
            $rc = $assessment;
            $fields = $controller->get_provider_form_fields($customer_id);
            $assessments = $controller->get_provider_data();
            if(!empty($provider_data)) {
                ?>
                <h2 style="font-size:2rem; text-align:left; font-weight:normal; margin-top:2em;">Previous Entries</h2>
                <ul class="accordion">
                    <?php
                    $fields = $controller->get_provider_form_fields(0); // so we can determine and encrypt textareas
                    $fields2 = [];
                    foreach($fields as $k => $field) {
                        $fields2[$field[0]] = $k;
                    }
                    foreach($provider_data as $assessment){
                        ?>
                        <li class="clearfix" style="padding:0;">
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <div class="label-priority label-priority-1 text-center">&nbsp;</div>
                                <?php
                                // find contact date
                                foreach($assessment as $k => $v) {
                                    if($v['keyname'] == 'specialist_contact_date') {
                                        $date = $v['keyvalue'];
                                        break;
                                    }
                                }
                                ?>
                                <span>Entry: <?php echo $date; ?></span>
                            </div>
                            <div class="accordion-content" style="display: none">
                                <table class="table">
                                    <?php foreach($assessment as $key => $value): ?>
                                        <?php if($fields2[$value['keyname']] == 'User ID'){ continue; } ?>
                                        <?php if($value['keyname'] == 'action'){ continue; } ?>
                                        <?php if(in_array($value['keyname'], ['referral_date', 'specialist_contact_date', 'quit_date', '12_week_date', '26_week_date', '52_week_date'])) {
                                            $value['keyvalue'] =  date('d/m/Y', strtotime($value['keyvalue']));

                                } ?>
                                        <tr>
                                            <th><?php echo $fields2[$value['keyname']]; ?></th>
                                            <td><?php echo $value['keyvalue']?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>
            <ul class="accordion" style="margin-top:2em;">
                <li style="padding: 0;">
                    <div class="accordion-btn">
                        <div class="accordion-btn-bckgnd"></div>
                        <div class="label-priority label-priority-3 text-center">&nbsp;</div>
                        <span><strong>Add:</strong> Provider Entry</span>
                    </div>
                    <div class="accordion-content" style="display: none;" class="LALALALALALLA">
                        <form id="" action="/providers/add_data/<?php echo $rc['id'] . '/' . $customer_id;?>" method="post" class="form" >
                            <input name="action" value="add_provider_data" type="hidden">
                            <input type="hidden" name="assessment_id" value="<?php echo $response_collection['id'];?>" />

                            <?php
                            // output form
                            foreach($fields as $k => $field) {
                                echo get_form_field([$k => $field], (count($assessments) > 1 ? 'second' : 'first'));
                            }
                            ?>

                            <button type="submit" class="button button-green button-lrg" style="clear: both;">Save Entry</button>
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready( function() {
        // save some info about select elements
         var selecth = $('select').height();
         var selectoh = $('select').outerHeight();
         var sdone = false;

        // Make 'please select' unselectable after something else selected in a dropdown
        $('select').on('change', function(e) {
            $(this).find('.please-select').remove();
        });

        // Expand select on focus
        $('select').on('focus', function(e) {
            if(!sdone) {
                selecth = $('select').height();
                selectoh = $('select').outerHeight();
                sdone = true;
            }
            var count = $(this).find('option').length;
            $(this).css('height', selectoh);
            $(this).attr('size', count);
            var tweak = 1;
            if(navigator.userAgent.indexOf('Edge') != -1) {
              tweak = 6; // Edge kludge. MS doesn't seem to know the real height of its options. Quel surprise.
              }
            $(this).animate( { 'height' : selectoh + ($(this).height() + tweak) * (count - 1) }, 500 );
        });

        // Collapse select on change
        $('select').on('change', function(e) {
            $(this).animate( { 'height' : selectoh }, 500, 'swing', function() {
                $(this).attr('size', 1).css('height', 'auto').blur();
            } );
        });


        // Collapse select on blur
            $('select').on('blur', function(e) {
            $(this).animate( { 'height' : selectoh }, 500, 'swing', function() {
              $(this).attr('size', 1).css('height', 'auto');
            } );
        });
    })


</script>

<?php

View::element('jsuistuff', [], 'firstforwellbeing');


//////////////////////////////////////// functions

function intToYesNo($i) {
    return ($i == 1 ? 'Yes' : 'No' );
}

function get_form_field($field, $filter) {
    foreach($field as $label => $field) {
        if(isset($field[2]) and $field[2] != $filter) { continue; }
        $name = $field[0];
        $type = $field[1];
        $default = isset($field[3]) ? $field[3] : '';

        switch($field[1]) {
            case 'text':
                $html = '
                    <div class="form-group input-wrapper input-wrapper-sml clearfix">
                        <label class="control-label">' . $label . '</label>
                    <div>
                        <input type="text" style="font-size:18px;" name="' . $name . '" placeholder="' . $label . '">
                    </div>
                    </div>';
                break;
            case 'boolean':
                $html = '            
                    <div class="form-group input-wrapper input-wrapper-sml clearfix">
                        <label class="control-label">' . $label . '</label>
                        <div>
                            <select style="font-size:18px;" name="' . $name . '">
                                <option>Please select</option>
                                <option value="1">Yes</option>
                                <option value="2">No</option>
                            </select>
                        </div>
                    </div>';
                break;
            case 'date':
                $html = '
                    <div class="form-group input-wrapper input-wrapper-sml clearfix">
                        <label class="control-label">' . $label . '</label>
                        <div>
                            <input style="font-size:18px;" type="date" name="' . $name . '" rel="date-picker">
                        </div>
                    </div>';
                break;
            case 'hidden':
                $html = '<input type="hidden" name="' . $name . '" value="' . $default . '">';
                break;
            case 'select':
                $html = '
                    <div class="form-group input-wrapper input-wrapper-sml clearfix">
                        <label class="control-label">' . $label . '</label>
                        <select style="font-size:18px;" name="' . $name . '">';
                foreach($field[4] as $k => $v) {
                    $html .= '<option value="' . $v . '">' . htmlentities($v) . '</options>';
                }
                $html .= '</select>
                    </div>';
                break;
            case 'textarea':
                $html = '
                    <div class="form-group input-wrapper input-wrapper-sml clearfix">
                        <label class="control-label">' . $label . '</label>
                        <div class="form-control textarea">
                            <textarea style="font-size:18px;" name="' . $name . '" placeholder="' . $label . '" rows="10"></textarea>
                        </div>
                    </div>';
                break;
            default:
                break;
        }
    }
    return $html;
}