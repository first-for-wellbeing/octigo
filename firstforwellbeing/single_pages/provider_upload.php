<div id="content" class="container">
    <div class="row">

        <section class="section col-md-12">
            <div class="wysiwyg">

                <?php
                if( $upload_done ) {
                    ?>

                    <h1>Your data has been uploaded and added to your customers' records</h1>

                    <?php
                } else {
                    ?>

                    <h1>Upload provider data to customer records</h1>
                    <div class="row">
                        <div class="col-md-12">
                            Please take a moment to check the following points before uploading your data:
                            <ul>
                                <li>All dates are in the format DD/MM/YYYY</li>
                                <li>All numeric values are integers with no decimal part</li>
                                <li>Each row in your CSV file is intended to be <i>added</i> to your customer's data. You cannot update information by uploading a CSV</li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <form method="post" action="doupload" enctype="multipart/form-data">
                                <div class="col-md-6">
                                    <label for="upload-button" id="upload-label">
                                        <button type="button" class="button button-green provider-import" onclick="jQuery('#upload-label').click();" id="import-button">Choose CSV file</button>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <input type="file" name="data" id="upload-button" accept=".csv" style="display:none;" onchange="jQuery('#go-button').show(); jQuery('#go-button').text('Upload file ' + jQuery('#upload-button').val().replace(/.*(\/|\\)/, '') );" />
                                    <button type="submit" class="button button-green provider-import" id="go-button" style="display:none;">Upload CSV</button><br />
                                </div>
                            </form>
                        </div>
                    </div>

                    <?php
                }
                ?>
            </div>
        </section>

    </div>
</div>
