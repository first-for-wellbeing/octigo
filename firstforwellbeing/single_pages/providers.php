<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<div id="content" class="container">
    <div class="row">

        <section class="section col-md-12">

            <div class="wysiwyg">
                <h1 style="font-size:3.25rem; text-align:left; font-weight:normal;">Welcome <?php echo $displayname; ?></h1>
                <div class="filter-assessment-results">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 style="font-size:2rem; text-align:left; font-weight:normal;">Filter Results</h2>
                        </div>
                    </div>
                    <form method="post" action="" class="js-filter-entries">
                        <div class="row">
                            <div class="col-md-2">
                                <div >
                                    <input type="text" name="user_id" placeholder="ID" title="User ID" style="font-size:18px" <?php echo 'value="' . htmlentities($_POST['user_id']) . '"'; ?>>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div>
                                    <input type="text" name="first_name" placeholder="First Name" title="First Name" style="font-size:18px" <?php echo 'value="' . htmlentities($_POST['first_name']) . '"'; ?>>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div>
                                    <input type="text" name="last_name" placeholder="Last Name" title="Last Name" style="font-size:18px" <?php echo 'value="' . htmlentities($_POST['last_name']) . '"'; ?>>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="button button-black" style="height:56px;line-height:56px;width:100%">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-responsive js-filter-entries-results">
                    <?php if(count($customers) > 0) { ?>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Customer ID</td>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Notes added</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach($customers as $customer){
                                // check filter
                                if(!empty($_POST['user_id'])) {
                                    if($customer['user_id'] != $_POST['user_id']) { continue; } // skip record
                                }
                                if(!empty($_POST['first_name'])) {
                                    if(stripos($customer['forename'], $_POST['first_name']) === false) { continue; } // skip record
                                }
                                if(!empty($_POST['last_name'])) {
                                    if(stripos($customer['surname'], $_POST['last_name']) === false) { continue; } // skip record
                                }

                                ?>
                                <tr data-first-name="<?php echo $customer['forename']; ?>" data-last-name="<?php echo $customer['surname']; ?>">
                                    <td><?php echo $customer['user_id']; ?></td>
                                    <td><?php echo $customer['forename']; ?></td>
                                    <td><?php echo $customer['surname']; ?></td>
                                    <td><?php echo $customer['notes_count']; ?></td>
                                    <td class="text-right">
                                        <a href="/providers/entry/<?php echo $customer['user_id']; ?>" title="View"><strong>View</strong></a>
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                        </tbody>
                    </table>
                    <?php } else {
                        echo'You currently have no customer entries to display';
                    } ?>
                </div>
                <div class="row">
                    <div class="col-md-2"><a href="/providers/export"><button type="button" class="button button-green provider-export">Export</button></a></div>
                    <div class="col-md-4 col-md-offset-2 text-center"><a href="/providers/template"><button type="button" class="button button-green provider-template">Download Import Template</button></a></div>
                    <div class="col-md-2 col-md-offset-2 text-right">
                        <a href="/providers/upload" class="button button-green">Upload CSV</a>
                    </div>
                </div>
        </section>

    </div>
</div>
