<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>
<div id="content">
    <div class="container">
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-green underline">User profile</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <ul class="accordion">
                        <li><!-- contact details -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <span>Contact details</span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed" style="font-family:museo-sans-rounded, sans-serif">
                                        <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <td>
                                                <span class="data"><img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo $user_data['forename'] . ' ' .$user_data['surname'] ?></span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:35%;">First name:</div> <input type="text" name="forename" class="edit-input" placeholder="Your first name" value="<?php echo $user_data['forename']; ?>" /><br />
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:35%;">Last name:</div> <input type="text" name="surname" class="edit-input" placeholder="Your last name" value="<?php echo $user_data['surname']; ?>" />
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo $user_data['email']; ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:35%;">Email:<br /> <span style="color:red; font-weight:bold;">NB This will NOT change your user login</span></div> <input type="text" name="email" class="edit-input" placeholder="Your email address" value="<?php echo $user_data['email']; ?>" />
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Address</th>
                                            <td>
                                                <span class="data"><img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo $user_data['address1']; ?>,<?php echo $user_data['address2']; ?>, <?php echo $user_data['town']; ?>, <?php echo $user_data['county']; ?> <?php echo $user_data['postcode']; ?></span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:35%;">Address line 1:</div> <input type="text" name="address1" class="edit-input" placeholder="Line 1 of your address" value="<?php echo $user_data['address1']; ?>" />
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:35%;">Address line 2:</div> <input type="text" name="address2" class="edit-input" placeholder="Line 2 of your address" value="<?php echo $user_data['address2']; ?>" />
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:35%;">Town:</div> <input type="text" name="town" class="edit-input" placeholder="Town" value="<?php echo $user_data['town']; ?>" />
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:35%;">County:</div> <input type="text" name="county" class="edit-input" placeholder="County" value="<?php echo $user_data['county']; ?>" />
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:35%;">Postcode:</div> <input type="text" name="postcode" class="edit-input" placeholder="Postcode" value="<?php echo $user_data['postcode']; ?>" />
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Telephone number</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo $user_data['tel1']; ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:35%;">Telephone number:</div> <input type="text" name="tel1" class="edit-input" placeholder="Your telephone number" value="<?php echo $user_data['tel1']; ?>" />
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Mobile</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo $user_data['tel2']; ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:35%;">Mobile number:</div> <input type="text" name="tel2" class="edit-input" placeholder="Your mobile number" value="<?php echo $user_data['tel2']; ?>" />
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Preferred method of contact</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php if( $user_data['contact_method'] == 1) echo 'Email'; else if( $user_data['contact_method'] == 2) echo 'Telephone'; else echo 'Post'; ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">Preferred method of contact:</div><input type="radio" name="contact_method" id="contact_method1" class="edit-input" style="-webkit-appearance:radio;" placeholder="Your mobile number" value="1" <?php echo ($user_data['contact_method']==1?'checked':''); ?> /> <label for="contact_method1">Email</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="contact_method" id="contact_method2" class="edit-input" style="-webkit-appearance:radio;" placeholder="Your mobile number" value="2" <?php echo ($user_data['contact_method']==2?'checked':''); ?> /> <label for="contact_method2">Telephone</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="contact_method" id="contact_method3" class="edit-input" style="-webkit-appearance:radio;" placeholder="Your mobile number" value="3" <?php echo ($user_data['contact_method']==3?'checked':''); ?> /> <label for="contact_method3">Post</label>
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li style="margin-bottom:50px;"><!-- customer info -->
                            <div class="accordion-btn">
                                <div class="accordion-btn-bckgnd"></div>
                                <span>Additional info</span>
                            </div>
                            <div class="accordion-content">
                                <div class="table-responsive">
                                    <table class="table table-fixed">
                                        <tbody>
                                        <tr>
                                            <th>NHS number</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo $user_data['nhsnumber']; ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:35%;">NHS number:</div> <input type="text" name="nhsnumber" class="edit-input" placeholder="Your NHS number" value="<?php echo $user_data['nhsnumber']; ?>" />
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Date of birth</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo $user_data['dob']; ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:35%;">Date of birth DD/MM/YYYY:</div> <input type="text" name="dob" class="edit-input" placeholder="Your date of birth" value="<?php echo $user_data['dob']; ?>" />
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Have you had a health check in last 4 years?</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo intToYesNo($user_data['healthcheck']); ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">Have you had a health check in the last 4 years?:</div><input type="radio" name="healthcheck" id="healthcheck1" class="edit-input" style="-webkit-appearance:radio;" value="1" <?php echo ($user_data['healthcheck']==1?'checked':''); ?> /> <label for="healthcheck1">Yes</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="healthcheck" name="healthcheck2" class="edit-input" style="-webkit-appearance:radio;" value="2" <?php echo ($user_data['healthcheck']!=1?'checked':''); ?> /> <label for="healthcheck2">No</label>
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Gender</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo $user_data['gender']; ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">Gender:</div><input type="radio" name="gender" id="gender1" class="edit-input" style="-webkit-appearance:radio;" value="Male" <?php echo ($user_data['gender']=='Male'?'checked':''); ?> /> <label for="gender1">Male</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="gender" id="gender2" class="edit-input" style="-webkit-appearance:radio;" value="Female" <?php echo ($user_data['gender']=='Female'?'checked':''); ?> /> <label for="gender2">Female</label>
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <?php
                                        if($user_data['gender'] == 'Female') {
                                            $pregnant_style = '';
                                        } else {
                                            $pregnant_style = 'display:none;';
                                        }
                                        ?>
                                        <tr class="pregnant" styel="<?php echo $pregnant_style; ?>">
                                            <th>Pregnant?</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo intToYesNo($user_data['pregnant']); ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">Are you pregnant?:</div><input type="radio" name="pregnant" id="pregnant1" class="edit-input" style="-webkit-appearance:radio;" value="1" <?php echo ($user_data['pregnant']==1?'checked':''); ?> /> <label for="pregnant1">Yes</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="pregnant" id="pregnant2" class="edit-input" style="-webkit-appearance:radio;" value="2" <?php echo ($user_data['pregnant']==2?'checked':''); ?> /> <label for="pregnant2">No</label>
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Religion</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo ($user_data['religion'] == 'Other') ? $user_data['religion_other'] : $user_data['religion']; ?>
													</span>
                                                <span class="edit religion" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">Religion:</div><input type="radio" name="religion" id="religion1" class="edit-input" style="-webkit-appearance:radio;" value="Christian" <?php echo ($user_data['religion']=='Christian'?'checked':''); ?> /> <label for="religion1">Christian</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="religion" id="religion2" class="edit-input" style="-webkit-appearance:radio;" value="Hindu" <?php echo ($user_data['religion']=='Hindu'?'checked':''); ?> /> <label for="religion2">Hindu</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="religion" id="religion3" class="edit-input" style="-webkit-appearance:radio;" value="Jewish" <?php echo ($user_data['religion']=='Jewish'?'checked':''); ?> /> <label for="religion3">Jewish</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="religion" id="religion4" class="edit-input" style="-webkit-appearance:radio;" value="Muslim" <?php echo ($user_data['religion']=='Muslim'?'checked':''); ?> /> <label for="religion4">Muslim</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="religion" id="religion5" class="edit-input" style="-webkit-appearance:radio;" value="Sikh" <?php echo ($user_data['religion']=='Sikh'?'checked':''); ?> /> <label for="religion5">Sikh</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="religion" id="religion6" class="edit-input" style="-webkit-appearance:radio;" value="Bhuddist" <?php echo ($user_data['religion']=='Bhuddist'?'checked':''); ?> /> <label for="religion6">Bhuddist</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="religion" id="religion7" class="edit-input" style="-webkit-appearance:radio;" value="None" <?php echo ($user_data['religion']=='None'?'checked':''); ?> /> <label for="religion7">None</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="religion" id="religion8" class="edit-input" style="-webkit-appearance:radio;" value="Other" <?php echo ($user_data['religion']=='Other'?'checked':''); ?> /> <label for="religion8">Other</label>
														</div>
                                                    <?php
                                                    if($user_data['religion']=='Other') {
                                                        $religion_other_style = '';
                                                    } else {
                                                        $religion_other_style = 'display:none;';
                                                    }
                                                    ?>
                                                    <div style="margin-bottom:10px;">
															<div class="religion-other" style="<?php echo $religion_other_style;?>"><div style="display:inline-block; width:60%;">&nbsp;</div><input type="text" name="religion_other" class="edit-input" placeholder="Your religion" value="<?php echo $user_data['religion_other']; ?>" /></div>
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Marital status</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo ($user_data['maritalstatus'] == 'Other') ? $user_data['maritalstatus_other'] : $user_data['maritalstatus']; ?>
													</span>
                                                <span class="edit marital" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">Marital status:</div><input type="radio" name="maritalstatus" id="maritalstatus1" class="edit-input" style="-webkit-appearance:radio;" value="Single" <?php echo ($user_data['marital_status']=='Single'?'checked':''); ?> /> <label for="maritalstatus1">Single</label><br />
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="maritalstatus" id="marital_status2" class="edit-input" style="-webkit-appearance:radio;" value="Married" <?php echo ($user_data['marital_status']=='Married'?'checked':''); ?> /> <label for="maritalstatus2">Married</label><br />
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="maritalstatus" id="maritalstatus3" class="edit-input" style="-webkit-appearance:radio;" value="Civil partnership" <?php echo ($user_data['marital_status']=='Civil partnership'?'checked':''); ?> /> <label for="maritalstatus3">Civil partnership</label><br />
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="maritalstatus" id="maritalstatus4" class="edit-input" style="-webkit-appearance:radio;" value="Widowed" <?php echo ($user_data['maritalstatus']=='Widowed'?'checked':''); ?> /> <label for="maritalstatus4">Widowed</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="maritalstatus" id="maritalstatus5" class="edit-input" style="-webkit-appearance:radio;" value="Divorced" <?php echo ($user_data['maritalstatus']=='Divorced'?'checked':''); ?> /> <label for="maritalstatus5">Divorced</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="maritalstatus" id="maritalstatus6" class="edit-input" style="-webkit-appearance:radio;" value="Separated" <?php echo ($user_data['maritalstatus']=='Separated'?'checked':''); ?> /> <label for="maritalstatus6">Separated</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="maritalstatus" id="maritalstatus7" class="edit-input" style="-webkit-appearance:radio;" value="Other" <?php echo ($user_data['maritalstatus']=='Other'?'checked':''); ?> /> <label for="maritalstatus7">Other</label>
														</div>
                                                    <?php
                                                    if($user_data['maritalstatus']=='Other') {
                                                        $marital_style = '';
                                                    } else {
                                                        $marital_style = 'display:none;';
                                                    }
                                                    ?>
                                                    <div style="margin-bottom:10px;">
															<div class="marital-other" style="<?php echo $marital_style;?>"><div style="display:inline-block; width:60%;">&nbsp;</div><input type="text" name="maritalstatus_other" class="edit-input" placeholder="Your marital status" value="<?php echo $user_data['maritalstatus_other']; ?>" /></div>
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Parental status</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo $user_data['parentalstatus']; ?>
													</span>
                                                <span class="edit marital" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">Parental status:</div><input type="radio" name="parentalstatus" id="parentalstatus1" class="edit-input" style="-webkit-appearance:radio;" value="Parent" <?php echo ($user_data['parentalstatus']=='Parent'?'checked':''); ?> /> <label for="parentalstatus1">Parent</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="parentalstatus" id="parentalstatus2" class="edit-input" style="-webkit-appearance:radio;" value="Carer" <?php echo ($user_data['parentalstatus']=='Carer'?'checked':''); ?> /> <label for="parentalstatus2">Carer</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="parentalstatus" id="parentalstatus3" class="edit-input" style="-webkit-appearance:radio;" value="Both of the above" <?php echo ($user_data['parentalstatus']=='Both of the above'?'checked':''); ?> /> <label for="parentalstatus3">Both of the above</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="parentalstatus" id="parentalstatus4" class="edit-input" style="-webkit-appearance:radio;" value="None of the above" <?php echo ($user_data['parentalstatus']=='None of the above'?'checked':''); ?> /> <label for="parentalstatus4">None of the above</label>
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Number of Dependants</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo $user_data['dependants']; ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">Parental status:</div><input type="radio" name="dependants" id="dependants1" class="edit-input" style="-webkit-appearance:radio;" value="0" <?php echo ($user_data['dependants']=='0'?'checked':''); ?> /> <label for="dependants1">0</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="dependants" id="dependants2" class="edit-input" style="-webkit-appearance:radio;" value="1" <?php echo ($user_data['dependants']=='1'?'checked':''); ?> /> <label for="dependants2">1</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="dependants" id="dependants3" class="edit-input" style="-webkit-appearance:radio;" value="2" <?php echo ($user_data['dependants']=='2'?'checked':''); ?> /> <label for="dependants3">2</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="dependants" id="dependants4" class="edit-input" style="-webkit-appearance:radio;" value="3" <?php echo ($user_data['dependants']=='3'?'checked':''); ?> /> <label for="dependants4">3</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="dependants" id="dependants5" class="edit-input" style="-webkit-appearance:radio;" value="4" <?php echo ($user_data['dependants']=='4'?'checked':''); ?> /> <label for="dependants5">4</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:60%;">&nbsp;</div><input type="radio" name="dependants" id="dependants6" class="edit-input" style="-webkit-appearance:radio;" value="5 or more" <?php echo ($user_data['dependants']=='5 or more'?'checked':''); ?> /> <label for="dependants6">5 or more</label>
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Disability Status</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo ($user_data['disabilitystatus'] == 'Other') ? $user_data['disabilitystatus_other'] : (empty($user_data['disabilitystatus']) || $user_data['disabilitystatus'] == "error" ? "N/A" : $user_data['disabilitystatus']) ; ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:30%;">Disability status:</div><input type="radio" name="disabilitystatus" id="disabilitystatus1" class="edit-input" style="-webkit-appearance:radio;" value="Physical disability/Impairment" <?php echo ($user_data['disabilitystatus']=='Physical disability/Impairment'?'checked':''); ?> /> <label for="disabilitystatus1">Physical disability/Impairment</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:30%;">&nbsp;</div><input type="radio" name="disabilitystatus" id="disabilitystatus2" class="edit-input" style="-webkit-appearance:radio;" value="Mental health condition" <?php echo ($user_data['disability_status']=='Mental health condition'?'checked':''); ?> /> <label for="disabilitystatus2">Mental health condition</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:30%;">&nbsp;</div><input type="radio" name="disabilitystatus" id="disabilitystatus3" class="edit-input" style="-webkit-appearance:radio;" value="Long term Illness" <?php echo ($user_data['disabilitystatus']=='Long term Illness'?'checked':''); ?> /> <label for="disabilitystatus3">Long term Illness</label>
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Ethnic origin</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo ($user_data['ethnicorigin'] == 'Other') ? $user_data['ethnicorigin_white_other'] : $user_data['ethnicorigin']; ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">Ethnic<br />origin:</div><input type="radio" name="ethnicorigin" id="ethnicorigin1" class="edit-input" style="-webkit-appearance:radio;" value="White English" <?php echo ($user_data['ethnicorigin']=='White English'?'checked':''); ?> /> <label for="ethnicorigin1">White English</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin2" class="edit-input" style="-webkit-appearance:radio;" value="White Welsh" <?php echo ($user_data['ethnicorigin']=='White Welsh'?'checked':''); ?> /> <label for="ethnicorigin2">White Welsh</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin3" class="edit-input" style="-webkit-appearance:radio;" value="White Scottish" <?php echo ($user_data['ethnicorigin']=='White Scottish'?'checked':''); ?> /> <label for="ethnicorigin3">White Scottish</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin4" class="edit-input" style="-webkit-appearance:radio;" value="White Irish" <?php echo ($user_data['ethnicorigin']=='White Irish'?'checked':''); ?> /> <label for="ethnicorigin4">White Irish</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin5" class="edit-input" style="-webkit-appearance:radio;" value="White Northern Irish" <?php echo ($user_data['ethnicorigin']=='White Northern Irish'?'checked':''); ?> /> <label for="ethnicorigin5">White Northern Irish</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin6" class="edit-input" style="-webkit-appearance:radio;" value="Asian or Asian British Indian" <?php echo ($user_data['ethnicorigin']=='Asian or Asian British Indian'?'checked':''); ?> /> <label for="ethnicorigin6">Asian or Asian British Indian</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin7" class="edit-input" style="-webkit-appearance:radio;" value="Asian or Asian British Bangladeshi" <?php echo ($user_data['ethnicorigin']=='Asian or Asian British Bangladeshi'?'checked':''); ?> /> <label for="ethnicorigin7">Asian or Asian British Bangladeshi</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin8" class="edit-input" style="-webkit-appearance:radio;" value="Asian or Asian British Pakistani" <?php echo ($user_data['ethnicorigin']=='Asian or Asian British Pakistani'?'checked':''); ?> /> <label for="ethnicorigin8">Asian or Asian British Pakistani</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin9" class="edit-input" style="-webkit-appearance:radio;" value="Mixed / Multiple ethnic White / Black Caribbean" <?php echo ($user_data['ethnicorigin']=='Mixed / Multiple ethnic White / Black Caribbean'?'checked':''); ?> /> <label for="ethnicorigin9">Mixed / Multiple ethnic White / Black Caribbean</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin10" class="edit-input" style="-webkit-appearance:radio;" value="Mixed / Multiple ethnic White / Black African" <?php echo ($user_data['ethnicorigin']=='Mixed / Multiple ethnic White / Black African'?'checked':''); ?> /> <label for="ethnicorigin10">Mixed / Multiple ethnic White / Black African</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin11" class="edit-input" style="-webkit-appearance:radio;" value="Mixed / Multiple ethnic White / Asian" <?php echo ($user_data['ethnicorigin']=='Mixed / Multiple ethnic White / Asian'?'checked':''); ?> /> <label for="ethnicorigin11">Mixed / Multiple ethnic White / Asian</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin12" class="edit-input" style="-webkit-appearance:radio;" value="Black or Black British Caribbean" <?php echo ($user_data['ethnicorigin']=='Black or Black British Caribbean'?'checked':''); ?> /> <label for="ethnicorigin12">Black or Black British Caribbean</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin13" class="edit-input" style="-webkit-appearance:radio;" value="Black or Black British African" <?php echo ($user_data['ethnicorigin']=='Black or Black British African'?'checked':''); ?> /> <label for="ethnicorigin13">Black or Black British African</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="ethnicorigin" id="ethnicorigin14" class="edit-input" style="-webkit-appearance:radio;" value="Other" <?php echo ($user_data['ethnicorigin']=='Other'?'checked':''); ?> /> <label for="ethnicorigin14">Other</label>
														</div>
                                                    <?php
                                                    if($user_data['ethnicorigin']=='Other') {
                                                        $ethnic_style = '';
                                                    } else {
                                                        $ethnic_style = 'display:none;';
                                                    }
                                                    ?>
                                                    <div style="margin-bottom:10px;">
															<div class="ethnic-other" style="<?php echo $ethnic_style;?>"><div style="display:inline-block; width:60%;">&nbsp;</div><input type="text" name="ethnicorigin_white_other" class="edit-input" placeholder="Your ethnic origin" value="<?php echo $user_data['ethnicorigin_white_other']; ?>" /></div>
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Sexuality</th>
                                            <td>
													<span class="data">
														<img src="/packages/firstforwellbeing/themes/ffw_assessment/images/edit_icon.png" class="edit-button" style="float:right; width:16px; height:auto; margin-left:1em;"/>&nbsp;<?php echo ($user_data['sexuality'] == 'Other') ? $user_data['sexuality_other'] : (empty($user_data['sexuality']) || $user_data['sexuality'] == "error" ? "N/A" : $user_data['sexuality']) ; ?>
													</span>
                                                <span class="edit" style="display:none;">
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">Sexuality:</div><input type="radio" name="sexuality" id="sexuality1" class="edit-input" style="-webkit-appearance:radio;" value="Heterosexual" <?php echo ($user_data['sexuality']=='Heterosexual'?'checked':''); ?> /> <label for="sexuality1">Heterosexual</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="sexuality" id="sexuality2" class="edit-input" style="-webkit-appearance:radio;" value="Bisexual" <?php echo ($user_data['sexuality']=='Bisexual'?'checked':''); ?> /> <label for="sexuality2">Bisexual</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="sexuality" id="sexuality3" class="edit-input" style="-webkit-appearance:radio;" value="Homosexual" <?php echo ($user_data['sexuality']=='Homosexual'?'checked':''); ?> /> <label for="sexuality3">Homosexual</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="sexuality" id="sexuality4" class="edit-input" style="-webkit-appearance:radio;" value="I prefer not to say" <?php echo ($user_data['sexuality']=='I prefer not to say'?'checked':''); ?> /> <label for="sexuality4">I prefer not to say</label>
														</div>
														<div style="margin-bottom:10px;">
															<div style="display:inline-block; width:15%;">&nbsp;</div><input type="radio" name="sexuality" id="sexuality5" class="edit-input" style="-webkit-appearance:radio;" value="Other" <?php echo ($user_data['sexuality']=='Other'?'checked':''); ?> /> <label for="sexuality5">Other</label>
														</div>
                                                    <?php
                                                    if($user_data['sexuality']=='Other') {
                                                        $sexuality_style = '';
                                                    } else {
                                                        $sexuality_style = 'display:none;';
                                                    }
                                                    ?>
                                                    <div style="margin-bottom:10px;">
															<div class="sexuality-other" style="<?php echo $sexuality_style;?>"><div style="display:inline-block; width:60%;">&nbsp;</div><input type="text" name="sexuality_other" class="edit-input" placeholder="Your sexuality" value="<?php echo $user_data['sexuality_other']; ?>" /></div>
														</div>
														<div style="margin-bottom:10px;">
															<button type="button" style="border-radius:5px; background-color:#008800; color:white;" class="update">Update</button>&nbsp;&nbsp;&nbsp;
															<button type="button" style="border-radius:5px; background-color:#880000; color:white;" class="cancel">Cancel</button>
														</div>
													</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>


        </section>
    </div>
</div>
<input type="hidden" name="custid" id="custid" value="<?php echo $custid ?>" />

<?php
View::element('jsuserprofile', [], 'firstforwellbeing');

function intToYesNo($a) {
    if($a == 1) {
        return 'Yes';
    } else {
        return 'No';
    }
}