<?php
namespace FfwAssessmentLibs; // unique namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Concrete\Core\User\Group\Group as Group;
use Concrete\Core\Encryption as Encryption;
use Exception;
use stdClass;

class FfwAssessment {

	private $data, $enc;

	// Consructor, ID is optional but will try to retrieve an assessment from the DB by ID
	function __construct( $id = null ) {
		// Get DB connection
		$this->data = \Database::connection('ffwdata');
		$this->enc = \Database::connection('ffwdata_e');
		// Find assessment if specified
		if($id !== null) {
			$this->getById($id);
		} else {
			// only one live assessment allowed currently so get its id and load it up
			$h = $this->data->executeQuery('select id from assessments where deleted = 0');
			$h = $h->fetch();
			$this->getById($h['id']);
		}
	}

	// Read entire assessment structure from the DB
	public function getById($id) {
		$h = $this->data->executeQuery('select created from assessments where id = ? and deleted = 0', [ $id ], [ \PDO::PARAM_INT ]);
		if($h) {
			// Get assessment info
			$h = $h->fetch();
			$this->id = $id;
			$this->created = $h['created'];
			$this->themes = [];
			// Get theme info
			$h = $this->data->executeQuery('select t.* from themes t join assessment_themes at on t.id = at.theme_id where t.deleted = 0 and at.assessment_id = ? order by at.display', [ $id ], [ \PDO::PARAM_INT ]);
			if($h) {
				while($row = $h->fetch()) {
					$row['questions'] = [];
					$row['rules'] = [];
					$this->themes[] = $row;
				}
			}
			// Get theme questions, rules
			foreach($this->themes as $i => $theme) {
				// Questions
				$h = $this->data->executeQuery('select q.* from questions q join theme_questions tq on q.id = tq.question_id where q.deleted = 0 and tq.theme_id = ? order by tq.display', [ $theme['id'] ], [ \PDO::PARAM_INT ]);
				if($h) { // some questions exist
					while($row = $h->fetch() ) {
						$row['rules'] = [];
						$row['choices'] = [];
						$row['response'] = '';
						$this->themes[$i]['questions'][] = $row;
					}
				}
				// Rules
				$h = $this->data->executeQuery('select rule_id from theme_rulesets tr join assessment_themes at on tr.assessment_theme_id = at.id join assessments a on at.assessment_id = a.id where a.deleted = 0 and tr.deleted = 0 and at.theme_id = ?', [ $theme['id'] ], [ \PDO::PARAM_INT ]);
				while($row = $h->fetch() ) {
					$row['conditions'] = [];
					$this->themes[$i]['rules'][] = $row;
				}
			}
			// get theme rule conditions, theme questions rules
			foreach( $this->themes as $ti => $theme ) {
				//conditions
				foreach($theme['rules'] as $ri => $rule ) {
					$this->themes[$ti]['rules'][$ri]['conditions'] = $this->getRules($rule['rule_id']);
				}
				// questions rules
				foreach($theme['questions'] as $qi => $question ) {
					$h = $this->data->executeQuery('select rule_id from question_rulesets qr join theme_questions tq on qr.theme_question_id = tq.id join themes t on t.id=tq.theme_id where qr.deleted = 0 and t.deleted = 0 and tq.question_id = ?', [ $question['id'] ], [ \PDO::PARAM_INT ]);
					while($row = $h->fetch() ) {
						$row['conditions'] = [];
						$this->themes[$ti]['questions'][$qi]['rules'][] = $row;
					}
				}
			}
			// get question rule conditions, question choices
			foreach( $this->themes as $ti => $theme ) {
				foreach($theme['questions'] as $qi => $question ) {
					// rules
					foreach($question['rules'] as $ri => $rule ) {
						$this->themes[$ti]['questions'][$qi]['rules'][$ri]['conditions'] = $this->getRules($rule['rule_id']);
					}
					// choices
					$h = $this->data->executeQuery('select * from question_choices where question_id = ?', [ $question['id'] ], [ \PDO::PARAM_INT ]);
					while($row = $h->fetch() ) {
						$this->themes[$ti]['questions'][$qi]['choices'][] = $row;
					}
					// get any stored responses
					//$h = $this->data->executeQuery('select * from responses where question_id = ? and assessment_id = ?', [ $question['id'], $this->id ], [ \PDO::PARAM_INT, \PDO::PARAM_INT ]);
					//while($row = $h->fetch() ) {
						//$this->themes[$ti]['questions'][$qi]['response'][] = $row;
						//// check for multiple choice response
						//$h2 = $this->data->executeQuery('select * from multiple_responses where response_id = ?', [ $row['id'] ], [ \PDO::PARAM_INT ]);
						//$mr = [];
						//while($row2 = $h2->fetch() ) {
							//$mr[] = $row2;
						//}
						//if(count($mr) > 0) {
							//$this->themes[$ti]['questions'][$qi]['response']['response_id'] = $mr;
						//}
						//// check for encrypted data
						//if($row['response_enc_asset_id'] > 0) {
							//$h = $this->enc->executeQuery('select asset from assets where id = ?', [ $row['response_enc_asset_id'] ], [ \PDO::PARAM_INT, \PDO::PARAM_INT ]);
							//if( $row = $h->fetch() ) {
								//$this->themes[$ti]['questions'][$qi]['response'][0]['response_enc_asset'] = $row['asset'];
							//}
						//}
					//}
				}
			}
		}
	}

	// Refresh the assessment data
	public function refresh() {
		if(is_numeric($this->id)) {
			$this->getById($this->id);
			return true;
		} else {
			return false;
		}
	}

	// Recursively retrieve the associated conditions for a rule id
	public function getRules($rule_id) {
		$list[ $rule_id ] = 1;
		// fetch rule tree
		$rules = [];
		$count = 1;
		while($count > 0) {
			$count = 0;
			$list = implode(',', array_keys($list)); // using keys minimises dupe entries
			$r = $this->data->executeQuery('
				select r.*, rt.name, rvl.name as left_name, rvr.name as right_name, ql.title as left_title, qr.title as right_title
				from rules r 
					join ruletypes rt on r.ruletype_id = rt.id
					join rule_valuetypes rvl on rvl.id = r.left_valuetype
					join rule_valuetypes rvr on rvr.id = r.right_valuetype
					left join questions ql on ql.id = r.left_value
					left join questions qr on qr.id = r.right_value
				where 
					r.id in (' . $list . ')'
			);
			$list = []; 
			while($row = $r->fetch() ) {
				$list[$row['true_path']] = 1;
				//$list[$row['false_path']] = 1; // only have a true path for now. 'Else' path might be overkill for users
				$rules[ $row['id'] ] = $row;
				$count++; // because PDO can't be relied on to return select mySQL rowcounts with rowCount()
			}
		}
		// Should have list of all rules in the tree now, in $rules
		return $rules;
	}

}


