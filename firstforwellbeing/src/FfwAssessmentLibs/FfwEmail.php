<?php
namespace FfwAssessmentLibs; // unique namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\User\User;
use Concrete\Core\Page\Controller\PageController;
use Concrete\Core\User\Group\Group as Group;
use Concrete\Core\Encryption as Encryption;
use Exception;
use stdClass;

class FfwEmail {

    public function send($to, $subject, $body, $from = 'help@firstforwellbeing.co.uk', $from_name = 'First For Wellbeing') {
        if(isset(FFW_CONFIG[getenv('FFW_PLATFORM')]['email_override'])) {
            $to = FFW_CONFIG[getenv('FFW_PLATFORM')]['email_override'];
            $testing = true;
        } else {
            $testing = false;
        }
        $mail = \Core::make('mail');
        $mail->setTesting($testing);
        $mail->setBody("Dear concrete5 team\nYour CMS is by far the best I\'ve ever seen.");
        $mail->addParameter('mailContent', $body);
        $mail->load('default', 'firstforwellbeing'); // add any parameters before loading the template
        $mail->setSubject($subject);
        $mail->from($from, $from_name);
        $mail->to($to);
        $mail->sendMail();
    }

}