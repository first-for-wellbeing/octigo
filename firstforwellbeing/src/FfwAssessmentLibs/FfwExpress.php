<?php
namespace FfwAssessmentLibs; // unique namespace

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\Support\Facade\Facade;
use Concrete\Core\Support\Facade\Express;
use Concrete\Core\Entity\Express\Entity;
use Concrete\Core\Express\Entry\Search\Result\Result;
use Concrete\Core\Express\EntryList;


class FfwExpress {

    public $entityManager;
    public $app;

    public function __construct() {
        $this->app = Facade::getFacadeApplication();
        $this->entityManager = $this->app->make('database/orm')->entityManager();
    }

    public function get_email($handle) {
        $r = $this->entityManager->getRepository('Concrete\Core\Entity\Express\Entity')->findBy(array("handle"=>"email"));
        $list = new EntryList($r[0]);
        $list->filterByAttribute("handle", $handle, "=");
        $entries = $list->getResults();
        return [
            'name' => $entries[0]->getName(),
            'subject' => $entries[0]->getSubject(),
            'body' => $entries[0]->getBody()
        ];
    }

    public function get_email_tokenised($handle, $tokens) {
        $email = $this->get_email($handle);
        $email = str_ireplace(array_keys($tokens), $tokens, $email);
        return $email;
    }

}

