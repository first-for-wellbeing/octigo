<?php
defined('C5_EXECUTE') or die("Access Denied.");

$this->inc('elements/header.php');

$this_path =  $c->getCollectionPath();
$this_page = basename($this_path);
?>

<main>
    <div class="container">
		<div class="row">
            <ul class="nav nav-tabs">
                <li <?php echo (strpos($this_path, '/ffw/assessment') !== false ? 'class="active"' : '');?>><a href="/ffw/assessments">Manage Assessment</a></li>
                <li <?php echo (strpos($this_path, '/ffw/theme') !== false  ? 'class="active"' : '');?>><a href="/ffw/themes">Manage Steps</a></li>
                <li <?php echo (strpos($this_path, '/ffw/question') !== false ? 'class="active"' : '');?>><a href="/ffw/questions">Manage Questions</a></li>
                <li <?php echo (strpos($this_path, '/ffw/pathway') !== false ? 'class="active"' : '');?>><a href="/ffw/pathways" style="text-align:right">Manage Pathways</a></li>
            </ul>
		</div>
				
        <div class="row">
            <div class="col-sm-12">
                <?php // Content
                View::element('system_errors', [
                    'format' => 'block',
                    'error' => isset($error) ? $error : null,
                    'success' => isset($success) ? $success : null,
                    'message' => isset($message) ? $message : null,
                ]);

                echo $innerContent;
                ?>
            </div>
        </div>
    </div>
</main>

<?php
$this->inc('elements/footer.php');
