<?php defined('C5_EXECUTE') or die("Access Denied.");

$footerSiteTitle = new GlobalArea('Footer Site Title');
$footerSiteTitleBlocks = $footerSiteTitle->getTotalBlocksInArea();

$footerSocial = new GlobalArea('Footer Social');
$footerSocialBlocks = $footerSocial->getTotalBlocksInArea();

$displayFirstSection = $footerSiteTitleBlocks > 0 || $footerSocialBlocks > 0 || $c->isEditMode();
?>

<script type="text/javascript">
	$(document).ready( function() {
		$('.bmi-how-weigh[textval=Metric]').on('change', function(e) {
				$('input.bmi-kg').val( Math.round((parseInt($('input.bmi-st').val()) * 14 + parseInt($('input.bmi-lbs').val())) / 2.2) );
		});
		$('.bmi-how-weigh[textval=Imperial]').on('change', function(e) {
				$('input.bmi-st').val( Math.floor( $('input.bmi-kg').val() * 2.2 / 14 ));
				$('input.bmi-lbs').val( Math.round(($('input.bmi-kg').val() * 2.2) % 14) );
		});
		$('.bmi-how-measure[textval=Metric]').on('change', function(e) {
				$('input.bmi-cm').val( Math.round( (parseInt($('input.bmi-feet').val()) * 12 + parseInt($('input.bmi-in').val())) * 2.54) );
		});
		$('.bmi-how-measure[textval=Imperial]').on('change', function(e) {
				$('input.bmi-feet').val( Math.floor( $('input.bmi-cm').val() / 2.54 / 12 ));
				$('input.bmi-in').val( Math.round(($('input.bmi-cm').val() / 2.54) % 12) );
		});
		$('input.bmi-kg, input.bmi-cm').on('change', function(e) {
				$('input.bmi-st').val( Math.floor( $('input.bmi-kg').val() * 2.2 / 14 ));
				$('input.bmi-lbs').val( Math.round(($('input.bmi-kg').val() * 2.2) % 14) );
				$('input.bmi-feet').val( Math.floor( $('input.bmi-cm').val() / 2.54 / 12 ));
				$('input.bmi-in').val( Math.round(($('input.bmi-cm').val() / 2.54) % 12) );
			//$('span.weight-bmi-score').val() * 10000 / $('.bmi-cm').val() / $('.bmi-cm').val() );
		});
		$('input.bmi-st, input.bmi-lbs, input.bmi-feet, input.bmi-in').on('change', function(e) {
				$('input.bmi-kg').val( Math.round((parseInt($('input.bmi-st').val()) * 14 + parseInt($('input.bmi-lbs').val())) / 2.2) );
				$('input.bmi-cm').val( Math.round( (parseInt($('input.bmi-feet').val()) * 12 + parseInt($('input.bmi-in').val())) * 2.54) );
			//$('span.weight-bmi-score').val() * 10000 / $('.bmi-cm').val() / $('.bmi-cm').val() );
		});
	});

</script>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="footer-nav col-md-6 col-lg-4">
                <div class="row">
                    <ul id="menu-footer-menu-one" class="col-sm-4">
                        <li id="menu-item-333"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home first menu-item-333">
                            <a href="https://www.firstforwellbeing.co.uk/">Home</a></li>
                        <li id="menu-item-334"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-334"><a
                                    href="https://www.firstforwellbeing.co.uk/about-us/">About Us</a></li>
                        <li id="menu-item-844"
                            class="menu-item menu-item-type-post_type menu-item-object-page last menu-item-844"><a
                                    href="https://www.firstforwellbeing.co.uk/contact-us/">Contact Us</a></li>
                    </ul>
                    <ul id="menu-footer-menu-two" class="col-sm-4">
                        <li id="menu-item-1337"
                            class="menu-item menu-item-type-post_type menu-item-object-page first menu-item-1337"><a
                                    href="https://www.firstforwellbeing.co.uk/careers/">Careers</a></li>
                        <li id="menu-item-1338"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1338"><a
                                    href="https://www.firstforwellbeing.co.uk/faqs/">Faqs</a></li>
                        <li id="menu-item-1339"
                            class="menu-item menu-item-type-post_type menu-item-object-page last menu-item-1339"><a
                                    href="https://www.firstforwellbeing.co.uk/healthy-workplaces/">Businesses</a></li>
                    </ul>
                    <ul id="menu-footer-menu-three" class="col-sm-4">
                        <li id="menu-item-1341"
                            class="menu-item menu-item-type-post_type menu-item-object-page first menu-item-1341"><a
                                    href="https://www.firstforwellbeing.co.uk/latest/">Latest</a></li>
                        <li id="menu-item-1340"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1340"><a
                                    href="https://www.firstforwellbeing.co.uk/health-and-wellbeing/">Wellbeing</a></li>
                        <li id="menu-item-1342"
                            class="menu-item menu-item-type-post_type menu-item-object-page last menu-item-1342"><a
                                    href="https://www.firstforwellbeing.co.uk/in-the-family/">In the Family</a></li>
                    </ul>
                </div>
            </div>
            <script type="text/javascript" src="/packages/firstforwellbeing/elements/Twitter-Post-Fetcher-master/js/twitterFetcher.js"></script>
            <script type="text/javascript">
                $(document).ready( function() {
                    var result = twitterFetcher.fetch({
                        "profile": {"screenName" : "Firstwellbeing"},
                        "id": '345170787868762112',
                        "domId": 'tweet-goes-here',
                        "maxTweets": 1,
                        "enableLinks": false,
                        "showImages" : false,
                        "showUser" : false,
                        "showTime" : false,
                        "showRetweet" : false,
                        "showInteraction" : false,
                        "customCallback" : function(d) {
                            $('div#tweet-goes-here').html(d[0]);
                        }
                    });
                });
            </script>
            <div class="footer-social col-md-3">
                <div class="tweet-box content-position-outer" data-handle="Firstwellbeing">
                    <div class="content-center-inner" id="tweet-goes-here">
                    </div>
                </div>
                <ul>
                    <li><a href="https://twitter.com/Firstwellbeing" title="Twitter" target="_blank"><span
                                    class="icon-twitter"></span></a></li>
                    <li><a href="https://www.facebook.com/First-for-Wellbeing" title="Facebook" target="_blank"><span
                                    class="icon-facebook"></span></a></li>
                    <li><a href="mailto:help@firstforwellbeing.co.uk" title="Email" target="_blank"><span
                                    class="icon-email"></span></a></li>
                </ul>
            </div>
            <div class="footer-info col-md-9">
                <p>First for Wellbeing CIC Ltd is a community interest company; a joint venture between three
                    organisations in Northamptonshire</p>
                <ul>
                    <li><img alt="Northamptonshire County Council"
                             src="/packages/firstforwellbeing/themes/ffw_assessment/images/logo-ncc.png"/></li>
                    <li><img alt="Northamptonshire Healthcare NHS"
                             src="/packages/firstforwellbeing/themes/ffw_assessment/images/logo-northamptonshirehealthcare.png"/>
                    </li>
                    <li><img alt="The University of Nothampton"
                             src="/packages/firstforwellbeing/themes/ffw_assessment/images/logo-universityofnorthampton.png"/>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>&copy; <?php echo date('Y'); ?> First for Wellbeing CIC Ltd</p>
                    <ul id="menu-footer-policies" class="footer-pocily-menu">
                        <li id="menu-item-341"
                            class="menu-item menu-item-type-post_type menu-item-object-page first menu-item-341"><a
                                    href="https://www.firstforwellbeing.co.uk/terms-conditions/">Terms &amp; Conditions</a></li>
                        <li id="menu-item-340" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-340"><a
                                    href="https://www.firstforwellbeing.co.uk/privacy-policy/">Privacy Policy</a></li>
                        <li id="menu-item-339" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-339"><a
                                    href="https://www.firstforwellbeing.co.uk/cookie-policy/">Cookie Policy</a></li>
                        <li id="menu-item-847" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-847"><a
                                    href="https://www.firstforwellbeing.co.uk/site-map/">Site Map</a></li>
                        <li id="menu-item-1356"
                            class="menu-item menu-item-type-custom menu-item-object-custom last menu-item-1356"><a
                                    href="https://www.firstforwellbeing.co.uk/faqs/what-is-octigo/">Powered by <span
                                        style="color:#DC3F7D; font-weight:bold;">OCTIGO</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php $this->inc('elements/footer_bottom.php');?>
