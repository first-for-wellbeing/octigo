<?php defined('C5_EXECUTE') or die("Access Denied.");

$this->inc('elements/header_top.php');

$as = new GlobalArea('Header Search');
$blocks = $as->getTotalBlocksInArea();
$displayThirdColumn = $blocks > 0 || $c->isEditMode();
$uID = $controller->uID;
$username = $controller->username;
$usergroups = $controller->usergroups;
?>

<header id="header" class="clearfix">
    <div class="container">
        <p class="site-title">
            <a href="https://www.firstforwellbeing.co.uk" rel="home" title="First For Wellbeing"><img alt="First For Wellbeing" src="/packages/firstforwellbeing/themes/ffw_assessment/images/logo-firstforwellbeing.png" /></a>
        </p>
        <button class="nav-toggle" type="button" aria-expanded="false">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="sr-only">Toggle navigation</span>
        </button>
        <div class="header-info">
            <form id="siteSearch" class="header-search" method="get" action="https://www.firstforwellbeing.co.uk">
                <div class="content-position-outer">
                    <div class="content-center-inner">
                        <img alt="" src="/packages/firstforwellbeing/themes/ffw_assessment/images/icon-search.png" />
                        <label class="sr-only" for="s">Search the site</label>
                        <input id="s" type="text" name="s" placeholder="Search" value="<?php if(isset($_GET['s'])) echo $_GET['s'] ?>" />
                        <input id="searchSubmit" class="invisible" type="submit" title="Search" value="Search" />
                    </div>
                </div>
            </form>
            <ul class="header-social">
                <li><a href="https://twitter.com/Firstwellbeing" title="Twitter" target="_blank"><span class="icon-twitter" style="color:white;"></span></a></li>
                <li><a href="https://www.facebook.com/First-for-Wellbeing" title="Facebook"><span class="icon-facebook" style="color:white;"></span></a></li>
                <li><a href="mailto:help@firstforwellbeing.co.uk" title="Email"><span class="icon-email" style="color:white;"></span></a></li>
                <li class="mobile-only"><a id="mobileSearch" href="#" title="Search"><span class="icon-search"></span></a></li>
                <?php
                if(!$uID) {
                    ?>
                    <li class="mobile-only"><a href="/login" title="Login"><span class="icon-login"></span></a></li>
                    <?php
                } else {
                    ?>
                    <li class="mobile-only"><a href="/login/logout" title="Logout"><span class="icon-login"></span></a></li>
                    <?php
                }
                ?>
            </ul>
            <p>Helpline: <a href="tel:+443001265000" title="Call the helpline">0300 126 5000</a></p>
            <?php
            if(!$uID) {
                ?>
                <a id="btn-login" href="<?php echo URL::to('/login'); ?>" title="Login to the site">Login</a>
                <?php
            } else {

                ?>
                <div id="btn-login" title="Logout">
                    &nbsp;&nbsp;&nbsp;&nbsp;Hi <?php echo $username; ?> &nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;
                    <?php
                    if( in_array('Providers', $usergroups) || in_array('FFW Advisors', $usergroups) || in_array('Admin', $usergroups)){
                        ?>
                        <a href="/assessors" title="Customer List">
                            Customer List</a>&nbsp;&nbsp;&nbsp;&nbsp;|
                        <?php
                    }else{
                        ?>
                        <a href="/assessmentresults" title="Your Plan">Your Plan</a>&nbsp;&nbsp;&nbsp;&nbsp;|
                        <?php if(!in_array('FFW Advisors', $usergroups)) { ?>
                            &nbsp;&nbsp;&nbsp;&nbsp;<a href="/user-profile" title="Your Info">Your
                                Info</a>&nbsp;&nbsp;&nbsp;&nbsp;|
                        <?php
                        }
                    }
                    ?>
                &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo URL::to('/login', 'logout', \Core::make('helper/validation/token')->generate('logout')); ?>" title="Logout">
                    Logout</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                </div>
                <?php
            }
            ?>

        </div>
    </div>
</header>
<script type="text/javascript">
    $(document).ready( function() {
        // Mobile Nav Toggle
        $('.nav-toggle').click(function() {
            $('body').toggleClass('nav-open');
            $('#header').toggleClass('nav-open');
            $('#nav').toggleClass('open').slideToggle('slow');
            $(this).toggleClass('open');
        });
    });

</script>