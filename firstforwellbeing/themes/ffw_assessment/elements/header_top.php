<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<!DOCTYPE html>
<html lang="<?php echo Localization::activeLanguage() ?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" type="text/css" href="<?php echo $view->getThemePath()?>/css/bootstrap-modified.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $view->getThemePath()?>/css/bootstrap-datepicker.standalone.min.css">
    <?php echo $html->css($view->getStylesheet('main.less')) ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $view->getThemePath()?>/css/styles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $view->getThemePath()?>/css/ffw_assessment.css">
    <meta name="format-detection" content="telephone=no">
    <?php
    View::element('header_required', [
        'pageTitle' => isset($pageTitle) ? $pageTitle : '',
        'pageDescription' => isset($pageDescription) ? $pageDescription : '',
        'pageMetaKeywords' => isset($pageMetaKeywords) ? $pageMetaKeywords : ''
    ]);
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script>
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement('style');
            msViewportStyle.appendChild(
                document.createTextNode(
                    '@-ms-viewport{width:auto!important}'
                )
            );
            document.querySelector('head').appendChild(msViewportStyle);
        }
    </script>
    <!-- GA -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-75810174-1', 'auto');
        ga('send', 'pageview');

    </script>
    <link href="https://fonts.googleapis.com/css?family=Neucha" rel="stylesheet">
    <!-- Hotjar Tracking Code for https://www.firstforwellbeing.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:411439,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <!-- FONTS -->
    <script src="https://use.typekit.net/vub4ffw.js<?php echo'?cache='.rand(); ?>"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <link rel="shortcut icon" type="image/png" href="/packages/firstforwellbeing/themes/ffw_assessment/images/favicon.png" />
    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>

<body<?php if( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8.0')  !== false ) echo ' class="ie8"';  else if( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9.0')  !== false ) echo ' class="ie9" '; ?> data-path="<?php echo $_SERVER['REQUEST_URI'];?>">

<div class="<?php echo $c->getPageWrapperClass()?>">
