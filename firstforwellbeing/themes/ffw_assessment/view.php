<?php
defined('C5_EXECUTE') or die("Access Denied.");

$this->inc('elements/header.php');
?>

<nav id="nav" role="navigation">
    <div class="container">
        <?php
        //$menuHtml = wp_nav_menu(array('menu_id' => 'menu-navigation', 'menu_class' => 'primary-nav', 'menu' => 'Main Menu', 'echo' => false, 'depth' => 2, 'container' => ''));
        //echo str_replace('sub-menu','level-2',$menuHtml);
        ?>
        <ul id="menu-navigation" class="primary-nav"><li id="menu-item-446" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children first menu-item-446"><a href="https://www.firstforwellbeing.co.uk/health-and-wellbeing/">Health and Wellbeing</a>
                <ul class="level-2">
                    <li id="menu-item-543" class="menu-item menu-item-type-post_type menu-item-object-health-wellbeing menu-item-543"><a href="https://www.firstforwellbeing.co.uk/health-wellbeing/alcohol/">Alcohol – Drinkaware</a></li>
                    <li id="menu-item-548" class="menu-item menu-item-type-post_type menu-item-object-health-wellbeing menu-item-548"><a href="https://www.firstforwellbeing.co.uk/health-wellbeing/emotional-wellbeing/">Emotional Wellbeing</a></li>
                    <li id="menu-item-546" class="menu-item menu-item-type-post_type menu-item-object-health-wellbeing menu-item-546"><a href="https://www.firstforwellbeing.co.uk/health-wellbeing/employment-adult-learning/">Employment &amp; Adult Learning</a></li>
                    <li id="menu-item-545" class="menu-item menu-item-type-post_type menu-item-object-health-wellbeing menu-item-545"><a href="https://www.firstforwellbeing.co.uk/health-wellbeing/financial/">Financial Wellbeing</a></li>
                    <li id="menu-item-547" class="menu-item menu-item-type-post_type menu-item-object-health-wellbeing menu-item-547"><a href="https://www.firstforwellbeing.co.uk/health-wellbeing/housing-support/">Housing Support</a></li>
                    <li id="menu-item-549" class="menu-item menu-item-type-post_type menu-item-object-health-wellbeing menu-item-549"><a href="https://www.firstforwellbeing.co.uk/health-wellbeing/social-wellbeing/">Social Wellbeing</a></li>
                    <li id="menu-item-550" class="menu-item menu-item-type-post_type menu-item-object-health-wellbeing menu-item-550"><a href="https://www.firstforwellbeing.co.uk/health-wellbeing/smoking/">Stop Smoking</a></li>
                    <li id="menu-item-544" class="menu-item menu-item-type-post_type menu-item-object-health-wellbeing menu-item-544"><a href="https://www.firstforwellbeing.co.uk/health-wellbeing/weight/">Weight Management</a></li>
                </ul>
            </li>
            <li id="menu-item-312" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-312"><a href="https://www.firstforwellbeing.co.uk/in-the-family/">In the Family</a>
                <ul class="level-2">
                    <li id="menu-item-3062" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3062"><a href="https://www.firstforwellbeing.co.uk/services/adult-learning/">Adult Learning</a></li>
                    <li id="menu-item-319" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-319"><a href="https://www.firstforwellbeing.co.uk/services/countryside/">Country Parks</a></li>
                    <li id="menu-item-317" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-317"><a href="https://www.firstforwellbeing.co.uk/services/knuston-hall/">Knuston Hall</a></li>
                    <li id="menu-item-314" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-314"><a href="https://www.firstforwellbeing.co.uk/services/libraries/">Libraries</a></li>
                    <li id="menu-item-315" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-315"><a href="https://www.firstforwellbeing.co.uk/services/n-sport/">Northamptonshire Sport</a></li>
                </ul>
            </li>
            <li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-56"><a href="https://www.firstforwellbeing.co.uk/about-us/">About Us</a>
                <ul class="level-2">
                    <li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57"><a href="https://www.firstforwellbeing.co.uk/about-us/mission-vision/">Mission and Vision</a></li>
                    <li id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59"><a href="https://www.firstforwellbeing.co.uk/about-us/our-members/">Our Members</a></li>
                    <li id="menu-item-1472" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1472"><a href="https://www.firstforwellbeing.co.uk/about-us/commissioning/">Commissioning</a></li>
                    <li id="menu-item-1357" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1357"><a href="https://www.firstforwellbeing.co.uk/careers/">Careers</a></li>
                </ul>
            </li>
            <li id="menu-item-331" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-331"><a href="https://www.firstforwellbeing.co.uk/latest/">Latest</a></li>
            <li id="menu-item-271" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-271"><a href="https://www.firstforwellbeing.co.uk/faqs/">FAQs</a></li>
            <li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page last menu-item-67"><a href="https://www.firstforwellbeing.co.uk/contact-us/">Contact</a></li>
        </ul>
        <?php
        $this_user = $controller->uID;
        $last_entry = preg_replace('/^(\d+).(\d+).(\d+)$/', "$3-$2-$1", $controller->response_collection['twelve_weeks']);
        $last_entry = ($last_entry <= date('Y-m-d'));

        if(
            $controller->response_collection['id'] < 1
            or in_array( 'FFW Advisors', $controller->usergroups)
            or in_array( 'Assessor Admins', $controller->usergroups)
        ) { // no existing entries for this custid and role check
            echo '<a class="button button-pink button-assessmenttool" href="/assessment" title="Online assessment" style="width:270px;"><img alt="" src="/packages/firstforwellbeing/themes/ffw_assessment/images/icon-heart.png" /> Online assessment</a>';
        } else {
            if( $last_entry ) {
                // Display retake button
                echo '<a class="button button-blue button-assessmenttool" href="/assessment" title="Online assessment" style="width:300px;"><img alt="" src="/packages/firstforwellbeing/themes/ffw_assessment/images/icon-heart.png" />&nbsp;Follow&nbsp;up&nbsp;assessment</a>';
            }
        }
        ?>
    </div>
</nav>

<div class="container" style="padding-top:15px;">
    <div class="row">
        <div class="col-md-12">
            <?php // Content
            View::element('system_errors', [
                'format' => 'block',
                'error' => isset($error) ? $error : null,
                'success' => isset($success) ? $success : null,
                'message' => isset($message) ? $message : null,
            ]);

            echo $innerContent;
            ?>
        </div>
    </div>
</div>

<?php
$this->inc('elements/footer.php');
